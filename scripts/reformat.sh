#!/bin/sh

# Requires: awk uncrustify

SOURCE_ROOT=$(cd "$(dirname -- "$0")/../maze/"; pwd)

cd ${SOURCE_ROOT}

FNAME=$(mktemp --tmpdir uncrustify.files.tmp.XXXXXXXX)

awk 'BEGIN {FS="[()]"};/^add_(header)|(source)\(.*\)/ {print $2}' cmake/files.cmake > ${FNAME}

uncrustify -c uncrustify.cfg --replace --no-backup -F ${FNAME}

rm ${FNAME}
