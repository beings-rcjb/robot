#!/bin/sh

set -e

REPO_ROOT="$(dirname -- "$0")/../"

ln -sr ${REPO_ROOT}/scripts/git/hooks/canonicalize_filename.sh ${REPO_ROOT}/.git/hooks/canonicalize_filename.sh
ln -sr ${REPO_ROOT}/scripts/git/hooks/pre-commit ${REPO_ROOT}/.git/hooks/pre-commit

exit
