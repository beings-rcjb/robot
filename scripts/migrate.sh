#!/bin/bash

SCRIPTNAME=`basename $0`

print_usage()
{
  cat <<USAGE
Usage: $SCRIPTNAME CUBE_LOC DEST_LOC
USAGE
}


if [ $# -ne 2 ]; then
  print_usage
fi

SRC=${1%/}
DEST=${2%/}

if [ -d $SRC ] && [ -d $DEST ]; then
  :
else
  print_usage
fi

if [ -f $SRC/Src/main.c ] && [ -f $SRC/Src/stm32f4xx_it.c ] && [ -f $SRC/Inc/stm32f4xx_hal_conf.h ]; then
  :
else
  echo \"$SRC\" is not a valid Cube project directory
  print_usage
fi

if [ -f $DEST/src/_initialize_hardware.c ]; then
  mv $DEST/src/_initialize_hardware.c $DEST/src/_initialize_hardware.c.BAK
fi
cat $SRC/Src/main.c | sed '
1 , 32 d
/#ifdef USE_FULL_ASSERT/ , $ d
s/int main/void __initialize_hardware/
\%/* USER CODE% , +1 d
/  [{}]/ d
\%/* Infinite loop% d
/#include "stm32f4xx_hal.h"/ s/stm32f4xx_hal.h/includes.h/
/SystemClock_Config();/ a\\n  SystemCoreClockUpdate();
\%/* Private function prototypes% a\void __initialize_hardware(void);
' > $DEST/src/_initialize_hardware.c

if [ -f $DEST/src/stm32f4xx_it.c ]; then
  mv $DEST/src/stm32f4xx_it.c $DEST/src/stm32f4xx_it.c.BAK
fi
cat $SRC/Src/stm32f4xx_it.c | sed '
1, 32 d
\%/* USER CODE% d
/#include "/ d
\%/* Includes % a\#include "includes.h"
/HAL_SYSTICK_IRQHandler/ a\\n  SystemTick();
' > $DEST/src/stm32f4xx_it.c

if [ -f $DEST/src/stm32f4xx_hal_msp.c ]; then
  mv $DEST/src/stm32f4xx_hal_msp.c $DEST/src/stm32f4xx_hal_msp.c.BAK
fi
cat $SRC/Src/stm32f4xx_hal_msp.c | sed '
1 , 33 d
\%/* USER CODE% d
/#include "stm32f4xx_hal.h"/ s/stm32f4xx_hal.h/includes.h/
' > $DEST/src/stm32f4xx_hal_msp.c

if [ -f $DEST/include/stm32f4xx_hal_conf.h ]; then
  mv $DEST/include/stm32f4xx_hal_conf.h $DEST/include/stm32f4xx_hal_conf.h.BAK
fi
cp $SRC/Inc/stm32f4xx_hal_conf.h $DEST/include/stm32f4xx_hal_conf.h
