#include "robot.h"
#include "gpio.h"
#include "includes.h"
#include "hardwaredefs.h"

#include <cmath>
#include <algorithm>
#include <tuple>

Robot robot(TIM2, TIM5, TIM10, SERVO_PULSE_CENTER, SERVO_PULSE_RANGE);

Robot::TOF_Sensor Robot::Distance_FrontSensor::Left      = TOF_Sensor(GPIOA, GPIO_PIN(6), TOF_OFFSET_FL);
Robot::TOF_Sensor Robot::Distance_FrontSensor::Right     = TOF_Sensor(GPIOA, GPIO_PIN(7), TOF_OFFSET_FR);
Robot::TOF_Sensor Robot::Distance_SideSensor::LeftFront  = TOF_Sensor(GPIOC, GPIO_PIN(0), TOF_OFFSET_LF);
Robot::TOF_Sensor Robot::Distance_SideSensor::LeftBack   = TOF_Sensor(GPIOC, GPIO_PIN(1), TOF_OFFSET_LB);
Robot::TOF_Sensor Robot::Distance_SideSensor::RightFront = TOF_Sensor(GPIOC, GPIO_PIN(2), TOF_OFFSET_RF);
Robot::TOF_Sensor Robot::Distance_SideSensor::RightBack  = TOF_Sensor(GPIOC, GPIO_PIN(3), TOF_OFFSET_RB);

template <class Cont>
uint16_t container_average(const Cont& container)
{
	uint32_t sum = 0;
	for (const uint16_t& val : container)
	{
		sum += val;
	}
	return sum / container.size();
}

Robot::Robot(TIM_TypeDef* PWM_TIM1_, TIM_TypeDef* PWM_TIM2_, TIM_TypeDef* Servo_TIM_,
             float Servo_Center, float Servo_Range)
	: mpu(Mpu(&hi2c2, 0x68)),
	PWM_TIM1(PWM_TIM1_), PWM_TIM2(PWM_TIM2_), Servo_TIM(Servo_TIM_),
	Servo_RangeMin(Servo_Center - Servo_Range), Servo_RangeMax(Servo_Center + Servo_Range)
{}

bool Robot::init()
{
	uint16_t status = 0;
	display.begin(SSD1306_SWITCHCAPVCC);
	display.clearDisplay();
	{
		display.clearDisplay();
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.print("Initializing... ");
	}
	display.display();

	Robot::Distance_FrontSensor::Left.preInit();
	Robot::Distance_FrontSensor::Right.preInit();
	Robot::Distance_SideSensor::LeftFront.preInit();
	Robot::Distance_SideSensor::LeftBack.preInit();
	Robot::Distance_SideSensor::RightFront.preInit();
	Robot::Distance_SideSensor::RightBack.preInit();

	// Wake up Motor Driver
	GPIO_WritePin(GPIOA, GPIO_PIN_12, true);

	for (Motor motor : motors)
	{
		status |= !motor.init();
	}
	{
		TIM_HandleTypeDef htim;
		htim.Instance = Servo_TIM;
		status |= HAL_TIM_PWM_Start(&htim, TIM_CHANNEL(1));
		setServoPos(0);
		htim.Instance = PWM_TIM1;
		status |= HAL_TIM_IC_Start(&htim, TIM_CHANNEL(1));
		status |= HAL_TIM_IC_Start(&htim, TIM_CHANNEL(2));
		htim.Instance = PWM_TIM2;
		status |= HAL_TIM_IC_Start(&htim, TIM_CHANNEL(1));
		status |= HAL_TIM_IC_Start(&htim, TIM_CHANNEL(2));
	}
	{
		status |= HAL_TIM_Base_Start(&htim12);
		status |= HAL_TIM_IC_Start(&htim12, TIM_CHANNEL(2));
	}
	status |= HAL_ADC_Start_DMA(&hadc1, ADC_values[0], 2);
	status |= HAL_ADC_Start_DMA(&hadc2, ADC_values[1], 1);
	status |= !mpu.init(MPU_OFFS_AX, MPU_OFFS_AY, MPU_OFFS_AZ,
	                    MPU_OFFS_GX, MPU_OFFS_GY, MPU_OFFS_GZ);
	status |= Robot::Distance_FrontSensor::Left.initSensor(0x40);
	status |= Robot::Distance_FrontSensor::Right.initSensor(0x42);
	status |= Robot::Distance_SideSensor::LeftFront.initSensor(0x44);
	status |= Robot::Distance_SideSensor::LeftBack.initSensor(0x46);
	status |= Robot::Distance_SideSensor::RightFront.initSensor(0x48);
	status |= Robot::Distance_SideSensor::RightBack.initSensor(0x50);
	status |= Robot::Distance_FrontSensor::Left.postInit();
	status |= Robot::Distance_FrontSensor::Right.postInit();
	status |= Robot::Distance_SideSensor::LeftFront.postInit();
	status |= Robot::Distance_SideSensor::LeftBack.postInit();
	status |= Robot::Distance_SideSensor::RightFront.postInit();
	status |= Robot::Distance_SideSensor::RightBack.postInit();
	status |= !piInterface.init();
	Servo_TIM->PSC = 200;
	Servo_TIM->ARR = (SystemCoreClock / 10000) - 1;
	if (!status)
	{
		isInitialized = true;
		display.print("DONE\n");
	}
	else
	{
		display.print("FAIL\n");
	}
	display.display();
	return !status;
}

float Robot::getBatteryVoltage()
{
	return ADC_values[1][0] / 4096.0 * 3.3 * 2; // Convert to volts, multiply by 2 (monitor circuit voltage divider)
}

void Robot::onSysTick()
{
	if (isInitialized)
	{
		{ // MPU
			static uint8_t count = 0;
			if (++count == 10)
			{
				mpu.updateMPU();

				count = 0;
			}
		}

		{ // IRs
			Robot::Distance_FrontSensor::Left.onSysTick();
			Robot::Distance_FrontSensor::Right.onSysTick();
			Robot::Distance_SideSensor::LeftFront.onSysTick();
			Robot::Distance_SideSensor::LeftBack.onSysTick();
			Robot::Distance_SideSensor::RightFront.onSysTick();
			Robot::Distance_SideSensor::RightBack.onSysTick();
		}

		{ // Ping
			static uint8_t count = 0;
			if (count == 0)
			{
				GPIO_WritePin(GPIOC, GPIO_PIN(8), false);
			}
			if (++count == 100)
			{
				GPIO_WritePin(GPIOC, GPIO_PIN(8), true);

				count = 0;
			}
		}
	}
}

uint16_t Robot::getRawFloorLight()
{
	return ADC_values[0][0];
}

float Robot::getDistance(const TOF_Sensor& sensor)
{
	return sensor.getDistance();
}

float Robot::getPingDistance()
{
	return htim12.Instance->CCR2 * 0.0170145 + PING_OFFSET; // / 2 * 340.29 (speed of sound m/s) * 10^2 * ((1 / 168E6) * 2 * 84)
}

float Robot::getFloorLight()
{
	return getRawFloorLight() / (float)(1 << 12);
}

float Robot::getTemp(Temp_Sensor sensor)
{
	TIM_TypeDef* tim;
	switch (sensor)
	{
	case Temp_Sensor::Left:
		tim = PWM_TIM1;
		break;
	case Temp_Sensor::Right:
		tim = PWM_TIM2;
		break;
	}
	return (2 * (tim->CCR2 - (tim->CCR1 / 8)) * IR_TEMP_RANGE / tim->CCR1) + IR_TEMP_BASE; // (((2 * t_2) / T) * (T_max - T_min)) + T_min; CCR2 = t_1 + t_2, t_1 = T/8, T = CCR1, IR_TEMP_RANGE = T_max - T_min, IR_TEMP_BASE = T_min
}

bool Robot::getSwitchState(Switch sw)
{
	bool value =  GPIO_ReadPin(GPIOB, GPIO_PIN((uint8_t)sw));
	if ((sw ==  Switch::LeftPush) || (sw == Switch::RightPush)) // These switches pull the line down, so they need to be inverted
	{
		value = !value;
	}
	return value;
}

void Robot::setLEDState(LEDIndicator led, bool state)
{
	GPIO_WritePin(GPIOC, GPIO_PIN((uint8_t)led), state);
}

void Robot::setServoPos(float pos)
{
	float pulseWidth = (clamp(pos, -90, 90) / 180 + 0.5) * (Servo_RangeMax - Servo_RangeMin) + Servo_RangeMin;
	uint16_t ccr = (long long)(pulseWidth * SystemCoreClock) / Servo_TIM->PSC / 1000;
	Servo_TIM->CCR1 = ccr;
}

Robot::TOF_Sensor::TOF_Sensor(GPIO_TypeDef* gpioX, uint16_t gpioPin, float offset) : platform(&hi2c2),
                                                                                     sensor(&platform),
                                                                                     gpioX(gpioX), gpioPin(gpioPin),
                                                                                     offset(offset)
{}

void Robot::TOF_Sensor::preInit()
{
	GPIO_WritePin(gpioX, gpioPin, false); // Shut down
}

VL53L0X::VL53L0X_Error Robot::TOF_Sensor::initSensor(uint8_t address)
{
	VL53L0X::VL53L0X_Error status;
	GPIO_WritePin(gpioX, gpioPin, true); // Power up
	HAL_Delay(10); // Wait for device to power up
	status = sensor.DataInit();
	if (status != VL53L0X::VL53L0X_ERROR_NONE)
		return status;
	status = sensor.StaticInit();
	if (status != VL53L0X::VL53L0X_ERROR_NONE)
		return status;
	status = sensor.SetDeviceAddress(static_cast<uint8_t>(address));
	if (status != VL53L0X::VL53L0X_ERROR_NONE)
		return status;
	return status;
}

VL53L0X::VL53L0X_Error Robot::TOF_Sensor::postInit()
{
	VL53L0X::VL53L0X_Error status;
	status = sensor.SetMeasurementTimingBudgetMicroSeconds(timingBudgetMs * 1000u);
	if (status != VL53L0X::VL53L0X_ERROR_NONE)
		return status;
	status = sensor.SetDeviceMode(VL53L0X::VL53L0X_DEVICEMODE_CONTINUOUS_RANGING);
	if (status != VL53L0X::VL53L0X_ERROR_NONE)
		return status;
	status = sensor.StartMeasurement();
	if (status != VL53L0X::VL53L0X_ERROR_NONE)
		return status;
	return status;
}

void Robot::TOF_Sensor::onSysTick()
{
	static uint8_t count = 0;
	if (++count == 5)
	{
		count = 0;
		uint8_t readyState;
		sensor.GetMeasurementDataReady(&readyState);
		if (readyState == 1)
		{
			// TODO: Handling data not in ISR
			sensor.GetRangingMeasurementData(&lastRangingData); // TODO: Reject bad data?
			sensor.ClearInterruptMask(0); // Clear all interrupt bits
		}
	}
}

float Robot::TOF_Sensor::getDistance() const
{
//	return lastRangingData.RangeMilliMeter / 10.f + lastRangingData.RangeFractionalPart / 2560.f;
	return lastRangingData.RangeMilliMeter / 10.f + offset;
}
