#include "includes.h"
#include "robot.h"
#include "hw_impl.h"
#include "timeout.h"

extern "C" {

void SystemTick()
{
	robot.onSysTick();
	HwImpl::mover.onSysTick();
	Timeout::onSysTick();
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef* huart)
{
	robot.hdlc.RxCpltCallback(huart);
}

void HAL_UART_ErrorCallback(UART_HandleTypeDef* huart)
{
	robot.hdlc.ErrorCallback(huart);
}

}
