//
// This file is part of the GNU ARM Eclipse distribution.
// Copyright (c) 2014 Liviu Ionescu.
//

// ----------------------------------------------------------------------------

#include "diag/Trace.h"
#include "robot.h"
#include "algo.h"
#include "calibration.h"
#include <cstdio>

#include <eeprom_mem.h>

// ----------------------------------------------------------------------------
//
// Standalone STM32F4 empty sample (trace via STDOUT).
//
// Trace support is enabled by adding the TRACE macro definition.
// By default the trace messages are forwarded to the STDOUT output,
// but can be rerouted to any device or completely suppressed, by
// changing the definitions required in system/src/diag/trace_impl.c
// (currently OS_USE_TRACE_ITM, OS_USE_TRACE_SEMIHOSTING_DEBUG/_STDOUT).
//

// ----- main() ---------------------------------------------------------------

// Sample pragmas to cope with warnings. Please note the related line at
// the end of this function, used to pop the compiler diagnostics status.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#pragma GCC diagnostic ignored "-Wmissing-declarations"
#pragma GCC diagnostic ignored "-Wreturn-type"

void run_maze()
{
	robot.display.println("\nStart       Resume");
	robot.display.display();
	while (1)
	{
		algo = Algorithm();
		if (robot.getSwitchState(Robot::Switch::LeftPush))
		{
			HAL_Delay(1000);
			robot.motors[0].update(0.0f);
			robot.motors[1].update(0.0f);
			HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_1);
			HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_2);
			algo.init_solve();
			HAL_TIM_PWM_Stop(&htim9, TIM_CHANNEL_1);
			HAL_TIM_PWM_Stop(&htim9, TIM_CHANNEL_2);
		}
		if (robot.getSwitchState(Robot::Switch::RightPush))
		{
			HAL_Delay(1000);
			robot.motors[0].update(0.0f);
			robot.motors[1].update(0.0f);
			HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_1);
			HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_2);
			algo.resume_solve();
			HAL_TIM_PWM_Stop(&htim9, TIM_CHANNEL_1);
			HAL_TIM_PWM_Stop(&htim9, TIM_CHANNEL_2);
		}
	}
}

int main(int argc, char* argv[])
{
	// At this stage the system clock should have already been configured
	// at high speed.

	HAL_TIM_Base_Start(&htim6);

	robot.init();

	{
		float vBat = robot.getBatteryVoltage();
		bool isLowBattery = false;
		if (vBat < 3.7)
		{
			robot.display.println("Low Battery!");
			isLowBattery = true;
		}
		{
			char buf[50];
			snprintf(buf, 50, "%.3fV\n", vBat);
			robot.display.print(buf);
		}
		robot.display.display();
		if (isLowBattery)
			HAL_Delay(1000);
	}

#if 1
	{
		AT24CXXX eeprom;
		char buf[50];
		uint8_t rst;
		uint8_t kit;
		eeprom.read(EEPROM::SilverResetCount, rst);
		eeprom.read(EEPROM::KitDropCount, kit);
		snprintf(buf, 50, "R:%d K:%d\n", rst, kit);
		robot.display.print(buf);
		robot.display.display();
	}
#endif

	// Infinite loop
	while (1)
	{
		if (robot.getSwitchState(Robot::Switch::Slide)) // Run maze if switch "on", calibration otherwise
		{
			run_maze();
		}
		else
		{
			run_calibration();
		}
	}
}

#pragma GCC diagnostic pop

// ----------------------------------------------------------------------------
