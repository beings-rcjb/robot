#include <cmath>
#include <limits>
#include "hw_impl.h"
#include "robot.h"
#include "move.h"
#include "mazeUtils.h"
#include "mazeData.h"
#include "algo.h"
#include "roundrobin.hpp"
#include "flash.h"
#include "calibration.h" // Calibration values
#include "hardwaredefs.h"
#include "timeout.h"
#include "eeprom_mem.h"

#include <MazeDrawer.h>

// Flash constants defined in linkerscript
extern size_t FLASH_MAZEDATA_BEGIN;
extern size_t FLASH_MAZEDATA_END;

__attribute__((section(".flash-maze"))) volatile const uint8_t flash_maze_isSaved = 0; // Flag for presence of save data
__attribute__((section(".flash-maze"))) volatile const uint8_t flash_maze_data[1024] = {}; // Maze data store in flash

namespace HwImpl
{

MoveDriver mover(robot);

}

namespace
{

using namespace HwImpl;

//  ######   #######  ##    ##  ######  ########    ###    ##    ## ########  ######
// ##    ## ##     ## ###   ## ##    ##    ##      ## ##   ###   ##    ##    ##    ##
// ##       ##     ## ####  ## ##          ##     ##   ##  ####  ##    ##    ##
// ##       ##     ## ## ## ##  ######     ##    ##     ## ## ## ##    ##     ######
// ##       ##     ## ##  ####       ##    ##    ######### ##  ####    ##          ##
// ##    ## ##     ## ##   ### ##    ##    ##    ##     ## ##   ###    ##    ##    ##
//  ######   #######  ##    ##  ######     ##    ##     ## ##    ##    ##     ######

// ====WALL DETECTION====
const float FRONT_DETECT_THRESH = 20;
const float SIDE_MAX_DETECT_THRESH = 20;
const float SIDE_MIN_DETECT_THRESH = 18;

// ====RAMP====
const float RAMP_MAX_REJECT_THRESHOLD = 25;
const float RAMP_AFTER_DIST_UP = 15;
const float RAMP_AFTER_DIST_DOWN = 10;

const float OBS_AVOID_MAX_TURN = 10;
const float OBS_AVOID_TURN_BUFFER = 3;

// ====SILVER DETECTION====
const unsigned int SILVER_SAMPLING_SAMPLES = 60;
const unsigned int SILVER_SAMPLING_INTERVAL = 5;
const float SILVER_BUMP_THRESHOLD = 3;

// ====VICTIM DETECTION====
const float TEMP_FALL_THRESHOLD = 0.5;
const float MOVING_COOLDOWN_DISTANCE = 15;
const float MOVING_BUFFER_SKIP_DISTANCE = 15;
const float MOVING_END_SKIP_DISTANCE = 15;
const float MOVING_OBSTACLE_DISTANCE = 8;
const float TURNING_COOLDOWN_DEGREES = 30;

// ====RETURN HOME====
const uint8_t RESET_MAX_TRIES = 3;
const uint8_t KIT_MAX_DROPS = 12;

// ########  ######## ######## #### ##    ## #### ######## ####  #######  ##    ##  ######
// ##     ## ##       ##        ##  ###   ##  ##     ##     ##  ##     ## ###   ## ##    ##
// ##     ## ##       ##        ##  ####  ##  ##     ##     ##  ##     ## ####  ## ##
// ##     ## ######   ######    ##  ## ## ##  ##     ##     ##  ##     ## ## ## ##  ######
// ##     ## ##       ##        ##  ##  ####  ##     ##     ##  ##     ## ##  ####       ##
// ##     ## ##       ##        ##  ##   ###  ##     ##     ##  ##     ## ##   ### ##    ##
// ########  ######## ##       #### ##    ## ####    ##    ####  #######  ##    ##  ######

/**
 * Reads and returns surrounding walls
 *
 * @param skipCheck whether or not to skip checking uncertain side readings
 * @return array of walls - [front, right, left]
 */
array<bool, 3> getWalls(bool skipCheck = true);

/**
 * Move forward one cell
 *
 * @return pair containing results - { {is ramp, ramp direction}, is black}
 */
void move_cell();

void move_ramp();

/**
 * Turn 90 degrees
 *
 * @param dir turning direction - true turns right, false turns left
 * @param doChecks whether or not to do movement-triggered navigation checks
 *                 (such as victim)
 */
void turn90(bool dir, bool doChecks = false);

float align(bool willMoveAfter);

void updateWalls();

void check_silver();

//=====================================================================================

Maze& maze = Maze::getInstance();

/**
 * Current cell type
 */
enum class CurrCellInfo
{
	InOld,     ///< Inside visited tile
	InNew,     ///< Inside new tile
} currCellInfo = CurrCellInfo::InOld;
// On cold start, algorithm will call initUpdateWalls which will set state ti InNew;
// Otherwise algorithm is resuming, and use default of InOld

/**
 * Next cell type
 */
enum class NextCellInfo
{
	ToVisited, ///< Moving to visited tile
	ToNew      ///< Moving to new tile
} nextCellInfo;

bool isTurn180 = false;

//=====================================================================================

uint32_t lastDirty;

void setDirty()
{
	lastDirty = HAL_GetTick();
}

void cleanRR()
{
	if (HAL_GetTick() - lastDirty < 128)
	{
		HAL_Delay(128 - (HAL_GetTick() - lastDirty));
	}
}

//=====================================================================================

uint8_t reset_count = 0;

void loadResetCount()
{
	robot.eeprom.read(EEPROM::SilverResetCount, reset_count);
}

void saveResetCount()
{
	robot.eeprom.write(EEPROM::SilverResetCount, reset_count);
}

void saveResetCount(uint8_t reset_count)
{
	::reset_count = reset_count;
	saveResetCount();
}

//=====================================================================================

uint8_t kit_count = 0;

void loadKitCount()
{
	robot.eeprom.read(EEPROM::KitDropCount, kit_count);
}

void setKitCount()
{
	robot.eeprom.write(EEPROM::KitDropCount, kit_count);
}

void incKitCount()
{
	++kit_count;
	setKitCount();
}

void resetKitCount()
{
	kit_count = 0;
	setKitCount();
}

//=====================================================================================

/**
 * Test walls infront of robot
 *
 * @return {is wall, {is obstacle, obstacle side (true = right) } }
 */
pair<bool, pair<bool, bool> > testWallFront()
{
	float right = robot.getDistance(Robot::Distance_FrontSensor::Right);
	float left  = robot.getDistance(Robot::Distance_FrontSensor::Left);
	float ping  = robot.getPingDistance();

	bool right_det = right < FRONT_DETECT_THRESH;
	bool left_det  = left  < FRONT_DETECT_THRESH;
	bool ping_det  = ping  < FRONT_DETECT_THRESH;

	if (right_det == left_det && right_det == ping_det)     // All agree
	{
		return make_pair(ping_det, make_pair(false, false));     // Return one
	}
	if (ping_det && !left_det && !right_det)     // Only ping detects
	{
		return make_pair(false, make_pair(false, false));
	}
	if (left_det && right_det && !ping_det)     // Only ping doesn't detect - most likely ramp
	{
		return make_pair(false, make_pair(false, false));
	}
	if (!ping_det && (left_det != right_det))     // Only one IR detects - either obstacle or misalignment
	{
		return make_pair(false, make_pair(true, right_det));
	}
	if (ping_det && (left_det != right_det))     // Only one IR does not detect - obstacle blocking in front or misalignment
	{
		return make_pair(true, make_pair(true, right_det));
	}

	// Should be unreachable
	assert_param(false);
	__builtin_unreachable();
}

/**
 *
 * @param side side to check on: true if right, false if left
 * @param reCheck whether or not to check uncertain values with front sensor
 * @param uncertainDefault value to return if check should be done but is disabled
 * @return if there is a wall
 */
bool testWallSide(bool side, bool reCheck, bool uncertainDefault = false)
{
	for (int i = 0; i < 2; ++i)
	{
		if (i != 0)
		{
			// Wait for fresh readings if is not first iteration
			HAL_Delay(128);
		}

		bool isUncertain = false;

		float front;
		float back;

		if (side)
		{
			front = robot.getDistance(Robot::Distance_SideSensor::RightFront);
			back = robot.getDistance(Robot::Distance_SideSensor::RightBack);
		}
		else
		{
			front = robot.getDistance(Robot::Distance_SideSensor::LeftFront);
			back = robot.getDistance(Robot::Distance_SideSensor::LeftBack);
		}

		// Set detect states: 0 -> wall, 1 -> uncertain, 2 -> no wall
		uint8_t frontDet = (front > SIDE_MIN_DETECT_THRESH) + (front > SIDE_MAX_DETECT_THRESH);
		uint8_t backDet = (back > SIDE_MIN_DETECT_THRESH) + (back > SIDE_MAX_DETECT_THRESH);

		if (frontDet != backDet)
		{
			// readings disagree - check
			isUncertain = true;
		}
		else
		{
			// readings are equal
			if (frontDet == 1)
				isUncertain = true;
			else
				return !frontDet;
		}

		if (isUncertain && reCheck)
		{
			// Read again to confirm
			continue;
		}
		else
			return uncertainDefault;

		// Should be unreachable
		assert_param(false);
		__builtin_unreachable();
	}
	return uncertainDefault;
}

/**
 * Handle a detected victim
 *
 * @param side The side the victim is located on: true if right, false if left
 * @param angle Angle to turn to face victim
 */
void victim(bool side, float angle = 90)
{
	static bool inProgress = false;
	while (inProgress)
	{}
	inProgress = true;
	incKitCount();
	robot.setServoPos(90 * (side ? 1 : -1));
	Timeout::setTimeout(250, [side] {
		robot.setServoPos(10 * (side ? -1 : 1));
		Timeout::setTimeout(250, [] {
			inProgress = false;
			robot.setServoPos(0);
		});
		{
			robot.display.clearDisplay();
			robot.display.setTextSize(1);
			robot.display.setCursor(0, 0);
			robot.display.display();
		}
	});
	{
		robot.display.clearDisplay();
		robot.display.setTextSize(2);
		robot.display.setCursor(5, 20);
		robot.display.println("Victim");
		robot.display.display();
	}
}

/**
 * Check for and avoid obstacles
 *
 * @return {should back out, degrees to turn back}
 */
pair<bool, float> obs_avoid()
{
	pair<bool, pair<bool, bool> > wallInfo = testWallFront();

	if (wallInfo.first)
	{
		// Obstacle / wall directly in front, back out
		return pair<bool, float>(true, 0);
	}
	if (wallInfo.second.first) // Obstacle detected
	{
		// Start turning away
		mover.turn(wallInfo.second.second ? -OBS_AVOID_MAX_TURN : OBS_AVOID_MAX_TURN, TURN_POWER_LOW);

		bool isClear = false;
		float clearStart;
		while (!mover.isFinished())
		{
			auto wallInfo = testWallFront();

			if (!wallInfo.first && !wallInfo.second.first) // Clear in front
			{
				if (!isClear) // Was not previously clear
					clearStart = mover.getCurrTurn();
				isClear = true;
			}
			else
			{
				isClear = false;
			}

			if (isClear)
			{
				if (abs(mover.getCurrTurn() - clearStart) > OBS_AVOID_TURN_BUFFER)
				{
					mover.stop();
				}
			}
		}
		return pair<bool, float>(false, -mover.getCurrTurn());
	}
	return pair<bool, float>(false, 0);

}

roundrobin<float> floorReadings(SILVER_SAMPLING_SAMPLES);

array<bool, 3> getWalls(bool skipCheck)
{
	cleanRR();

	array<bool, 3> walls;
	walls[0] = testWallFront().first;
	walls[1] = testWallSide(true,  !skipCheck, false);
	walls[2] = testWallSide(false, !skipCheck, false);
	return walls;
}

namespace VictimData
{

class LastDropInfo
{
private:

	float pos = 1e10; // Never

	static constexpr float distToAngleScale = 5; // TODO: tune

public:

	void setDistance(float distance) { pos = distance; }
	float getDistance() { return pos; }
	void setDegrees(float degrees) { pos = degrees / distToAngleScale; }
	float getDegrees() { return pos * distToAngleScale; }
};

LastDropInfo lastThermalDrops[2];
LastDropInfo lastVisualDrops[2];

}

namespace VisualVictimProcessing
{

using VisVicType = PiCamInterface::VisVicType;

constexpr float VISUAL_VICTIM_WALL_DISTANCE = 20;

class Parser
{
private:

	static constexpr uint8_t idleTime = 60;
	static constexpr unsigned int delayTime = 250;

	std::vector<VisVicType> detectedTypes;
	uint32_t lastDetectedTime = 0;
	bool active = true;

	VisVicType detectedType = VisVicType::None;

	bool side;

public:

	Parser(bool side) : side(side) {}

	void updateCb(VisVicType data)
	{
		if (data == VisVicType::None)
			return;
		if (!active)
			return;
		if (robot.getDistance(side ? Robot::Distance_SideSensor::RightFront : Robot::Distance_SideSensor::LeftFront)
		    > VISUAL_VICTIM_WALL_DISTANCE)
		{
			active = false;
			return;
		}
		detectedTypes.push_back(data);
		lastDetectedTime = HAL_GetTick();
		Timeout::setTimeout(idleTime, [this]() {
			if (robot.getDistance(side ? Robot::Distance_SideSensor::RightFront : Robot::Distance_SideSensor::LeftFront)
			    > VISUAL_VICTIM_WALL_DISTANCE)
				return;
			if ((HAL_GetTick() - lastDetectedTime) > (idleTime - 5) && detectedType == VisVicType::None)
			{
				if (!active)
				{
					active = true;
					return;
				}
				active = true;
				detectedType = detectedTypes[detectedTypes.size() / 2];
				detectedTypes.clear();
			}
		});
	}

	VisVicType isDetected()
	{
		if (HAL_GetTick() - lastDetectedTime < delayTime)
			return VisVicType::None;
		VisVicType ret = detectedType;
		if (ret != VisVicType::None)
			detectedType = VisVicType::None;
		if (robot.getDistance(side ? Robot::Distance_SideSensor::RightFront : Robot::Distance_SideSensor::LeftFront)
		    > VISUAL_VICTIM_WALL_DISTANCE)
			return VisVicType::None;
		if (HAL_GetTick() - lastDetectedTime > 500 + delayTime)
			return VisVicType::None;
		return ret;
	}

};

Parser parser[2] = { Parser(0), Parser(1) };

class _StaticInitializer
{
public:

	_StaticInitializer()
	{
		robot.piCamInterface.valueAcquiredCallbackManager.addCallback(
				[](std::pair<VisVicType, bool> data) {
			parser[data.second].updateCb(data.first);
		});
	}

} volatile _si;

}

void move_cell()
{
	bool doCheck = nextCellInfo ==  NextCellInfo::ToNew;
	float travel_distance = align(true);
	float remaining_distance = travel_distance;
	float endTurn = 0;
	{
		// Check for and move around obstacles
		pair<bool, float> ret = obs_avoid();
		if (ret.first)
		{
			coord_t nextPos = MazeUtils::move_dir(MazeState::pos, MazeState::orientation);
			maze[nextPos].setVisited(true);
			maze[nextPos].setBlack(true);
			currCellInfo = CurrCellInfo::InOld;
			return;
		}
		endTurn = ret.second;
		remaining_distance /= cos(endTurn * 0.01745329251994322);
	}
	bool isBlack = false, finished = false;
	uint32_t black_sample_end; // Tick when sampling to confirm black tile should end
	float last_drop_thermal[2];
	float max_temps[2] = { 0, 0 };
	float last_drop_visual[2];
	{
		for (uint8_t i = 0; i < 2; ++i)
		{
			last_drop_thermal[i] = VictimData::lastThermalDrops[i].getDistance() + remaining_distance;
			VictimData::lastThermalDrops[i].setDistance(last_drop_thermal[i]);
			last_drop_visual[i] = VictimData::lastVisualDrops[i].getDistance() + remaining_distance;
			VictimData::lastVisualDrops[i].setDistance(last_drop_visual[i]);
		}
	}
	{
		bool shouldSkipFirstSamples = currCellInfo == CurrCellInfo::InOld;
		if (shouldSkipFirstSamples)
		{
			float fake_last_drop = remaining_distance + MOVING_COOLDOWN_DISTANCE - MOVING_BUFFER_SKIP_DISTANCE;
			last_drop_thermal[0] = last_drop_thermal[1] = fake_last_drop;
		}
	}
	bool shouldSkipLastSamples = currCellInfo == CurrCellInfo::InNew && nextCellInfo == NextCellInfo::ToVisited
	                             && !isTurn180;
	uint32_t next_sample = HAL_GetTick();
	floorReadings.clear();
	bool isBlackStarted = false;
	while (!finished)
	{
		mover.move_straight(remaining_distance, MOVE_POWER_NORM);
		finished = true;
		while (!mover.isFinished())
		{
			{
				float y, p, r;
				robot.mpu.getQuat().toYPR(y, p, r);
				// If black tile detected
				if ((robot.getFloorLight()) < BLACK_THRESHOLD && (abs(p) < 5))
				{
					if (!isBlackStarted)
					{
						isBlackStarted = true;
						black_sample_end = HAL_GetTick() + 100; // Sample for 100ms
					}
					else
					{
						if (HAL_GetTick() >= black_sample_end) // Done sampling for full duration and is all black
						{
							robot.display.clearDisplay();
							robot.display.setTextSize(2);
							robot.display.invertDisplay(1);
							robot.display.setCursor(5, 20);
							robot.display.println("Black");
							robot.display.display();
							robot.display.invertDisplay(0);

							isBlack = true;
							finished = true;
							mover.stop();
							remaining_distance -= mover.getCurrDistance();
							break;
						}
					}
				}
				else
				{
					if (isBlackStarted) // Sampling for black tile and not black detected
					{
						isBlackStarted = false;
					}
				}
				if (HAL_GetTick() >= next_sample)
				{
					floorReadings.insert(robot.getFloorLight());
					next_sample = HAL_GetTick() + SILVER_SAMPLING_INTERVAL;
				}
			}
			if (remaining_distance - mover.getCurrDistance() > MOVING_OBSTACLE_DISTANCE - 3)
			{
				float frontDistance = robot.getPingDistance();
				if (frontDistance < MOVING_OBSTACLE_DISTANCE)
				{
					mover.stop();
					remaining_distance -= mover.getCurrDistance();
					HAL_Delay(100); // Get a new reading
					frontDistance = robot.getPingDistance();
					if (frontDistance < MOVING_OBSTACLE_DISTANCE)
					{
						isBlack = true; // TODO: handle separately for more customization options
						finished = true;
					}
					else
					{
						finished = false;
					}
					break;
				}
			}
			if (shouldSkipLastSamples)
			{
				if (remaining_distance - mover.getCurrDistance() < MOVING_END_SKIP_DISTANCE)
					shouldSkipLastSamples = false;
			}
			if (doCheck || shouldSkipLastSamples)
			{
				float temperatures[2];
				temperatures[0] = robot.getTemp(Robot::Temp_Sensor::Left);
				temperatures[1] = robot.getTemp(Robot::Temp_Sensor::Right);
				for (uint8_t i = 0; i < 2; ++i)
				{
					if ((last_drop_thermal[i] - remaining_distance + mover.getCurrDistance()) > MOVING_COOLDOWN_DISTANCE)
					{
						// Update max temperatures
						if (temperatures[i] > max_temps[i])
						{
							max_temps[i] = temperatures[i];
						}
						// If MOVING_COOLDOWN_DISTANCE cm past last detected victim on side, max temperature is high
						// enough, and temperature starts falling a reasonable amount - victim detected
						else if ((max_temps[i] > MIN_TEMPERATURE)
						         && (temperatures[i] < max_temps[i] - TEMP_FALL_THRESHOLD))
						{
							// Stop moving and record distance still left
							mover.stop();
							remaining_distance -= mover.getCurrDistance();
							mover.clearPreviousRecords();
							last_drop_thermal[i] = remaining_distance;
							VictimData::lastThermalDrops[i].setDegrees(remaining_distance);
							max_temps[i] = 0;

							victim(i);

							finished = false;
							break;
						}
					}
					{
						PiCamInterface::VisVicType vicType = VisualVictimProcessing::parser[i].isDetected();
						if ((last_drop_visual[i] - remaining_distance + mover.getCurrDistance() > MOVING_COOLDOWN_DISTANCE)
						    && vicType != PiCamInterface::VisVicType::None)
						{
							// Stop moving and record distance still left
							mover.stop();
							remaining_distance -= mover.getCurrDistance();
							mover.clearPreviousRecords();
							last_drop_visual[i] = remaining_distance;
							VictimData::lastVisualDrops[i].setDegrees(remaining_distance);

							victim(i);
							if (vicType == PiCamInterface::VisVicType::H)
								victim(i);

							finished = false;
							break;
						}
					}
				}
				// Keep breaking out
				if (!finished)
				{
					break;
				}
			}
		}
	}
	setDirty();
	using namespace MazeUtils;
	using MazeState::pos;
	using MazeState::orientation;
	if (isBlack)
	{
		mover.move_straight(-(travel_distance - remaining_distance), MOVE_POWER_NORM);
		while (!mover.isFinished())
		{}
		setDirty();
		coord_t nextPos = move_dir(pos, orientation);
		maze[nextPos].setVisited(true);
		maze[nextPos].setBlack(true);
		currCellInfo = CurrCellInfo::InOld;
		return;
	}
	pos = move_dir(pos, orientation);
	float y, p, r;
	robot.mpu.getQuat().toYPR(y, p, r);
	if (abs(p) > 15) // ramp
	{
		bool direction = p > 0;
		maze[pos].setWall(orientation);
		maze[pos].setWall(orientation + 2);
		maze[pos].setVisited(true);
		maze[pos].setRamp(true);
		maze[pos].setRampDirection(direction);
		pos.second += direction ? 1 : -1;
		maze[pos].setWall(orientation); // Repeat on other layer
		maze[pos].setWall(orientation + 2);
		maze[pos].setVisited(true);
		maze[pos].setRamp(true);
		maze[pos].setRampDirection(!direction);
		pos = move_dir(pos, orientation); // Move past ramp as we are now at the top/bottom
		maze[pos].setWall(orientation + 2, Maze::Cell::Wall_state::NoWall); // Mark wall behind current position as nonexistent
		move_ramp();
	}
	else
	{
		maze[pos].setWall(orientation + 2, false); // Clear wall behind
		maze[move_dir(pos, orientation + 2)].setWall(orientation, false); // Other side of wall
		mover.turn(endTurn, TURN_POWER_NORM);
		while (!mover.isFinished())
		{}
		setDirty();
	}
	updateWalls();
	check_silver();
	currCellInfo = maze[pos].getVisited() ? CurrCellInfo::InOld : CurrCellInfo::InNew;
}

class MoveRampInfo : public MoveDriver::MoverMoveInfo
{
private:

	typedef MoveDriver::MoverMoveInfo super;

	static constexpr size_t HEADING_CORRECTION_ROLLING_AVERAGE_LENGTH = 1;

	PIDCtrl<float> centering_pid = PIDCtrl<float>(1, 0, 1);
	PIDCtrl<float> aligning_pid  = PIDCtrl<float>(0.010, 0, 0.0050);

	roundrobin<float> heading_correction_rr = roundrobin<float>(HEADING_CORRECTION_ROLLING_AVERAGE_LENGTH);

	float alignment_correction = 0;

	float previous_correction = 0;

	bool rampDirection;

public:

	MoveRampInfo(bool rampDirection) : rampDirection(rampDirection) {}

	virtual void bind(MoveDriver* md) override;
	virtual bool isFinished() override;
	virtual float getHeadingCorrection(float currError) override;
	virtual PIDCtrl<float> getHeadingPID() override;
};

bool MoveRampInfo::isFinished()
{
	static float y, p, r; // FIXME: Is there any purpose to static?
	robot.mpu.getQuat().toYPR(y, p, r);
	return abs(p) < 5; // Until at (roughly) bottom / top
}

void MoveRampInfo::bind(MoveDriver* md)
{
	heading_correction_rr.fill(0);
}

//#define HW_IMPL_MOVERAMP_DEBUG

float MoveRampInfo::getHeadingCorrection(float hPIDCurrError)
{
#ifdef HW_IMPL_MOVERAMP_DEBUG
	robot.display.clearDisplay();
#endif // HW_IMPL_MOVERAMP_DEBUG
	float distances[2][2];
	{
		distances[0][0] = robot.getDistance(Robot::Distance_SideSensor::LeftFront);
		distances[1][0] = robot.getDistance(Robot::Distance_SideSensor::LeftBack);
		distances[0][1] = robot.getDistance(Robot::Distance_SideSensor::RightFront);
		distances[1][1] = robot.getDistance(Robot::Distance_SideSensor::RightBack);
	}
	bool isValid = true;
	{
		if (abs((distances[0][0] + distances[0][1])
		        - (distances[1][0] + distances[1][1])) > 2)
		{
			isValid = false;
		}
		for (uint8_t i = 0; i < 2; ++i)
		{
			for (uint8_t j = 0; j < 2; ++j)
			{
				if (distances[i][j] > RAMP_MAX_REJECT_THRESHOLD)
				{
					isValid = false;
				}
			}
		}
	}
	float total_correction = 0;
#ifdef HW_IMPL_MOVERAMP_DEBUG
	if (isValid)
	{
		robot.display.fillRect(0, 60, 128, 3, WHITE);
	}
#endif // HW_IMPL_MOVERAMP_DEBUG

	float centering_error = 0;
	float centering_correction = 0;
	if (isValid)
	{
		centering_error = ((distances[0][0] - distances[0][1])
		                   + (distances[1][0] - distances[1][1])) / 2.0f;
		centering_correction = centering_pid.update(centering_error);
	}

#ifdef HW_IMPL_MOVERAMP_DEBUG
	{
		int8_t len = centering_error;
		int8_t start = min(63, 63 + len);
		robot.display.drawFastHLine(start, 36, abs(len), WHITE);
	}
	robot.display.fillRect(centering_correction + 63, 36, 1, 10, WHITE);
#endif // HW_IMPL_MOVERAMP_DEBUG

	float alignment_error = 0;
	if (isValid)
	{
		for (int i = 0; i < 2; ++i)
		{
			alignment_error += atan2((distances[0][i] - distances[1][i]) * (i ? -1 : 1), IR_FB_DISTANCE) * 180 / 3.1415926535;
		}
		alignment_error /= 2.0f;
		// remove what heading PID is already accounting for
		alignment_error -= (hPIDCurrError + previous_correction);
	}

#ifdef HW_IMPL_MOVERAMP_DEBUG
	{
		int8_t len = alignment_error;
		int8_t start = min(63, 63 + len);
		robot.display.drawFastHLine(start, 48, abs(len), WHITE);
	}
#endif // HW_IMPL_MOVERAMP_DEBUG

	alignment_error += centering_correction;

	alignment_correction += aligning_pid.update(alignment_error);
	total_correction += alignment_correction;

#ifdef HW_IMPL_MOVERAMP_DEBUG
	robot.display.fillRect(alignment_correction + 63, 48, 1, 10, WHITE);

	robot.display.fillRect(63, 0, 1, 58, WHITE);
	robot.display.fillRect(total_correction + 63, 24, 1, 10, WHITE);
#endif // HW_IMPL_MOVERAMP_DEBUG

	{
		// Rolling average
		heading_correction_rr.insert(total_correction);
		total_correction = 0;
		for (const auto& val : heading_correction_rr)
		{
			total_correction += val;
		}
		total_correction /= HEADING_CORRECTION_ROLLING_AVERAGE_LENGTH; // Round-robin is filled at start, should always be full
	}

#ifdef HW_IMPL_MOVERAMP_DEBUG
	{
		robot.display.fillRect(total_correction + 63, 12, 1, 10, WHITE);
	}
	robot.display.display();
#endif // HW_IMPL_MOVERAMP_DEBUG

	previous_correction = total_correction;

	return total_correction;
}

PIDCtrl<float> MoveRampInfo::getHeadingPID()
{
	if (rampDirection) // up
	{
		return PIDCtrl<float>(1, 0, 1);
	}
	else // down
	{
		return super::getHeadingPID();
	}
}

void move_ramp()
{
	float y, p, r;
	robot.mpu.getQuat().toYPR(y, p, r);
	bool rampDirection = p > 0; // true -> up, false -> down
	mover.move_straight(false, rampDirection ? MOVE_POWER_RAMP_UP : MOVE_POWER_RAMP_DOWN,
	                    unique_ptr<MoveDriver::MoverMoveInfo>(new MoveRampInfo(rampDirection)));
	while (!mover.isFinished())
	{}
	mover.move_straight(rampDirection ? RAMP_AFTER_DIST_UP : RAMP_AFTER_DIST_DOWN, MOVE_POWER_NORM);
	while (!mover.isFinished())
	{}
	setDirty();
}

void turn90(bool direction, bool doChecks)
{
	align(false);
	if (doChecks) // Master on switch of sorts
	{
		doChecks = currCellInfo == CurrCellInfo::InNew;
	}
	auto victim = [&direction] { ::victim(!direction); };
	bool finished = false;
	float remaining_degrees = 90;
	float last_drop_thermal;
	float max_temp = 0;
	float last_drop_visual;
	VictimData::LastDropInfo& globalLastThermalDrop = VictimData::lastThermalDrops[direction ? 0 : 1];
	VictimData::LastDropInfo& globalLastVisualDrop = VictimData::lastVisualDrops[direction ? 0 : 1];
	VisualVictimProcessing::Parser& parser = VisualVictimProcessing::parser[direction ? 0 : 1];
	auto sensor = direction ? Robot::Temp_Sensor::Left : Robot::Temp_Sensor::Right;
	{
		last_drop_thermal = globalLastThermalDrop.getDegrees() + remaining_degrees;
		globalLastThermalDrop.setDegrees(last_drop_thermal);
		last_drop_visual = globalLastVisualDrop.getDegrees() + remaining_degrees;
		globalLastVisualDrop.setDegrees(last_drop_visual);
	}
	while (!finished)
	{
		mover.turn(remaining_degrees * (direction ? 1 : -1), TURN_POWER_NORM);
		finished = true;
		while (!mover.isFinished())
		{
			if (doChecks)
			{
				if ((last_drop_thermal - remaining_degrees + abs(mover.getCurrTurn())) > TURNING_COOLDOWN_DEGREES)
				{
					float temperature = robot.getTemp(sensor);
					// Update max temperature
					if (temperature > max_temp)
					{
						max_temp = temperature;
					}
					// If turned far enough past last detected victim and max temperature is high enough, and
					// starts falling a reasonable amount - victim detected
					else if ((max_temp > MIN_TEMPERATURE) && (temperature < max_temp - TEMP_FALL_THRESHOLD))
					{
						mover.stop();
						remaining_degrees -= abs(mover.getCurrTurn());
						last_drop_thermal = remaining_degrees;
						globalLastThermalDrop.setDegrees(remaining_degrees);
						max_temp = 0;

						victim();

						finished = false;
						break;
					}
				}
				{
					PiCamInterface::VisVicType vicType = parser.isDetected();
					if ((last_drop_visual - remaining_degrees + abs(mover.getCurrTurn()) > TURNING_COOLDOWN_DEGREES)
					    && vicType != PiCamInterface::VisVicType::None)
					{
						// Stop moving and record distance still left
						mover.stop();
						remaining_degrees -= abs(mover.getCurrTurn());
						last_drop_visual = remaining_degrees;
						globalLastVisualDrop.setDegrees(remaining_degrees);

						victim();
						if (vicType == PiCamInterface::VisVicType::H)
							victim();

						finished = false;
						break;
					}
				}
			}
		}
	}
	if (doChecks && nextCellInfo == NextCellInfo::ToVisited && !testWallFront().first)
	{
		if ((last_drop_thermal > TURNING_COOLDOWN_DEGREES) && (max_temp > MIN_TEMPERATURE))
		{
			victim();
		}
	}
	setDirty();
	if (direction)
	{
		++MazeState::orientation;
	}
	else
	{
		--MazeState::orientation;
	}
	updateWalls();
}

float align(bool willMoveAfter)
{
	static float prev_correction_angle = 0;

	bool didMove = false;

	float ret = willMoveAfter ? CELL_INTER_SIZE : 0;

	cleanRR();
	{
		float s_distances[2][2], diff_dist = 0;
		{
			s_distances[0][0] = robot.getDistance(Robot::Distance_SideSensor::LeftFront);
			s_distances[0][1] = robot.getDistance(Robot::Distance_SideSensor::LeftBack);
			s_distances[1][0] = robot.getDistance(Robot::Distance_SideSensor::RightFront);
			s_distances[1][1] = robot.getDistance(Robot::Distance_SideSensor::RightBack);
		}
		bool isValid[2] = { false, false };
		float differences[2];
		for (uint8_t i = 0; i < 2; ++i)
		{
			if (s_distances[i][0] < 15 && s_distances[i][1] < 15)
			{
				isValid[i] = true;
				differences[i] = s_distances[i][1] - s_distances[i][0];
			}
		}
		float angle = 1e10; // Some extremely large number
		for (uint8_t i = 0; i < 2; ++i)
		{
			if (isValid[i])
			{
				float curr_angle = atan2(differences[i] * (i ? -1 : 1), IR_FB_DISTANCE) * 180 / 3.1415926535;
				angle = abs(curr_angle) < abs(angle) ? curr_angle : angle; // Use whichever is smaller: current angle or previous
			}
		}
		if (willMoveAfter)
		{
			if (isValid[0] && isValid[1]) // If there are 2 valid walls
			{
				diff_dist = ((s_distances[1][0] + s_distances[1][1]) - (s_distances[0][0] + s_distances[0][1])) / 2;
			}
			else if (isValid[0] != isValid[1]) // If there is 1 valid wall
			{
				for (int i = 0; i < 2; ++i)
				{
					if (isValid[i]) // Make sure we are operating on the valid side
					{
						// Get difference between measured value and expected (half of inside cell size, with robot width of 10cm subtracted)
						diff_dist = (((s_distances[i][0] + s_distances[i][1]) / 2) - ((CELL_INTRA_SIZE - 10) / 2)) * (i ? 1 : -1);
					}
				}
			}
		}
		if (abs(angle) < 15) // Angle greater than 15 is probably incorrect - don't use
		{
			float correction_angle = 0;
			if (diff_dist != 0) // If there is a valid difference (centering is possible)
			{
				diff_dist *= cos(angle * 3.1415926535 / 180);
				correction_angle = atan2(diff_dist, CELL_INTER_SIZE) * 180 / 3.1415926535;
				ret = sqrt(diff_dist * diff_dist + CELL_INTER_SIZE * CELL_INTER_SIZE);
			}

			didMove = true; // There is at least 1 valid wall at this point

			mover.turn(angle + correction_angle, 50);

			prev_correction_angle = -1 * correction_angle;
			while (!mover.isFinished())
			{}
		}
		else
		{
			didMove = prev_correction_angle != 0; // move was done if prev_correction_angle was non-zero

			mover.turn(prev_correction_angle, 50);
			while (!mover.isFinished())
			{}
			prev_correction_angle = 0;
		}
	}
	if (!willMoveAfter)
	{
		float f_distances[3];
		{
			f_distances[0] = robot.getDistance(Robot::Distance_FrontSensor::Left);
			f_distances[1] = robot.getDistance(Robot::Distance_FrontSensor::Right);
			f_distances[2] = robot.getPingDistance();
		}
		bool isValid = true;
		for (size_t i = 0; i < 3; ++i)
		{
			if (f_distances[i] > 25) // If distance is too far away, can't do align
			{
				isValid = false;
			}
		}
		if (isValid)
		{
			float f_distance = f_distances[0] + f_distances[1];
			f_distance /= 2;
			// Move to target reading of being centered (using 10cm robot length, assuming centered)
			float move_target_distance = f_distance - ((CELL_INTRA_SIZE - 10) / 2);
			if (currCellInfo == CurrCellInfo::InNew && move_target_distance > 0) // Check for victims
			{
				// (code probably won't work for negative distance)
				float remaining_distance = move_target_distance;
				bool finished = false;
				float last_drop_thermal[2];
				float max_temps[2] = { 0, 0 };
				float last_drop_visual[2];
				{
					for (uint8_t i = 0; i < 2; ++i)
					{
						last_drop_thermal[i] = VictimData::lastThermalDrops[i].getDistance() + remaining_distance;
						VictimData::lastThermalDrops[i].setDistance(last_drop_thermal[i]);
						last_drop_visual[i] = VictimData::lastVisualDrops[i].getDistance() + remaining_distance;
						VictimData::lastVisualDrops[i].setDistance(last_drop_visual[i]);
					}
				}
				while (!finished)
				{
					mover.move_straight(remaining_distance, MOVE_POWER_LOW);
					finished = true;
					while (!mover.isFinished())
					{
						float temperatures[2];
						temperatures[0] = robot.getTemp(Robot::Temp_Sensor::Left);
						temperatures[1] = robot.getTemp(Robot::Temp_Sensor::Right);
						for (uint8_t i = 0; i < 2; ++i)
						{
							if ((last_drop_thermal[i] - remaining_distance + mover.getCurrDistance()) > MOVING_COOLDOWN_DISTANCE)
							{
								// Update max temperatures
								if (temperatures[i] > max_temps[i])
								{
									max_temps[i] = temperatures[i];
								}
								// If MOVING_COOLDOWN_DISTANCE cm past last detected victim on side, max temperature is
								// high enough, and temperature starts falling a reasonable amount - victim detected
								else if ((max_temps[i] > MIN_TEMPERATURE)
								         && (temperatures[i] < max_temps[i] - TEMP_FALL_THRESHOLD))
								{
									// Stop moving and record distance still left
									mover.stop();
									remaining_distance -= mover.getCurrDistance();
									mover.clearPreviousRecords();
									last_drop_thermal[i] = remaining_distance;
									VictimData::lastThermalDrops[i].setDegrees(remaining_distance);
									max_temps[i] = 0;

									victim(i);

									finished = false;
									break;
								}
							}
							{
								PiCamInterface::VisVicType vicType = VisualVictimProcessing::parser[i].isDetected();
								if ((last_drop_visual[i] - remaining_distance + mover.getCurrDistance() > MOVING_COOLDOWN_DISTANCE)
								    && vicType != PiCamInterface::VisVicType::None)
								{
									// Stop moving and record distance still left
									mover.stop();
									remaining_distance -= mover.getCurrDistance();
									mover.clearPreviousRecords();
									last_drop_visual[i] = remaining_distance;
									VictimData::lastVisualDrops[i].setDegrees(remaining_distance);

									victim(i);
									if (vicType == PiCamInterface::VisVicType::H)
										victim(i);

									finished = false;
									break;
								}
							}
						}
						// Keep breaking out
						if (!finished)
						{
							break;
						}
					}
				}
			}
			else // Otherwise move normally
			{
				mover.move_straight(move_target_distance, MOVE_POWER_LOW);
				while (!mover.isFinished())
				{}
			}
		}
	}

	if (didMove)
		setDirty();

	return ret;
}

void check_silver()
{
	uint8_t silver_count = 0;
	for (auto reading : floorReadings)
	{
		if (reading > SILVER_THRESHOLD)
			++silver_count;
	}
	if (silver_count > (floorReadings.size() * SILVER_SAMPLING_SAMPLES_NEEDED))
	{
		float y, p, r;
		robot.mpu.getQuat().toYPR(y, p, r);
		if (abs(p) < SILVER_BUMP_THRESHOLD)
			algo.save_state();
	}
}

void updateWalls()
{
	auto walls = getWalls(false);
	using MazeState::pos;
	using MazeState::orientation;
	using MazeUtils::move_dir;
	maze[pos].setWall(orientation,     walls[0]);
	maze[pos].setWall(orientation + 1, walls[1]);
	maze[pos].setWall(orientation - 1, walls[2]);
	maze[move_dir(pos, orientation    )].setWall(orientation + 2, walls[0]);
	maze[move_dir(pos, orientation + 1)].setWall(orientation + 3, walls[1]);
	maze[move_dir(pos, orientation - 1)].setWall(orientation + 1, walls[2]);
	MazeDrawer::drawToDisplay(robot.display);
}

}

namespace HwImpl
{

bool moveToAdjacent(coord_t tile)
{
	using namespace MazeUtils;

	nextCellInfo = maze[tile].getVisited() ? NextCellInfo::ToVisited : NextCellInfo::ToNew;

	bool isThruRamp = !isAdj(MazeState::pos, tile); // Is it possible for it to be through a ramp? (not adj)
	if (isThruRamp)
	{
		isThruRamp = ::MazeUtils::isThruRamp(MazeState::pos, tile); // Make sure it actually is
		if (!isThruRamp)
			return false; // If not, fail; bad input
	}

	Direction turn_dir;
	if (!isThruRamp) // Go between tiles normally
		turn_dir = getDir(MazeState::pos, tile);
	else // Go up ramp
		turn_dir = getDirThruRamp(MazeState::pos, tile);
	turn_dir = turn_dir - MazeState::orientation;
	if (turn_dir == Direction::S)
	{
		isTurn180 = true;
	}
	if (turn_dir == Direction::N)
	{
		// Go forward
		move_cell();
		isTurn180 = false; // Clear as we are in a new cell
	}
	else
	{
		// Do turn, then recompute
		if (turn_dir == Direction::W)
		{
			turn90(false, true);
		}
		else
		{
			turn90(true, true);
		}
	}

	return true;
}

void preinitInitSolve()
{
	clear_data();
}

void preinitResumeSolve()
{
	loadKitCount();
	loadResetCount();
	++reset_count;
	saveResetCount();
}

void initUpdateWalls()
{
	currCellInfo = CurrCellInfo::InNew;
	updateWalls();
	turn90(true, true);
}

bool shouldReturn()
{
	if (reset_count >= RESET_MAX_TRIES)
	{
		return true;
	}
	if (kit_count >= KIT_MAX_DROPS)
	{
		return true;
	}
	return false;
}

const uint8_t FLASH_MAZEDATA_MAGIC = 0xD3;
void save_data(vector<uint8_t> data)
{
	robot.display.clearDisplay();
	robot.display.setTextSize(2);
	robot.display.invertDisplay(1);
	robot.display.setCursor(5, 20);
	robot.display.println("Saving");
	robot.display.display();
	robot.display.invertDisplay(0);

	robot.setLEDState(Robot::LEDIndicator::Right, true);
	FlashBank flash(1, (void*)&FLASH_MAZEDATA_BEGIN, (size_t)&FLASH_MAZEDATA_END - (size_t)&FLASH_MAZEDATA_BEGIN);
	flash.write(&flash_maze_isSaved, FLASH_MAZEDATA_MAGIC);
	flash.write(flash_maze_data, data.begin(), data.end());
	flash.commit();
	saveResetCount(0);
	robot.setLEDState(Robot::LEDIndicator::Right, false);

	robot.display.clearDisplay();
	robot.display.setTextSize(2);
	robot.display.invertDisplay(1);
	robot.display.setCursor(5, 20);
	robot.display.println("Save End");
	robot.display.display();
	robot.display.invertDisplay(0);
}

void clear_data()
{
	FlashBank flash(1, (void*)&FLASH_MAZEDATA_BEGIN, (size_t)&FLASH_MAZEDATA_END - (size_t)&FLASH_MAZEDATA_BEGIN);
	flash.write(&flash_maze_isSaved, 0);
	flash.commit();
	saveResetCount(0);
	HAL_Delay(5); // TODO: replace with batch write
	resetKitCount();
}

const vector<uint8_t> read_data()
{
	if (flash_maze_isSaved != FLASH_MAZEDATA_MAGIC)
		return vector<uint8_t>();                                             // Return empty data
	return vector<uint8_t>(flash_maze_data, flash_maze_data + sizeof(flash_maze_data));
}

uint8_t getSeed()
{
	return reset_count;
}

uint8_t buttonHandler()
{
	static bool lleft = false, lright = false, lsw = false,
	            left, right, sw;
	left = robot.getSwitchState(Robot::Switch::LeftPush);
	right = robot.getSwitchState(Robot::Switch::RightPush);
	sw = robot.getSwitchState(Robot::Switch::Slide);
	uint8_t ret = 0;
	if (lleft == true && left == false)
	{
		ret = 1;
	}
	if (lright == true && right == false)
	{
		ret = 2;
	}
	if (lsw == true && sw == false)
	{
		ret = 3;
	}
	lleft = left;
	lright = right;
	lsw = sw;
	HAL_Delay(50);
	return ret;
}

}
