#include "mazeData.h"

#include "bits.hpp"

using namespace std;

Direction MazeState::orientation;
coord_t MazeState::pos;

Direction& Direction::operator++()
{
	_dir = (dirs)((((int8_t)_dir) + 1) & 3);
	return *this;
}

Direction& Direction::operator--()
{
	_dir = (dirs)((((int8_t)_dir) - 1) & 3);
	return *this;
}

Direction Direction::operator++(int)
{
	Direction tmp = *this;
	++(*this);
	return tmp;
}

Direction Direction::operator--(int)
{
	Direction tmp = *this;
	--(*this);
	return tmp;
}

Direction Direction::operator+(const Direction& other) const
{
	return (dirs)(((int8_t)(_dir) + (int8_t)(other._dir)) & 3);
}

Direction Direction::operator-(const Direction& other) const
{
	return (dirs)(((int8_t)(_dir) - (int8_t)(other._dir)) & 3);
}

Direction Direction::operator+(int8_t count) const
{
	// TODO: consolidate w/ other operator+
	return (dirs)(((int8_t)(_dir) + count) & 3);
}

Direction Direction::operator-(int8_t count) const
{
	// TODO: consolidate w/ other operator-
	return (dirs)(((int8_t)(_dir) - count) & 3);
}

bool Direction::operator==(const Direction& other) const
{
	return _dir == other._dir;
}

bool Direction::operator!=(const Direction& other) const
{
	return !(other == *this);
}

vector<uint8_t> Maze::serialize() const
{
	vector<uint8_t> ret;
	ret.resize(2); // Reserve for size bytes
	uint16_t size = 0;
	for (pair<coord_t, Cell> dat : data)
	{
		auto cell_data = dat.second.serialize();
		ret.push_back(dat.first.first.first);
		ret.push_back(dat.first.first.second);
		ret.push_back(dat.first.second);
		++size;
		ret.insert(ret.end(), cell_data.begin(), cell_data.end());
	}
	ret[0] = (uint8_t)(size & 0xff); // Low bit
	ret[1] = (uint8_t)((size >> 8) & 0xff); // High bit
	return ret;
}

vector<uint8_t>::const_iterator Maze::deserialize(vector<uint8_t>::const_iterator it)
{
	data.clear();
	size_t size = 0;
	size |= *it++;
	size |= (*it++) << 8;
	for (size_t i = 0; i < size; ++i)
	{
		coord_t cd;
		cd.first.first  = *it++;
		cd.first.second = *it++;
		cd.second       = *it++;
		Cell cl; it = cl.deserialize(it);
		data[cd] = cl;
	}
	return it;
}

vector<uint8_t> Maze::Cell::serialize() const
{
	vector<uint8_t> ret(2);
	ret[0] = (uint8_t)(data & 0xff);
	ret[1] = (uint8_t)((data >> 8) & 0xff);
	return ret;
}

vector<uint8_t>::const_iterator Maze::Cell::deserialize(vector<uint8_t>::const_iterator it)
{
	data = 0;
	data |= *it++;
	data |= *it++ << 8;
	return it;
}

Maze::Cell::Wall_state Maze::Cell::getWall(Direction direction) const
{
	if (!(data & (1 << (direction.toEnum() + 4)))) // Check unknown bit
	{
		return Wall_state::Unknown;
	}
	return (data & (1 << direction.toEnum())) ? Wall_state::Wall : Wall_state::NoWall;
}

void Maze::Cell::setWall(Direction direction, Wall_state state)
{
	data = writeBit(data, direction.toEnum(), state == Wall_state::Wall);
	data = writeBit(data, direction.toEnum() + 4, state != Wall_state::Unknown);

	// If all walls of a cell are empty, then mark as visited
	if (state == Wall_state::NoWall)
	{
		//           known mask                             walls mask
		if (((data & 0b11110000) == 0b11110000) && !(data & 0b00001111))
		{
			setVisited(true);
		}
	}
}

bool Maze::Cell::getVisited() const
{
	return data & (1 << 8);
}

void Maze::Cell::setVisited(bool state)
{
	data = writeBit(data, 8, state);
}

bool Maze::Cell::getBlack() const
{
	return data & (1 << 9);
}

void Maze::Cell::setBlack(bool state)
{
	data = writeBit(data, 9, state);
}

bool Maze::Cell::getRamp() const
{
	return data & (1 << 10);
}

void Maze::Cell::setRamp(bool state)
{
	data = writeBit(data, 10, state);
}

bool Maze::Cell::getRampDirection() const
{
	return data & (1 << 11);
}

void Maze::Cell::setRampDirection(bool direction)
{
	data = writeBit(data, 11, direction);
}

Maze& Maze::getInstance()
{
	static Maze maze;
	return maze;
}
