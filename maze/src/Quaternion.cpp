/*
 * Quaternion.cpp
 *
 *  Created on: Aug 26, 2015
 *      Author: ethan
 */

#include "Quaternion.h"
#include <cmath>

void Quaternion::toYPR(float& y, float& p, float& r)
{
	y = atan2(2.0f * (q1 * q2 + q0 * q3), q0 * q0 + q1 * q1 - q2 * q2 - q3 * q3);
	p = -asin(2.0f * (q1 * q3 - q0 * q2));
	r = atan2(2.0f * (q0 * q1 + q2 * q3), q0 * q0 - q1 * q1 - q2 * q2 + q3 * q3);
	y *= 57.29577951308232;
	p *= 57.29577951308232;
	r *= 57.29577951308232;
}
