#include "mpu.h"
#include "bits.h"

Mpu::Mpu(I2C_HandleTypeDef* hi2c_, uint8_t addr_) : AHRS(MadgwickAHRS(0)), isStarted(false), hi2c(hi2c_), addr(addr_)
{}

bool Mpu::init(uint16_t offs_ax, uint16_t offs_ay, uint16_t offs_az,
               uint16_t offs_gx, uint16_t offs_gy, uint16_t offs_gz)
{
	uint16_t ret = 0;
	uint8_t buf[12];
	// Wake up MPU
	ret |= HAL_I2C_Mem_Read(&hi2c2, 0x68 << 1, 0x6B, 1, buf, 1, 0xff);
	buf[0] = resetBit(buf[0], 6);
	ret |= HAL_I2C_Mem_Write(&hi2c2, 0x68 << 1, 0x6B, 1, buf, 1, 0xff);
	// Write offsets
	{
		buf[0]  = (uint8_t)(offs_ax >> 8);
		buf[1]  = (uint8_t)(offs_ax & 0xff);
		buf[2]  = (uint8_t)(offs_ay >> 8);
		buf[3]  = (uint8_t)(offs_ay & 0xff);
		buf[4]  = (uint8_t)(offs_az >> 8);
		buf[5]  = (uint8_t)(offs_az & 0xff);
		buf[6]  = (uint8_t)(offs_gx >> 8);
		buf[7]  = (uint8_t)(offs_gx & 0xff);
		buf[8]  = (uint8_t)(offs_gy >> 8);
		buf[9]  = (uint8_t)(offs_gy & 0xff);
		buf[10] = (uint8_t)(offs_gz >> 8);
		buf[11] = (uint8_t)(offs_gz & 0xff);
	}
	ret |= HAL_I2C_Mem_Write(&hi2c2, 0x68 << 1, 0x06, 1, buf, 6, 0xff);
	ret |= HAL_I2C_Mem_Write(&hi2c2, 0x68 << 1, 0x13, 1, buf + 6, 6, 0xff);
	if (!ret)
	{
		isStarted = true;
	}
	return !ret;
}

Quaternion Mpu::getQuat()
{
	return AHRS.getQuat();
}

bool Mpu::updateMPU()
{
	static bool isFirst = true;
	if (!isStarted)
	{
		return false;
	}
	if (isFirst)
	{
		// Don't do anything but log current time so there is
		// no bad time interval caused by last_time == 0
		isFirst = false;
		last_time = htim6.Instance->CNT;
		return true;
	}
	volatile float gx, gy, gz, ax, ay, az;
	uint8_t buf[14];
	HAL_I2C_Mem_Read(&hi2c2, 0x68 << 1, 0x3B, 1, buf, 14, 0xff);
	ax = ((int16_t)((buf[0] << 8) | buf[1])) / 16384.0;
	ay = ((int16_t)((buf[2] << 8) | buf[3])) / -16384.0;
	az = ((int16_t)((buf[4] << 8) | buf[5])) / 16384.0;
	gx = ((int16_t)((buf[8] << 8) | buf[9])) / -3754.936206276687; //* (PI / 180) * (500 / 32768)
	gy = ((int16_t)((buf[10] << 8) | buf[11])) / 3754.936206276687; //* (PI / 180) * (500 / 32768)
	gz = ((int16_t)((buf[12] << 8) | buf[13])) / -3754.936206276687; //* (PI / 180) * (500 / 32768)
	uint16_t time = htim6.Instance->CNT;
	AHRS.setFreq(1000000.0 / (uint16_t)(time - last_time));
	AHRS.update(gx, gy, gz, ax, ay, az);
	last_time = time;
	return true;
}
