#include "timer_defs.h"

uint16_t tim9_period;
uint16_t tim9_pulse1;
uint16_t tim9_pulse2;

void init_timerdefs(void)
{
	tim9_period = (SystemCoreClock / 20000) - 1;
	tim9_pulse1 = 0;
	tim9_pulse2 = 0;
}
