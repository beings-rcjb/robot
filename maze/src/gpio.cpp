#include "gpio.h"

void GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, bool state)
{
	// Check the parameters
	assert_param(IS_GPIO_PIN(GPIO_Pin));

	if (state)
	{
		GPIOx->BSRR = GPIO_Pin;
	}
	else
	{
		GPIOx->BSRR = (uint32_t)GPIO_Pin << 16;
	}
}

bool GPIO_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	// Check the parameters
	assert_param(IS_GPIO_PIN(GPIO_Pin));

	return (GPIOx->IDR & GPIO_Pin) != (uint32_t)GPIO_PIN_RESET;
}
