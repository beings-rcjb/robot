#include "PiCamInterface.h"

#include <functional>

using namespace std;

PiCamInterface::PiCamInterface(PiInterface& device) : device(device)
{
	using namespace std::placeholders;
	device.registerHandler(CHANNEL_ID, std::bind(&PiCamInterface::dataHandler, this, _1));
}

void PiCamInterface::dataHandler(std::vector<uint8_t> data)
{
	VisVicType type;
	bool side;
	if (data.size() != 1)
		return;
	type = static_cast<VisVicType>(data[0] & 0x03);
	side = static_cast<bool>(data[0] & 0x04);
	valueAcquiredCallbackManager.call(std::make_pair(type, side));
}
