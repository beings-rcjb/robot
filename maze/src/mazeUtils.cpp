#include "mazeUtils.h"

#include <stdexcept>
#include <cmath>

using namespace std;
using namespace MazeState;

Maze& maze = Maze::getInstance();

coord_t MazeUtils::move_dir(coord_t start, Direction dir)
{
	switch (dir.toEnum())
	{
	case Direction::N:
		--start.first.second;
		break;
	case Direction::E:
		++start.first.first;
		break;
	case Direction::S:
		++start.first.second;
		break;
	case Direction::W:
		--start.first.first;
		break;
	default:
		throw invalid_argument("invalid dir");
	}
	return start;
}

// move_dir and follow ramps
coord_t MazeUtils::move_dir_follow(coord_t start, Direction dir)
{
	start = move_dir(start, dir);
	const Maze::Cell& cell_data = maze[start];
	if (cell_data.getRamp()) // this is a ramp cell
	{
		bool r_direction = cell_data.getRampDirection();
		start.second += r_direction ? 1 : -1;
		start = move_dir(start, dir);
	}
	return start;
}

Direction MazeUtils::getDir(coord_t a, coord_t b)
{
	if (((b.first.first - a.first.first) ==  1) && ((b.first.second - a.first.second) ==  0))
	{
		return Direction::E;
	}
	if (((b.first.first - a.first.first) ==  0) && ((b.first.second - a.first.second) ==  1))
	{
		return Direction::S;
	}
	if (((b.first.first - a.first.first) == -1) && ((b.first.second - a.first.second) ==  0))
	{
		return Direction::W;
	}
	if (((b.first.first - a.first.first) ==  0) && ((b.first.second - a.first.second) == -1))
	{
		return Direction::N;
	}
	throw invalid_argument("not adjacent");
}

bool MazeUtils::isAdj(coord_t a, coord_t b)
{
	if (a.second != b.second)
	{
		return false;
	}
	if ((abs(a.first.first - b.first.first) + abs(a.first.second - b.first.second)) == 1)
	{
		return maze[pos].getWall(getDir(a, b)) == Maze::Cell::Wall_state::NoWall;
	}
	return false;
}

Direction MazeUtils::getDirThruRamp(coord_t a, coord_t b)
{
	if (((b.first.first - a.first.first) ==  2) && ((b.first.second - a.first.second) ==  0))
	{
		return Direction::E;
	}
	if (((b.first.first - a.first.first) ==  0) && ((b.first.second - a.first.second) ==  2))
	{
		return Direction::S;
	}
	if (((b.first.first - a.first.first) == -2) && ((b.first.second - a.first.second) ==  0))
	{
		return Direction::W;
	}
	if (((b.first.first - a.first.first) ==  0) && ((b.first.second - a.first.second) == -2))
	{
		return Direction::N;
	}
	throw invalid_argument("not adjacent thru ramp");
}

bool MazeUtils::isThruRamp(coord_t a, coord_t b)
{
	if (abs(a.second - b.second) != 1)
		return false;
	int8_t dir = -1;
	if (((b.first.first - a.first.first) ==  2) && ((b.first.second - a.first.second) ==  0))
	{
		dir = 1;
	}
	if (((b.first.first - a.first.first) ==  0) && ((b.first.second - a.first.second) ==  2))
	{
		dir = 2;
	}
	if (((b.first.first - a.first.first) == -2) && ((b.first.second - a.first.second) ==  0))
	{
		dir = 3;
	}
	if (((b.first.first - a.first.first) ==  0) && ((b.first.second - a.first.second) == -2))
	{
		dir = 0;
	}
	if (dir == -1)
	{
		return false;
	}
	coord_t ramp = a;
	switch (dir)
	{
	case 0:
		--ramp.first.second;
		break;
	case 1:
		++ramp.first.first;
		break;
	case 2:
		++ramp.first.second;
		break;
	case 3:
		--ramp.first.first;
		break;
	}
	if (!(maze[ramp].getRamp())) // Make sure the tile is a ramp
	{
		return false;
	}
	ramp.second = b.second; // Move to b level
	if (!(maze[ramp].getRamp())) // Make sure the tile is a ramp
	{
		return false;
	}
	return true;
}
