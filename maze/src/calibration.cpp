#include "calibration.h"

#include "hardwaredefs.h"

#include <cstdio>
#include "includes.h"
#include "robot.h"
#include "disp_menu.h"
#include "hw_impl.h"


// CONSTANTS
ld_section(".flash-config") volatile const float MIN_TEMPERATURE = 25;

ld_section(".flash-config") volatile const float BLACK_THRESHOLD = 0.15;

ld_section(".flash-config") volatile const float SILVER_THRESHOLD = 0.85;
ld_section(".flash-config") volatile const float SILVER_SAMPLING_SAMPLES_NEEDED = 0.25;

ld_section(".flash-config") volatile const float CELL_INTER_SIZE = 30;
ld_section(".flash-config") volatile const float CELL_INTRA_SIZE = 30;


#define __calibrate_wrap(x) [] (void* n) {x(); return false; }

// Flash constants defined in linkerscript
extern size_t FLASH_CONFIGDATA_BEGIN;
extern size_t FLASH_CONFIGDATA_END;
extern size_t FLASH_MAZEDATA_BEGIN;
extern size_t FLASH_MAZEDATA_END;


const size_t BUF_LEN = 50;

namespace calibrate
{

using HwImpl::buttonHandler;

void dispBatt()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset

	char buf[BUF_LEN];
	while (!(robot.getSwitchState(Robot::Switch::LeftPush) || robot.getSwitchState(Robot::Switch::RightPush)))
	{
		const uint16_t avg_cnt = 500;

		float batt = 0;
		for (uint16_t i = 0; i < avg_cnt; ++i)
		{
			batt += robot.getBatteryVoltage();
			HAL_Delay(1);
		}
		batt /= avg_cnt;

		display.clearDisplay();

		display.setCursor(0, 20);
		snprintf(buf, BUF_LEN, "Battery: %5.3fV", batt);
		display.println(buf);

		display.display();
	}
	while (robot.getSwitchState(Robot::Switch::LeftPush) || robot.getSwitchState(Robot::Switch::RightPush))
	{}
}

void shutdownPi()
{
	bool status = robot.piInterface.send(0x01, {0x0F});
	robot.display.clearDisplay();
	robot.display.setCursor(0, 20);
	robot.display.println(status ? "OK" : "FAIL");
	robot.display.display();
	HAL_Delay(250);
}

void dispReadings()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset

	char buf[BUF_LEN];
	while (!(robot.getSwitchState(Robot::Switch::LeftPush) || robot.getSwitchState(Robot::Switch::RightPush)))
	{
		display.clearDisplay();

		display.setCursor(0, 0);
		snprintf(buf, BUF_LEN, "LF:%3d FL:%3d RF:%3d", (int)(robot.getDistance(Robot::Distance_SideSensor::LeftFront) * 10),
		         (int)(robot.getDistance(Robot::Distance_FrontSensor::Left) * 10),
		         (int)(robot.getDistance(Robot::Distance_SideSensor::RightFront) * 10));
		display.println(buf);
		snprintf(buf, BUF_LEN, "LB:%3d FR:%3d RB:%3d", (int)(robot.getDistance(Robot::Distance_SideSensor::LeftBack) * 10),
		         (int)(robot.getDistance(Robot::Distance_FrontSensor::Right) * 10),
		         (int)(robot.getDistance(Robot::Distance_SideSensor::RightBack) * 10));
		display.println(buf);
		snprintf(buf, BUF_LEN, "PING:%3d", (int)(robot.getPingDistance() * 10));
		display.println(buf);
		display.print('\n');

		snprintf(buf, BUF_LEN, "TMP: L: %3d R:%3d", (int)(robot.getTemp(Robot::Temp_Sensor::Left) * 10),
		         (int)(robot.getTemp(Robot::Temp_Sensor::Right) * 10));
		display.println(buf);
		snprintf(buf, BUF_LEN, "FL: %3d", (int)(robot.getFloorLight() * 100));
		display.println(buf);
		display.print('\n');

		float y, p, r;
		robot.mpu.getQuat().toYPR(y, p, r);
		snprintf(buf, BUF_LEN, "Y%4d P%4d R%4d", (int)y, (int)p, (int)r);
		display.println(buf);

		display.display();

		HAL_Delay(500);
	}
	while (robot.getSwitchState(Robot::Switch::LeftPush) || robot.getSwitchState(Robot::Switch::RightPush))
	{}
}

int16_t
    offs_ax = MPU_OFFS_AX,
    offs_ay = MPU_OFFS_AY,
    offs_az = MPU_OFFS_AZ,
    offs_gx = MPU_OFFS_GX,
    offs_gy = MPU_OFFS_GY,
    offs_gz = MPU_OFFS_GZ;

void calMpu()
{
	SSD1306& display = robot.display;
	for (uint8_t i = 3; i > 0; --i)
	{
		display.clearDisplay();
		display.setTextSize(1);
		display.setCursor(0, 0);
		display.println("Do not disturb robot");
		display.println("Starting in");
		display.setTextSize(2);
		display.println(i);
		display.setTextSize(1);
		display.display();
		HAL_Delay(1000);
	}

	const uint8_t AVERAGE_SIZE = 64, ITERATION_COUNT = 5;
	for (uint8_t i = 0; i < ITERATION_COUNT; ++i)
	{
		int16_t offs_ax_orig, offs_ay_orig, offs_az_orig, offs_gx_orig, offs_gy_orig, offs_gz_orig;
		int32_t ax_sum, ay_sum, az_sum, gx_sum, gy_sum, gz_sum;
		int16_t ax_avg, ay_avg, az_avg, gx_avg, gy_avg, gz_avg;

		ax_sum = ay_sum = az_sum = gx_sum = gy_sum = gz_sum = 0; // clear out sums

		uint8_t buf[12];
		// Read offsets
		HAL_I2C_Mem_Read(&hi2c2, 0x68 << 1, 0x06, 1, buf, 6, 0xff);
		HAL_I2C_Mem_Read(&hi2c2, 0x68 << 1, 0x13, 1, buf + 6, 6, 0xff);
		offs_ax_orig = ((int16_t)((buf[0] << 8) | buf[1]));
		offs_ay_orig = ((int16_t)((buf[2] << 8) | buf[3]));
		offs_az_orig = ((int16_t)((buf[4] << 8) | buf[5]));
		offs_gx_orig = ((int16_t)((buf[6] << 8) | buf[7]));
		offs_gy_orig = ((int16_t)((buf[8] << 8) | buf[9]));
		offs_gz_orig = ((int16_t)((buf[10] << 8) | buf[11]));
		for (uint8_t j = 0; j < AVERAGE_SIZE; ++j)
		{
			// Read measurements
			HAL_I2C_Mem_Read(&hi2c2, 0x68 << 1, 0x3B, 1, buf, 6, 0xff);
			HAL_I2C_Mem_Read(&hi2c2, 0x68 << 1, 0x43, 1, buf + 6, 6, 0xff);
			ax_sum += ((int16_t)((buf[0] << 8) | buf[1]));
			ay_sum += ((int16_t)((buf[2] << 8) | buf[3]));
			az_sum += ((int16_t)((buf[4] << 8) | buf[5]));
			gx_sum += ((int16_t)((buf[6] << 8) | buf[7]));
			gy_sum += ((int16_t)((buf[8] << 8) | buf[9]));
			gz_sum += ((int16_t)((buf[10] << 8) | buf[11]));

			display.clearDisplay();
			display.setTextSize(1);
			display.setCursor(0, 0);
			display.println("Do not disturb robot");
			float pct = (i * AVERAGE_SIZE + j) / (float)(ITERATION_COUNT * AVERAGE_SIZE);
			char strbuf[20];
			display.setCursor(10, 20);
			snprintf(strbuf, 20, "%3d%%", (uint8_t)(pct * 100));
			display.setTextSize(2);
			display.println(strbuf);
			display.fillRect(0, display.height() - 2, display.width() * pct, 2, WHITE);

			display.display();

			HAL_Delay(100);
		}
		ax_avg = ax_sum / AVERAGE_SIZE;
		ay_avg = ay_sum / AVERAGE_SIZE;
		az_avg = az_sum / AVERAGE_SIZE;
		gx_avg = gx_sum / AVERAGE_SIZE;
		gy_avg = gy_sum / AVERAGE_SIZE;
		gz_avg = gz_sum / AVERAGE_SIZE;

		// Calculate new offsets
		offs_ax = offs_ax_orig - (ax_avg / 8.0);
		offs_ay = offs_ay_orig - (ay_avg / 8.0);
		offs_az = offs_az_orig - ((az_avg - 16384) / 8.0);
		offs_gx = offs_gx_orig - (gx_avg / 4.0);
		offs_gy = offs_gy_orig - (gy_avg / 4.0);
		offs_gz = offs_gz_orig - (gz_avg / 4.0);

		// Set new offsets
		buf[0]  = (uint8_t)(offs_ax >> 8);
		buf[1]  = (uint8_t)(offs_ax & 0xff);
		buf[2]  = (uint8_t)(offs_ay >> 8);
		buf[3]  = (uint8_t)(offs_ay & 0xff);
		buf[4]  = (uint8_t)(offs_az >> 8);
		buf[5]  = (uint8_t)(offs_az & 0xff);
		buf[6]  = (uint8_t)(offs_gx >> 8);
		buf[7]  = (uint8_t)(offs_gx & 0xff);
		buf[8]  = (uint8_t)(offs_gy >> 8);
		buf[9]  = (uint8_t)(offs_gy & 0xff);
		buf[10] = (uint8_t)(offs_gz >> 8);
		buf[11] = (uint8_t)(offs_gz & 0xff);
		HAL_I2C_Mem_Write(&hi2c2, 0x68 << 1, 0x06, 1, buf, 6, 0xff);
		HAL_I2C_Mem_Write(&hi2c2, 0x68 << 1, 0x13, 1, buf + 6, 6, 0xff);
	}

	display.clearDisplay();
	display.setTextSize(2);
	display.setCursor(0, 0);
	display.print("Done!");
	display.setTextSize(1);
	display.display();
	HAL_Delay(500);
}

float set_temp = MIN_TEMPERATURE;

void calTemp()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset


	char buf[BUF_LEN];
	uint32_t next = HAL_GetTick();
	float cl, cr;
	float inc = 1;
	while (inc >= 0.1)
	{
		char actCode = buttonHandler();
		if (actCode == 1)
		{
			set_temp += inc;
		}
		if (actCode == 2)
		{
			set_temp -= inc;
		}
		if (actCode == 3)
		{
			inc /= 10;
		}

		// Display
		display.clearDisplay();

		// Set temp
		display.setCursor(0, 0);
		snprintf(buf, BUF_LEN, "Set: %4.1f", set_temp);
		//                 Set: xx.x
		//                 012345678
		display.println(buf);
		display.print('\n');

		// Current temp
		if (HAL_GetTick() >= next)
		{
			cl = robot.getTemp(Robot::Temp_Sensor::Left);
			cr = robot.getTemp(Robot::Temp_Sensor::Right);
			next += 500;
		}
		snprintf(buf, BUF_LEN, "L: %4.1f R: %4.1f", cl, cr);
		display.println(buf);

		// Digit underline
		display.setCursor(0, 2);
		snprintf(buf, BUF_LEN, "                           "); // Pad the string with spaces
		if (inc == 1)
		{
			buf[6] = '_';
		}
		if (inc == 0.1)
		{
			buf[8] = '_';
		}
		display.print(buf);

		display.display();
	}
}

float new_turn_offs[2] = { 1, 1 };

void calTurn()
{
	using namespace Graph;

	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);

	Menu menu(display, buttonHandler, WHITE);
	menu.addItem("Auto calibrate", [] (void* a)->bool {
	                 SSD1306& display = robot.display;

	                 float y, p, r;

	                 HAL_Delay(100); // Wait for buttons to reset

	                 uint8_t actCode;
	                 while ((actCode = buttonHandler()) != 3)
	                 {
	                     display.clearDisplay();
	                     display.setCursor(0, 0);
	                     display.print("Press button to begin");
	                     display.display();

	                     int8_t direction = 0;
	                     switch (actCode)
	                     {
						 case 1:
							 direction = -1;
							 break;
						 case 2:
							 direction = 1;
							 break;
						 default:
							 break;
						 }
	                     if (direction == 0)
							 continue;
	                     robot.mpu.getQuat().toYPR(y, p, r);
	                     float start = y;

	                     {
	                         display.clearDisplay();
	                         display.setCursor(0, 0);
	                         display.print("Turn robot ");
	                         if (direction == 1)
	                         {
	                             display.println("right");
							 }
	                         if (direction == -1)
	                         {
	                             display.println("left");
							 }
	                         display.print("90 degrees and then\npress any button");
	                         display.display();
						 }

	                     while (buttonHandler() == 0)
	                     {}
	                     robot.mpu.getQuat().toYPR(y, p, r);
	                     float delta = (y - start) * direction;
	                     new_turn_offs[(direction > 0 ? 0 : 1)] = delta / 90;

	                     {
	                         display.clearDisplay();
	                         display.setCursor(0, 0);
	                         char buf[100];
	                         snprintf(buf, 30, "Done!\n%05.3f\n", delta / 90);
	                         display.print(buf);
	                         display.display();
	                         HAL_Delay(1000);
						 }
					 }
	                 return false;
				 });
	menu.addItem("Reset values", [] (void* a)->bool {
	                 SSD1306& display = robot.display;

	                 new_turn_offs[0] = 1;
	                 new_turn_offs[1] = 1;

	                 display.clearDisplay();
	                 display.setCursor(0, 0);
	                 display.println("Done!");
	                 display.display();
	                 HAL_Delay(500);
	                 return true;
				 });
	menu.addExitItem("Back");
	menu.show();
}

float set_black = BLACK_THRESHOLD;

void calBlack()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset


	char buf[BUF_LEN];
	uint32_t next = HAL_GetTick();
	float curr;
	float inc = 0.1;
	while (inc >= 0.01)
	{
		char actCode = buttonHandler();
		if (actCode == 1)
		{
			set_black += inc;
		}
		if (actCode == 2)
		{
			set_black -= inc;
		}
		if (actCode == 3)
		{
			inc /= 10;
		}

		if (set_black > 1)
			set_black = 1;
		if (set_black < 0)
			set_black = 0;

		// Display
		display.clearDisplay();

		// Set black level
		display.setCursor(0, 0);
		snprintf(buf, BUF_LEN, "Set: %3d%%", (int)round(set_black * 100));
		//                 Set: xxx%
		//                 012345678
		display.println(buf);
		display.print('\n');

		// Current light level
		if (HAL_GetTick() >= next)
		{
			curr = robot.getFloorLight();
			next += 500;
		}
		snprintf(buf, BUF_LEN, "Current: %3d%%", (int)(curr * 100));
		display.println(buf);

		// Digit underline
		display.setCursor(0, 2);
		snprintf(buf, BUF_LEN, "                           "); // Pad the string with spaces
		if (inc == 0.1)
		{
			buf[6] = '_';
		}
		if (inc == 0.01)
		{
			buf[7] = '_';
		}
		display.print(buf);

		display.display();
	}
}

float set_silver_thresh = SILVER_THRESHOLD;

void calSilverThresh()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset


	char buf[BUF_LEN];
	uint32_t next = HAL_GetTick();
	float curr;
	float inc = 0.1;
	while (inc >= 0.01)
	{
		char actCode = buttonHandler();
		if (actCode == 1)
		{
			set_silver_thresh += inc;
		}
		if (actCode == 2)
		{
			set_silver_thresh -= inc;
		}
		if (actCode == 3)
		{
			inc /= 10;
		}

		if (set_silver_thresh > 1)
			set_silver_thresh = 1;
		if (set_silver_thresh < 0)
			set_silver_thresh = 0;

		// Display
		display.clearDisplay();

		// Set silver level
		display.setCursor(0, 0);
		snprintf(buf, BUF_LEN, "Set: %3d%%", (int)round(set_silver_thresh * 100));
		//                 Set: xxx%
		//                 012345678
		display.println(buf);
		display.print('\n');

		// Current light level
		if (HAL_GetTick() >= next)
		{
			curr = robot.getFloorLight();
			next += 500;
		}
		snprintf(buf, BUF_LEN, "Current: %3d%%", (int)(curr * 100));
		display.println(buf);

		// Digit underline
		display.setCursor(0, 2);
		snprintf(buf, BUF_LEN, "                           "); // Pad the string with spaces
		if (inc == 0.1)
		{
			buf[6] = '_';
		}
		if (inc == 0.01)
		{
			buf[7] = '_';
		}
		display.print(buf);

		display.display();
	}
}

float set_silver_samples = SILVER_SAMPLING_SAMPLES_NEEDED;

void calSilverSamples()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset


	char buf[BUF_LEN];
	float inc = 0.1;
	while (inc >= 0.01)
	{
		char actCode = buttonHandler();
		if (actCode == 1)
		{
			set_silver_samples += inc;
		}
		if (actCode == 2)
		{
			set_silver_samples -= inc;
		}
		if (actCode == 3)
		{
			inc /= 10;
		}

		if (set_silver_samples > 1)
			set_silver_samples = 1;
		if (set_silver_samples < 0)
			set_silver_samples = 0;

		// Display
		display.clearDisplay();

		// Set silver samples level
		display.setCursor(0, 0);
		snprintf(buf, BUF_LEN, "Set: %3d%%", (int)round(set_silver_samples * 100));
		//                 Set: xxx%
		//                 012345678
		display.println(buf);
		display.print('\n');


		// Digit underline
		display.setCursor(0, 2);
		snprintf(buf, BUF_LEN, "                           "); // Pad the string with spaces
		if (inc == 0.1)
		{
			buf[6] = '_';
		}
		if (inc == 0.01)
		{
			buf[7] = '_';
		}
		display.print(buf);

		display.display();
	}
}

void calSilver()
{
	using namespace Graph;
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);

	Menu menu(display, buttonHandler, WHITE);
	menu.addItem("Silver threshold", __calibrate_wrap(calSilverThresh));
	menu.addItem("Sampling threshold", __calibrate_wrap(calSilverSamples));
	menu.addExitItem("Back");
	menu.show();
}

float set_cell_inter = CELL_INTER_SIZE;

void calCellInter()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset


	char buf[BUF_LEN];
	float inc = 1;
	while (inc >= 0.1)
	{
		char actCode = buttonHandler();
		if (actCode == 1)
		{
			set_cell_inter += inc;
		}
		if (actCode == 2)
		{
			set_cell_inter -= inc;
		}
		if (actCode == 3)
		{
			inc /= 10;
		}

		// Display
		display.clearDisplay();

		// Set temp
		display.setCursor(0, 0);
		snprintf(buf, BUF_LEN, "Set: %4.1f", set_cell_inter);
		//                 Set: xx.x
		//                 012345678
		display.println(buf);
		display.print('\n');

		// Digit underline
		display.setCursor(0, 2);
		snprintf(buf, BUF_LEN, "                           "); // Pad the string with spaces
		if (inc == 1)
		{
			buf[6] = '_';
		}
		if (inc == 0.1)
		{
			buf[8] = '_';
		}
		display.print(buf);

		display.display();
	}
}

float set_cell_intra = CELL_INTRA_SIZE;

void calCellIntra()
{
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.display();
	HAL_Delay(100); // Wait for buttons to reset


	char buf[BUF_LEN];
	float inc = 1;
	while (inc >= 0.1)
	{
		char actCode = buttonHandler();
		if (actCode == 1)
		{
			set_cell_intra += inc;
		}
		if (actCode == 2)
		{
			set_cell_intra -= inc;
		}
		if (actCode == 3)
		{
			inc /= 10;
		}

		// Display
		display.clearDisplay();

		// Set temp
		display.setCursor(0, 0);
		snprintf(buf, BUF_LEN, "Set: %4.1f", set_cell_intra);
		//                 Set: xx.x
		//                 012345678
		display.println(buf);
		display.print('\n');

		// Digit underline
		display.setCursor(0, 2);
		snprintf(buf, BUF_LEN, "                           "); // Pad the string with spaces
		if (inc == 1)
		{
			buf[6] = '_';
		}
		if (inc == 0.1)
		{
			buf[8] = '_';
		}
		display.print(buf);

		display.display();
	}
}

void calCellDistances()
{
	Graph::Menu menu(robot.display, buttonHandler, WHITE);

	menu.addItem("Inter-cell dist", __calibrate_wrap(calCellInter));
	menu.addItem("Intra-cell dist", __calibrate_wrap(calCellIntra));

	menu.addExitItem("Back");

	menu.show();
}

void clearMaze()
{
	robot.display.clearDisplay();
	robot.display.setTextSize(1);
	robot.display.setCursor(0, 15);
	robot.display.println("Press both push");
	robot.display.println("buttons to clear");
	robot.display.display();

	{
		bool buttonsPressed = false;
		while (true)
		{
			{
				uint8_t state = buttonHandler();
				if (state == 3 || state == 4) // action Select or Exit - cancel operation
				{
					robot.display.print("Canceled!");
					robot.display.display();
					HAL_Delay(250);
					return;
				}
			}

			if (!buttonsPressed)
			{
				if (robot.getSwitchState(Robot::Switch::LeftPush) // if both buttons pressed, set flag
				    && robot.getSwitchState(Robot::Switch::RightPush))
					buttonsPressed = true;
			}
			else
			{
				if (!robot.getSwitchState(Robot::Switch::LeftPush) // if both buttons released, continue
				    && !robot.getSwitchState(Robot::Switch::RightPush))
					break;
			}
		}
		buttonHandler(); // clear button event
	}

	robot.display.clearDisplay();
	robot.display.setCursor(0, 20);
	robot.display.print("Clearing... ");
	robot.display.display();

	HwImpl::clear_data();

	robot.display.println("OK");
	robot.display.display();
	HAL_Delay(250);
}

void showValues()
{
	const uint8_t NUM_PAGES = 3;
	SSD1306& display = robot.display;
	display.clearDisplay();
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setTextWrap(false);
	display.setCursor(0, 0);

	uint8_t page = 0;
	uint8_t actCode;
	while ((actCode = buttonHandler()) != 3)
	{
		switch (actCode)
		{
		case 1:
			++page;
			page %= NUM_PAGES;
			break;
		case 2:
			--page;
			if (page == 255)
				page += NUM_PAGES;
			break;
		}
		display.clearDisplay();
		display.setCursor(0, 0);
		char buf[BUF_LEN];
		switch (page)
		{
		case 0:
			display.println("MPU Offsets:");
			snprintf(buf, BUF_LEN, "AX: %d", offs_ax);
			display.println(buf);
			snprintf(buf, BUF_LEN, "AY: %d", offs_ay);
			display.println(buf);
			snprintf(buf, BUF_LEN, "AZ: %d", offs_az);
			display.println(buf);
			snprintf(buf, BUF_LEN, "GX: %d", offs_gx);
			display.println(buf);
			snprintf(buf, BUF_LEN, "GY: %d", offs_gy);
			display.println(buf);
			snprintf(buf, BUF_LEN, "GZ: %d", offs_gz);
			display.println(buf);
			break;
		case 1:
			snprintf(buf, BUF_LEN, "Temp: %4.1f", set_temp);
			display.println(buf);
			display.print('\n');
			snprintf(buf, BUF_LEN, "Black: %4.2f", set_black);
			display.println(buf);
			snprintf(buf, BUF_LEN, "Silver: %4.2f", set_silver_thresh);
			display.println(buf);
			snprintf(buf, BUF_LEN, "  Samples: %4.2f", set_silver_samples);
			display.println(buf);
			display.print('\n');
			snprintf(buf, BUF_LEN, "Turn 0: %5.3f\n     1: %5.3f", new_turn_offs[0], new_turn_offs[1]);
			display.println(buf);
			break;
		case 2:
			display.println("Cell dists:");
			snprintf(buf, BUF_LEN, " Inter: %4.1f", set_cell_inter);
			display.println(buf);
			snprintf(buf, BUF_LEN, " Intra: %4.1f", set_cell_intra);
			display.println(buf);
		}
		display.display();
	}
}

void save()
{
	robot.display.clearDisplay();
	robot.display.setTextSize(1);
	robot.display.setCursor(0, 20);
	robot.display.print("Saving... ");
	robot.display.display();

	FlashBank flash(2, &FLASH_CONFIGDATA_BEGIN, (size_t)&FLASH_CONFIGDATA_END - (size_t)&FLASH_CONFIGDATA_BEGIN, false);
	flash.write(&MIN_TEMPERATURE, set_temp);
	flash.write(&BLACK_THRESHOLD, set_black);
	flash.write(&SILVER_THRESHOLD, set_silver_thresh);
	flash.write(&SILVER_SAMPLING_SAMPLES_NEEDED, set_silver_samples);
	{
		flash.write(&CELL_INTER_SIZE, set_cell_inter);
		flash.write(&CELL_INTRA_SIZE, set_cell_intra);
	}
	{
		flash.write(&MPU_OFFS_AX, offs_ax);
		flash.write(&MPU_OFFS_AY, offs_ay);
		flash.write(&MPU_OFFS_AZ, offs_az);
		flash.write(&MPU_OFFS_GX, offs_gx);
		flash.write(&MPU_OFFS_GY, offs_gy);
		flash.write(&MPU_OFFS_GZ, offs_gz);
	}
	{
		flash.write(&TURN_OFFSETS[0], new_turn_offs[0]);
		flash.write(&TURN_OFFSETS[1], new_turn_offs[1]);
	}
	flash.commit();

	robot.display.println("OK");
	robot.display.display();
	HAL_Delay(250);
}

bool reset(void* nul)
{
	robot.display.clearDisplay();
	robot.display.setCursor(0, 0);
	robot.display.println("Reset in 1 sec...");
	robot.display.display();
	HAL_Delay(1000);
	robot.display.clearDisplay();
	robot.display.display();
	reset_hardware();
}

}

using namespace Graph;
using namespace calibrate;

void run_calibration()
{
	Menu calib(robot.display, buttonHandler, WHITE);
	calib.addItem("Battery Level", __calibrate_wrap(dispBatt));

	calib.addItem("Shutdown Pi", __calibrate_wrap(shutdownPi));

	calib.addItem("Display Readings", __calibrate_wrap(dispReadings));

	calib.addItem("Temperature", __calibrate_wrap(calTemp));
	calib.addItem("Black level", __calibrate_wrap(calBlack));
	calib.addItem("Silver level", __calibrate_wrap(calSilver));
	calib.addItem("MPU", __calibrate_wrap(calMpu));
	calib.addItem("Turns", __calibrate_wrap(calTurn));
	calib.addItem("Cell sizes", __calibrate_wrap(calCellDistances));

	calib.addItem("Clear maze data", __calibrate_wrap(clearMaze));

	calib.addItem("Show values", __calibrate_wrap(showValues));
	calib.addItem("Save", __calibrate_wrap(save));

	calib.addItem("Reset", reset);
	calib.show();
	robot.display.clearDisplay();
	robot.display.display();
	HAL_Delay(1000);
}
