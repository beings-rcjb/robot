/* Includes ------------------------------------------------------------------*/
#include "includes.h"

extern DMA_HandleTypeDef hdma_adc1;

extern DMA_HandleTypeDef hdma_adc2;

extern DMA_HandleTypeDef hdma_adc3;

extern DMA_HandleTypeDef hdma_i2c2_tx;

extern DMA_HandleTypeDef hdma_i2c2_rx;

extern DMA_HandleTypeDef hdma_uart4_tx;


/**
 * Initializes the Global MSP.
 */
void HAL_MspInit(void)
{


	HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

	/* System interrupt init*/
/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);


}

void HAL_ADC_MspInit(ADC_HandleTypeDef* hadc)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if (hadc->Instance == ADC1)
	{

		/* Peripheral clock enable */
		__ADC1_CLK_ENABLE();

		/**ADC1 GPIO Configuration
		 * PA5     ------> ADC1_IN5
		 * PC4     ------> ADC1_IN14
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_5;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_4;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		/* Peripheral DMA init*/

		hdma_adc1.Instance = DMA2_Stream0;
		hdma_adc1.Init.Channel = DMA_CHANNEL_0;
		hdma_adc1.Init.Direction = DMA_PERIPH_TO_MEMORY;
		hdma_adc1.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_adc1.Init.MemInc = DMA_MINC_ENABLE;
		hdma_adc1.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_adc1.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
		hdma_adc1.Init.Mode = DMA_CIRCULAR;
		hdma_adc1.Init.Priority = DMA_PRIORITY_MEDIUM;
		hdma_adc1.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		hdma_adc1.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
		hdma_adc1.Init.MemBurst = DMA_MBURST_SINGLE;
		hdma_adc1.Init.PeriphBurst = DMA_PBURST_SINGLE;
		HAL_DMA_Init(&hdma_adc1);

		__HAL_LINKDMA(hadc, DMA_Handle, hdma_adc1);

		/* Peripheral interrupt init*/
		HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
		//HAL_NVIC_EnableIRQ(ADC_IRQn);

	}
	else if (hadc->Instance == ADC2)
	{

		/* Peripheral clock enable */
		__ADC2_CLK_ENABLE();

		/**ADC2 GPIO Configuration
		 * PC5     ------> ADC2_IN15
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_5;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		/* Peripheral DMA init*/

		hdma_adc2.Instance = DMA2_Stream2;
		hdma_adc2.Init.Channel = DMA_CHANNEL_1;
		hdma_adc2.Init.Direction = DMA_PERIPH_TO_MEMORY;
		hdma_adc2.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_adc2.Init.MemInc = DMA_MINC_ENABLE;
		hdma_adc2.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
		hdma_adc2.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
		hdma_adc2.Init.Mode = DMA_CIRCULAR;
		hdma_adc2.Init.Priority = DMA_PRIORITY_MEDIUM;
		hdma_adc2.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		hdma_adc2.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
		hdma_adc2.Init.MemBurst = DMA_MBURST_SINGLE;
		hdma_adc2.Init.PeriphBurst = DMA_PBURST_SINGLE;
		HAL_DMA_Init(&hdma_adc2);

		__HAL_LINKDMA(hadc, DMA_Handle, hdma_adc2);

		/* Peripheral interrupt init*/
		HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
		//HAL_NVIC_EnableIRQ(ADC_IRQn);

	}
	else if (hadc->Instance == ADC3)
	{

//		/* Peripheral clock enable */
//		__ADC3_CLK_ENABLE();
//
//		/**ADC3 GPIO Configuration
//		 * PC0     ------> ADC3_IN10
//		 * PC1     ------> ADC3_IN11
//		 * PC2     ------> ADC3_IN12
//		 * PC3     ------> ADC3_IN13
//		 */
//		GPIO_InitStruct.Pin = GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3;
//		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
//		GPIO_InitStruct.Pull = GPIO_NOPULL;
//		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
//
//		/* Peripheral DMA init*/
//
//		hdma_adc3.Instance = DMA2_Stream1;
//		hdma_adc3.Init.Channel = DMA_CHANNEL_2;
//		hdma_adc3.Init.Direction = DMA_PERIPH_TO_MEMORY;
//		hdma_adc3.Init.PeriphInc = DMA_PINC_DISABLE;
//		hdma_adc3.Init.MemInc = DMA_MINC_ENABLE;
//		hdma_adc3.Init.PeriphDataAlignment = DMA_PDATAALIGN_WORD;
//		hdma_adc3.Init.MemDataAlignment = DMA_MDATAALIGN_WORD;
//		hdma_adc3.Init.Mode = DMA_CIRCULAR;
//		hdma_adc3.Init.Priority = DMA_PRIORITY_MEDIUM;
//		hdma_adc3.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
//		hdma_adc3.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
//		hdma_adc3.Init.MemBurst = DMA_MBURST_SINGLE;
//		hdma_adc3.Init.PeriphBurst = DMA_PBURST_SINGLE;
//		HAL_DMA_Init(&hdma_adc3);
//
//		__HAL_LINKDMA(hadc, DMA_Handle, hdma_adc3);
//
//		/* Peripheral interrupt init*/
//		HAL_NVIC_SetPriority(ADC_IRQn, 0, 0);
//		//HAL_NVIC_EnableIRQ(ADC_IRQn);

	}

}

void HAL_ADC_MspDeInit(ADC_HandleTypeDef* hadc)
{

	if (hadc->Instance == ADC1)
	{

		/* Peripheral clock disable */
		__ADC1_CLK_DISABLE();

		/**ADC1 GPIO Configuration
		 * PA5     ------> ADC1_IN5
		 * PA6     ------> ADC1_IN6
		 * PA7     ------> ADC1_IN7
		 * PC4     ------> ADC1_IN14
		 */
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_5 | GPIO_PIN_6 | GPIO_PIN_7);

		HAL_GPIO_DeInit(GPIOC, GPIO_PIN_4);

		/* Peripheral DMA DeInit*/
		HAL_DMA_DeInit(hadc->DMA_Handle);

		/* Peripheral interrupt DeInit*/

		/**
		 * Uncomment the line below to disable the "ADC_IRQn" interrupt
		 * Be aware, disabling shared interrupt may affect other IPs
		 */
		/* HAL_NVIC_DisableIRQ(ADC_IRQn); */


	}
	else if (hadc->Instance == ADC2)
	{

		/* Peripheral clock disable */
		__ADC2_CLK_DISABLE();

		/**ADC2 GPIO Configuration
		 * PC5     ------> ADC2_IN15
		 */
		HAL_GPIO_DeInit(GPIOC, GPIO_PIN_5);

		/* Peripheral DMA DeInit*/
		HAL_DMA_DeInit(hadc->DMA_Handle);

		/* Peripheral interrupt DeInit*/

		/**
		 * Uncomment the line below to disable the "ADC_IRQn" interrupt
		 * Be aware, disabling shared interrupt may affect other IPs
		 */
		/* HAL_NVIC_DisableIRQ(ADC_IRQn); */


	}
	else if (hadc->Instance == ADC3)
	{

		/* Peripheral clock disable */
		__ADC3_CLK_DISABLE();

		/**ADC3 GPIO Configuration
		 * PC0     ------> ADC3_IN10
		 * PC1     ------> ADC3_IN11
		 * PC2     ------> ADC3_IN12
		 * PC3     ------> ADC3_IN13
		 */
		HAL_GPIO_DeInit(GPIOC, GPIO_PIN_0 | GPIO_PIN_1 | GPIO_PIN_2 | GPIO_PIN_3);

		/* Peripheral DMA DeInit*/
		HAL_DMA_DeInit(hadc->DMA_Handle);

		/* Peripheral interrupt DeInit*/

		/**
		 * Uncomment the line below to disable the "ADC_IRQn" interrupt
		 * Be aware, disabling shared interrupt may affect other IPs
		 */
		/* HAL_NVIC_DisableIRQ(ADC_IRQn); */


	}

}

void HAL_DAC_MspInit(DAC_HandleTypeDef* hdac)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if (hdac->Instance == DAC)
	{

		/* Peripheral clock enable */
		__DAC_CLK_ENABLE();

		/**DAC GPIO Configuration
		 * PA4     ------> DAC_OUT1
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_4;
		GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	}

}

void HAL_DAC_MspDeInit(DAC_HandleTypeDef* hdac)
{

	if (hdac->Instance == DAC)
	{

		/* Peripheral clock disable */
		__DAC_CLK_DISABLE();

		/**DAC GPIO Configuration
		 * PA4     ------> DAC_OUT1
		 */
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_4);

	}


}

void HAL_I2C_MspInit(I2C_HandleTypeDef* hi2c)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if (hi2c->Instance == I2C2)
	{


		/**I2C2 GPIO Configuration
		 * PB10     ------> I2C2_SCL
		 * PB11     ------> I2C2_SDA
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_11;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_OD;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF4_I2C2;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		/* Peripheral clock enable */
		__I2C2_CLK_ENABLE();

		/* Peripheral DMA init*/

		hdma_i2c2_tx.Instance = DMA1_Stream7;
		hdma_i2c2_tx.Init.Channel = DMA_CHANNEL_7;
		hdma_i2c2_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_i2c2_tx.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_i2c2_tx.Init.MemInc = DMA_MINC_ENABLE;
		hdma_i2c2_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
		hdma_i2c2_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_i2c2_tx.Init.Mode = DMA_NORMAL;
		hdma_i2c2_tx.Init.Priority = DMA_PRIORITY_LOW;
		hdma_i2c2_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		hdma_i2c2_tx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
		hdma_i2c2_tx.Init.MemBurst = DMA_MBURST_SINGLE;
		hdma_i2c2_tx.Init.PeriphBurst = DMA_PBURST_SINGLE;
		HAL_DMA_Init(&hdma_i2c2_tx);

		__HAL_LINKDMA(hi2c, hdmatx, hdma_i2c2_tx);

		hdma_i2c2_rx.Instance = DMA1_Stream2;
		hdma_i2c2_rx.Init.Channel = DMA_CHANNEL_7;
		hdma_i2c2_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
		hdma_i2c2_rx.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_i2c2_rx.Init.MemInc = DMA_MINC_ENABLE;
		hdma_i2c2_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
		hdma_i2c2_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_i2c2_rx.Init.Mode = DMA_NORMAL;
		hdma_i2c2_rx.Init.Priority = DMA_PRIORITY_LOW;
		hdma_i2c2_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		hdma_i2c2_rx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
		hdma_i2c2_rx.Init.MemBurst = DMA_MBURST_SINGLE;
		hdma_i2c2_rx.Init.PeriphBurst = DMA_PBURST_SINGLE;
		HAL_DMA_Init(&hdma_i2c2_rx);

		__HAL_LINKDMA(hi2c, hdmarx, hdma_i2c2_rx);


	}

}

void HAL_I2C_MspDeInit(I2C_HandleTypeDef* hi2c)
{

	if (hi2c->Instance == I2C2)
	{

		/* Peripheral clock disable */
		__I2C2_CLK_DISABLE();

		/**I2C2 GPIO Configuration
		 * PB10     ------> I2C2_SCL
		 * PB11     ------> I2C2_SDA
		 */
		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_10 | GPIO_PIN_11);

		/* Peripheral DMA DeInit*/
		HAL_DMA_DeInit(hi2c->hdmatx);
		HAL_DMA_DeInit(hi2c->hdmarx);
	}


}

void HAL_SPI_MspInit(SPI_HandleTypeDef* hspi)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if (hspi->Instance == SPI1)
	{

		/* Peripheral clock enable */
		__SPI1_CLK_ENABLE();

		/**SPI1 GPIO Configuration
		 * PB3     ------> SPI1_SCK
		 * PB4     ------> SPI1_MISO
		 * PB5     ------> SPI1_MOSI
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF5_SPI1;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	}

}

void HAL_SPI_MspDeInit(SPI_HandleTypeDef* hspi)
{

	if (hspi->Instance == SPI1)
	{

		/* Peripheral clock disable */
		__SPI1_CLK_DISABLE();

		/**SPI1 GPIO Configuration
		 * PB3     ------> SPI1_SCK
		 * PB4     ------> SPI1_MISO
		 * PB5     ------> SPI1_MOSI
		 */
		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_3 | GPIO_PIN_4 | GPIO_PIN_5);

	}


}

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim_base)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if (htim_base->Instance == TIM2)
	{

		/* Peripheral clock enable */
		__TIM2_CLK_ENABLE();

		/**TIM2 GPIO Configuration
		 * PA15     ------> TIM2_CH1
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_15;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF1_TIM2;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		/* Peripheral interrupt init*/
		HAL_NVIC_SetPriority(TIM2_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(TIM2_IRQn);

	}
	else if (htim_base->Instance == TIM5)
	{

		/* Peripheral clock enable */
		__TIM5_CLK_ENABLE();

		/**TIM5 GPIO Configuration
		 * PA0-WKUP     ------> TIM5_CH1
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_0;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF2_TIM5;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		/* Peripheral interrupt init*/
		HAL_NVIC_SetPriority(TIM5_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(TIM5_IRQn);

	}
	else if (htim_base->Instance == TIM6)
	{

		/* Peripheral clock enable */
		__TIM6_CLK_ENABLE();

	}
	else if (htim_base->Instance == TIM9)
	{

		/* Peripheral clock enable */
		__TIM9_CLK_ENABLE();

		/**TIM9 GPIO Configuration
		 * PA2     ------> TIM9_CH1
		 * PA3     ------> TIM9_CH2
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_2 | GPIO_PIN_3;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF3_TIM9;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);


	}
	else if (htim_base->Instance == TIM10)
	{

		/* Peripheral clock enable */
		__TIM10_CLK_ENABLE();

		/**TIM10 GPIO Configuration
		 * PB8     ------> TIM10_CH1
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_8;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF3_TIM10;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	}
	else if (htim_base->Instance == TIM12)
	{

		/* Peripheral clock enable */
		__TIM12_CLK_ENABLE();

		/**TIM12 GPIO Configuration
		 * PB14     ------> TIM12_CH1
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_14;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF9_TIM12;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

		/* Peripheral interrupt init*/
		HAL_NVIC_SetPriority(TIM8_BRK_TIM12_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(TIM8_BRK_TIM12_IRQn);

	}

}

void HAL_TIM_Encoder_MspInit(TIM_HandleTypeDef* htim_encoder)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if (htim_encoder->Instance == TIM3)
	{

		/* Peripheral clock enable */
		__TIM3_CLK_ENABLE();

		/**TIM3 GPIO Configuration
		 * PC6     ------> TIM3_CH1
		 * PC7     ------> TIM3_CH2
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF2_TIM3;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);


	}
	else if (htim_encoder->Instance == TIM4)
	{

		/* Peripheral clock enable */
		__TIM4_CLK_ENABLE();

		/**TIM4 GPIO Configuration
		 * PB6     ------> TIM4_CH1
		 * PB7     ------> TIM4_CH2
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_6 | GPIO_PIN_7;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		GPIO_InitStruct.Speed = GPIO_SPEED_LOW;
		GPIO_InitStruct.Alternate = GPIO_AF2_TIM4;
		HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	}

}

void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef* htim_base)
{

	if (htim_base->Instance == TIM2)
	{

		/* Peripheral clock disable */
		__TIM2_CLK_DISABLE();

		/**TIM2 GPIO Configuration
		 * PA15     ------> TIM2_CH1
		 */
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_15);

		/* Peripheral interrupt DeInit*/
		HAL_NVIC_DisableIRQ(TIM2_IRQn);


	}
	else if (htim_base->Instance == TIM5)
	{

		/* Peripheral clock disable */
		__TIM5_CLK_DISABLE();

		/**TIM5 GPIO Configuration
		 * PA0-WKUP     ------> TIM5_CH1
		 */
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_0);

		/* Peripheral interrupt DeInit*/
		HAL_NVIC_DisableIRQ(TIM5_IRQn);


	}
	else if (htim_base->Instance == TIM6)
	{

		/* Peripheral clock disable */
		__TIM6_CLK_DISABLE();

	}
	else if (htim_base->Instance == TIM9)
	{

		/* Peripheral clock disable */
		__TIM9_CLK_DISABLE();

		/**TIM9 GPIO Configuration
		 * PA2     ------> TIM9_CH1
		 * PA3     ------> TIM9_CH2
		 */
		HAL_GPIO_DeInit(GPIOA, GPIO_PIN_2 | GPIO_PIN_3);


	}
	else if (htim_base->Instance == TIM10)
	{

		/* Peripheral clock disable */
		__TIM10_CLK_DISABLE();

		/**TIM10 GPIO Configuration
		 * PB8     ------> TIM10_CH1
		 */
		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_8);


	}
	else if (htim_base->Instance == TIM12)
	{

		/* Peripheral clock disable */
		__TIM12_CLK_DISABLE();

		/**TIM12 GPIO Configuration
		 * PB14     ------> TIM12_CH1
		 */
		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_14);

		/* Peripheral interrupt DeInit*/
		HAL_NVIC_DisableIRQ(TIM8_BRK_TIM12_IRQn);


	}

}

void HAL_TIM_Encoder_MspDeInit(TIM_HandleTypeDef* htim_encoder)
{

	if (htim_encoder->Instance == TIM3)
	{

		/* Peripheral clock disable */
		__TIM3_CLK_DISABLE();

		/**TIM3 GPIO Configuration
		 * PC6     ------> TIM3_CH1
		 * PC7     ------> TIM3_CH2
		 */
		HAL_GPIO_DeInit(GPIOC, GPIO_PIN_6 | GPIO_PIN_7);


	}
	else if (htim_encoder->Instance == TIM4)
	{

		/* Peripheral clock disable */
		__TIM4_CLK_DISABLE();

		/**TIM4 GPIO Configuration
		 * PB6     ------> TIM4_CH1
		 * PB7     ------> TIM4_CH2
		 */
		HAL_GPIO_DeInit(GPIOB, GPIO_PIN_6 | GPIO_PIN_7);


	}

}

void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{

	GPIO_InitTypeDef GPIO_InitStruct;
	if (huart->Instance == UART4)
	{

		/* Peripheral clock enable */
		__UART4_CLK_ENABLE();

		/**UART4 GPIO Configuration
		 * PC10     ------> UART4_TX
		 * PC11     ------> UART4_RX
		 */
		GPIO_InitStruct.Pin = GPIO_PIN_10 | GPIO_PIN_11;
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		GPIO_InitStruct.Speed = GPIO_SPEED_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF8_UART4;
		HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

		/* Peripheral DMA init*/

		hdma_uart4_tx.Instance = DMA1_Stream4;
		hdma_uart4_tx.Init.Channel = DMA_CHANNEL_4;
		hdma_uart4_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
		hdma_uart4_tx.Init.PeriphInc = DMA_PINC_DISABLE;
		hdma_uart4_tx.Init.MemInc = DMA_MINC_ENABLE;
		hdma_uart4_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
		hdma_uart4_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
		hdma_uart4_tx.Init.Mode = DMA_NORMAL;
		hdma_uart4_tx.Init.Priority = DMA_PRIORITY_LOW;
		hdma_uart4_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
		hdma_uart4_tx.Init.FIFOThreshold = DMA_FIFO_THRESHOLD_FULL;
		hdma_uart4_tx.Init.MemBurst = DMA_MBURST_SINGLE;
		hdma_uart4_tx.Init.PeriphBurst = DMA_PBURST_SINGLE;
		HAL_DMA_Init(&hdma_uart4_tx);

		__HAL_LINKDMA(huart, hdmatx, hdma_uart4_tx);

		/* Peripheral interrupt init*/
		HAL_NVIC_SetPriority(UART4_IRQn, 0, 0);
		HAL_NVIC_EnableIRQ(UART4_IRQn);


	}

}

void HAL_UART_MspDeInit(UART_HandleTypeDef* huart)
{

	if (huart->Instance == UART4)
	{

		/* Peripheral clock disable */
		__UART4_CLK_DISABLE();

		/**UART4 GPIO Configuration
		 * PC10     ------> UART4_TX
		 * PC11     ------> UART4_RX
		 */
		HAL_GPIO_DeInit(GPIOC, GPIO_PIN_10 | GPIO_PIN_11);

		/* Peripheral DMA DeInit*/
		HAL_DMA_DeInit(huart->hdmatx);

		/* Peripheral interrupt DeInit*/
		HAL_NVIC_DisableIRQ(UART4_IRQn);
	}


}

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
