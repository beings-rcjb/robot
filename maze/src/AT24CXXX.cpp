#include "AT24CXXX.h"

#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_i2c.h>
#include "includes.h"

AT24CXXX::AT24CXXX(uint8_t address) : device_address(address)
{}

uint8_t AT24CXXX::write(uint16_t address, void* data, uint8_t size)
{
	return HAL_I2C_Mem_Write(&hi2c2, device_address, address,
	                         I2C_MEMADD_SIZE_16BIT,
	                         reinterpret_cast<uint8_t*>(data),
	                         size, 0xff);
}

uint8_t AT24CXXX::write(uint16_t address, uint8_t data)
{
	return HAL_I2C_Mem_Write(&hi2c2, device_address, address, I2C_MEMADD_SIZE_16BIT, &data, 1, 0xff);
}

uint8_t AT24CXXX::read(uint16_t address, void* data, uint8_t size)
{
	return HAL_I2C_Mem_Read(&hi2c2, device_address, address,
	                        I2C_MEMADD_SIZE_16BIT,
	                        reinterpret_cast<uint8_t*>(data),
	                        size, 0xff);
}

uint8_t AT24CXXX::read(uint16_t address, uint8_t& data)
{
	return HAL_I2C_Mem_Read(&hi2c2, device_address, address, I2C_MEMADD_SIZE_16BIT, &data, 1, 0xff);
}
