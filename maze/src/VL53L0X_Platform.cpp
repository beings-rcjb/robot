#include "VL53L0X_Platform.h"
#include "VL53L0X_Types.hpp"
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_i2c.h>

using namespace VL53L0X;

const int I2C_TIMEOUT = 0xff;

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::WriteMulti(uint8_t index, uint8_t* pdata, uint32_t count)
{
	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(hi2c, address, index, 1, pdata, static_cast<uint16_t>(count), I2C_TIMEOUT);
	return status == HAL_OK ? VL53L0X_ERROR_NONE : VL53L0X_ERROR_CONTROL_INTERFACE;
}

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::ReadMulti(uint8_t index, uint8_t* pdata, uint32_t count)
{
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(hi2c, address, index, 1, pdata, static_cast<uint16_t>(count), I2C_TIMEOUT);
	return status == HAL_OK ? VL53L0X_ERROR_NONE : VL53L0X_ERROR_CONTROL_INTERFACE;
}

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::WrByte(uint8_t index, uint8_t data)
{
	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(hi2c, address, index, 1, &data, 1, I2C_TIMEOUT);
	return status == HAL_OK ? VL53L0X_ERROR_NONE : VL53L0X_ERROR_CONTROL_INTERFACE;
}

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::WrWord(uint8_t index, uint16_t data)
{
	data = static_cast<uint16_t>(__REV16(data));
	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(hi2c, address, index, 1, reinterpret_cast<uint8_t*>(&data), 2, I2C_TIMEOUT);
	return status == HAL_OK ? VL53L0X_ERROR_NONE : VL53L0X_ERROR_CONTROL_INTERFACE;
}

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::WrDWord(uint8_t index, uint32_t data)
{
	data = __REV(data);
	HAL_StatusTypeDef status = HAL_I2C_Mem_Write(hi2c, address, index, 1, reinterpret_cast<uint8_t*>(&data), 4, I2C_TIMEOUT);
	return status == HAL_OK ? VL53L0X_ERROR_NONE : VL53L0X_ERROR_CONTROL_INTERFACE;
}

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::RdByte(uint8_t index, uint8_t* data)
{
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(hi2c, address, index, 1, data, 1, I2C_TIMEOUT);
	return status == HAL_OK ? VL53L0X_ERROR_NONE : VL53L0X_ERROR_CONTROL_INTERFACE;
}

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::RdWord(uint8_t index, uint16_t* data)
{
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(hi2c, address, index, 1, reinterpret_cast<uint8_t*>(data), 2, I2C_TIMEOUT);
	if (status != HAL_OK)
		return VL53L0X_ERROR_CONTROL_INTERFACE;
	*data = static_cast<uint16_t>(__REV16(*data));
	return VL53L0X_ERROR_NONE;
}

VL53L0X::VL53L0X_Error VL53L0X_RobotPlatform::RdDWord(uint8_t index, uint32_t* data)
{
	HAL_StatusTypeDef status = HAL_I2C_Mem_Read(hi2c, address, index, 1, reinterpret_cast<uint8_t*>(data), 4, I2C_TIMEOUT);
	if (status != HAL_OK)
		return VL53L0X_ERROR_CONTROL_INTERFACE;
	*data = __REV(*data);
	return VL53L0X_ERROR_NONE;
}

void VL53L0X_RobotPlatform::UpdateAddress(uint8_t DeviceAddress)
{
	address = DeviceAddress;
}
