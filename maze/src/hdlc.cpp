#include "hdlc.h"

bool HDLC::init()
{
	uint8_t status = 0;

	status |= startRx();

	return !status;
}

size_t HDLC::load_and_escape(std::vector<uint8_t> data)
{
	const size_t size = data.size();
	if (size > BUF_SIZE - 2)
		return 0;
	size_t index_offset = 1;
	for (size_t i = 0; i < size; ++i) // No iterators because they would be invalidated by inserting
	{
		unsigned char& byte = data[i];
		if (byte == FLAG || byte == ESCAPE_CHAR)
		{
			// Check if it still fits
			if (index_offset + 1 + size + 1 > BUF_SIZE) // TODO: optimize?
				return 0;
			byte = byte ^ ESCAPE_MASK;
			frameTxBuf[i + index_offset++] = ESCAPE_CHAR;
		}
		frameTxBuf[i + index_offset] = byte;
	}
	frameTxBuf[0] = FLAG;
	frameTxBuf[size + index_offset] = FLAG;
	return size + index_offset + 1; // add last flag
}

bool HDLC::send(const std::vector<uint8_t>& data)
{
	size_t len = load_and_escape(data);
	if (len == 0)
		return false;
	return !HAL_UART_Transmit_DMA(huart, frameTxBuf, len);
}

void HDLC::RxCpltCallback(UART_HandleTypeDef* huart_)
{
	if (huart == huart_)
	{
		if (*intRxBuf == FLAG)
		{
			if (handlerFunc)
			{
				handlerFunc(std::vector<uint8_t>(frameRxBuf, frameRxBuf + rxFrameIndex), rxIsInFrame);
			}
			rxIsInFrame = true;
			rxFrameIndex = 0;
			rxIsNextByteEscaped = false;
		}
		else if (*intRxBuf == ESCAPE_CHAR)
		{
			rxIsNextByteEscaped = true;
		}
		else
		{
			if (rxIsNextByteEscaped)
			{
				rxIsNextByteEscaped = false;
				*intRxBuf ^= ESCAPE_MASK;
			}
			if (rxFrameIndex < BUF_SIZE)
			{
				frameRxBuf[rxFrameIndex++] = *intRxBuf;
			}
			else
			{
				// Overflow
				rxIsInFrame = false; // TODO: independent flag?
			}
		}
		startRx();
	}
}

void HDLC::ErrorCallback(UART_HandleTypeDef* huart_)
{
	if (huart == huart_)
	{
		if (huart->ErrorCode != HAL_UART_ERROR_NONE) // TODO: Make more specific?
		{
			startRx();
		}
	}
}

bool HDLC::setRecvHandler(const handler_t& func)
{
	if (hasRecvHandler())
		return false;
	handlerFunc = func;
	return true;
}

bool HDLC::clearRecvHandler()
{
	if (hasRecvHandler())
		return false;
	handlerFunc = nullptr;
	return true;
}
