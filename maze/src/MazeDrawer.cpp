#include "MazeDrawer.h"

#include <array>
#include <cstdint>
#include <mazeData.h>

using namespace std;

namespace
{

constexpr uint8_t ImgSize = 8;
typedef array<array<bool, ImgSize>, ImgSize> Img;

Maze& maze = Maze::getInstance();

namespace Sprites
{

// @formatter:off

constexpr Img empty =
		{{ {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};

constexpr Img Base =
		{{ {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};
constexpr Img CurrCell =
		{{ {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 1, 1, 0, 0, 0},
		   {0, 0, 1, 1, 1, 1, 0, 0},
		   {0, 0, 1, 0, 0, 1, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};
constexpr Img WallWall =
		{{ {1, 1, 1, 1, 1, 1, 1, 1},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};
constexpr Img WallNoWall =
		{{ {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};
constexpr Img WallUnknown =
		{{ {0, 0, 1, 0, 0, 1, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};
constexpr Img CellVisited =
		{{ {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 1, 0, 0, 0},
		   {0, 0, 0, 1, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};
constexpr Img CellStart =
		{{ {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 1, 0, 0, 1, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 1, 0, 0, 1, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};
constexpr Img CellBlack =
		{{ {0, 0, 0, 0, 0, 0, 0, 0},
		   {0, 1, 1, 1, 1, 1, 1, 0},
		   {0, 1, 1, 1, 1, 1, 1, 0},
		   {0, 1, 1, 1, 1, 1, 1, 0},
		   {0, 1, 1, 1, 1, 1, 1, 0},
		   {0, 1, 1, 1, 1, 1, 1, 0},
		   {0, 1, 1, 1, 1, 1, 1, 0},
		   {0, 0, 0, 0, 0, 0, 0, 0} }};

// @formatter:on

}

Img rotate(const Img& src, Direction direction)
{
	if (direction == Direction::N)
		return src;
	Img ret;
	if (direction == Direction::E)
	{
		for (uint8_t i = 0; i < src.size(); ++i)
		{
			for (uint8_t j = 0; j < src[0].size(); ++j)
			{
				ret[j][ImgSize - i - 1] = src[i][j];
			}
		}
	}
	if (direction == Direction::W)
	{
		for (uint8_t i = 0; i < src.size(); ++i)
		{
			for (uint8_t j = 0; j < src[0].size(); ++j)
			{
				ret[ImgSize - j - 1][i] = src[i][j];
			}
		}
	}
	if (direction == Direction::S)
	{
		for (uint8_t i = 0; i < src.size(); ++i)
		{
			for (uint8_t j = 0; j < src[0].size(); ++j)
			{
				ret[ImgSize - i - 1][ImgSize - j - 1] = src[i][j];
			}
		}
	}
	return ret;
}

void overlay(Img& target, const Img& src)
{
	for (uint8_t i = 0; i < ImgSize; ++i)
	{
		for (uint8_t j = 0; j < ImgSize; ++j)
		{
			target[i][j] |= src[i][j];
		}
	}
}

Img drawCell(coord_t pos, Direction rotation)
{
	Img img = Sprites::Base;
	if (!maze.cellExists(pos))
		return img;
	const Maze::Cell& cellInfo = maze[pos];
	for (const Direction& dir : {Direction::N, Direction::E, Direction::S, Direction::W })
	{
		switch (cellInfo.getWall(dir))
		{
		case Maze::Cell::Wall_state::NoWall:
			overlay(img, rotate(Sprites::WallNoWall, dir - rotation));
			break;
		case Maze::Cell::Wall_state::Wall:
			overlay(img, rotate(Sprites::WallWall, dir - rotation));
			break;
		case Maze::Cell::Wall_state::Unknown:
			overlay(img, rotate(Sprites::WallUnknown, dir - rotation));
			break;
		}
	}
	if (MazeState::pos == pos)
	{
		overlay(img, rotate(Sprites::CurrCell, MazeState::orientation - rotation));
	}
	else
	{
		if (cellInfo.getVisited())
			overlay(img, Sprites::CellVisited);
		if (cellInfo.getBlack())
			overlay(img, Sprites::CellBlack);
		if (pos == coord_t(make_pair<int8_t, int8_t>(0, 0), 0))
			overlay(img, Sprites::CellStart);
	}
	return img;
}

}

namespace MazeDrawer
{

array<array<bool, 56>, 56> draw()
{
	constexpr uint8_t NumTiles = 7;
	array<array<bool, NumTiles * ImgSize>, NumTiles * ImgSize> img;
	for (int8_t i = -NumTiles / 2; i <= NumTiles / 2; ++i)
	{
		for (int8_t j = -NumTiles / 2; j <= NumTiles / 2; ++j)
		{
			coord_t pos = MazeState::pos;
			switch (MazeState::orientation.toEnum())
			{
			case Direction::N:
				pos.first.second += i;
				pos.first.first += j;
				break;
			case Direction::E:
				pos.first.second += j;
				pos.first.first -= i;
				break;
			case Direction::W:
				pos.first.second -= j;
				pos.first.first += i;
				break;
			case Direction::S:
				pos.first.second -= i;
				pos.first.first -= j;
				break;
			}
			Img tile = drawCell(pos, MazeState::orientation);
			uint8_t i_base = (i + (NumTiles / 2)) * ImgSize;
			uint8_t j_base = (j + (NumTiles / 2)) * ImgSize;
			for (uint8_t il = 0; il < ImgSize; ++il)
			{
				for (uint8_t jl = 0; jl < ImgSize; ++jl)
				{
					img[i_base + il][j_base + jl] = tile[il][jl];
				}
			}
		}
	}
	return img;
}

void drawToDisplay(GFX& display)
{
	constexpr uint8_t verti_offset = 8;
	constexpr uint8_t horiz_offset = 36;
//	display.fillRect(horiz_offset, verti_offset, 56, 56, 0);
	display.fillRect(0, 0, display.width(), display.height(), 0);
	auto img = draw();
	for (uint8_t i = 0; i < 56; ++i)
	{
		for (uint8_t j = 0; j < 56; ++j)
		{
			if (img[i][j])
				display.drawPixel(j + horiz_offset, i + verti_offset, 1);
		}
	}
	display.display();
}

}
