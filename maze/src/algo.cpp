#include "algo.h"

#include "mazeData.h"
#include "mazeUtils.h"

#include <map>
#include <list>
#include <queue>
#include <set>
#include <list>
#include <vector>
#include <cassert>

#include <array>

#include "hw_impl.h"
#include "flash.h"

using namespace std;
using namespace MazeState;
using namespace MazeUtils;

namespace
{
Maze& maze = Maze::getInstance();
}

namespace
{
const std::array<std::array<Direction, 4>, 2> check_orders = {{ {{Direction::N, Direction::E, Direction::W, Direction::S}},
                                                                {{Direction::N, Direction::W, Direction::E, Direction::S}} }};
};

Algorithm algo;

bool Algorithm::pathfind_to(const coord_t& next_pos)
{
	if (next_pos == pos)
		return true;
	map<coord_t, coord_t> backtrace_list;
	queue<coord_t> toVisit;
	set<coord_t> visited;
	visited.insert(pos);
	bool done = false;
	for (Direction check_dir : { Direction::N, Direction::E, Direction::S, Direction::W })
	{
		if (maze[pos].getWall(check_dir) == Maze::Cell::Wall_state::NoWall)
		{
			const coord_t& next_loc = move_dir_follow(pos, check_dir);
			if (next_loc == next_pos)
			{
				backtrace_list[next_loc] = pos;
				queue<coord_t>().swap(toVisit); // clear the queue; pathfinding complete
				done = true;
				break;
			}
			if (!visited.count(next_loc))
			{
				if (maze[next_loc].getVisited())
				{
					if (!(maze[next_loc].getBlack()))
					{
						toVisit.push(next_loc);
						visited.insert(next_loc);
						backtrace_list[next_loc] = pos;
					}
				}
			}
		}
	}
	while (!toVisit.empty())
	{
		auto curr = toVisit.front();
		toVisit.pop();
		for (Direction check_dir : { Direction::N, Direction::E, Direction::S, Direction::W })
		{
			if (maze[curr].getWall(check_dir) == Maze::Cell::Wall_state::NoWall)
			{
				const coord_t& next_loc = move_dir_follow(curr, check_dir);
				if (next_loc == next_pos)
				{
					backtrace_list[next_loc] = curr;
					done = true;
					break;
				}
				if (!visited.count(next_loc))
				{
					if (maze[next_loc].getVisited())
					{
						if (!(maze[next_loc].getBlack()))
						{
							toVisit.push(next_loc);
							visited.insert(next_loc);
							backtrace_list[next_loc] = curr;
						}
					}
				}
			}
		}
		if (done)
			break;
	}
	if (!done)
	{
		return false;
	}
	list<coord_t> trace;
	auto curr = next_pos;
	while (curr != pos)
	{
		trace.push_front(curr);
		curr = backtrace_list[curr];
	}
	HwImpl::moveToAdjacent(trace.front());
	return true;
}

namespace
{
list<coord_t> toVisit;
}

void Algorithm::solve()
{
	while (!toVisit.empty() && !HwImpl::shouldReturn())
	{
		coord_t next_pos(toVisit.back());
		bool isNextPosInvalid = false;
		if (maze[next_pos].getRamp()) // Target cell was ramp (algorithm would never set ramp tile as target)
			isNextPosInvalid = true;
		if (maze[next_pos].getBlack())
			isNextPosInvalid = true;
		if (next_pos == pos || isNextPosInvalid)
		{
			// Target cell reached
			toVisit.pop_back();
			maze[pos].setVisited(true); // Mark as visited
			for (Direction check_dir : check_order_rev)
			{
				Direction facing = orientation + check_dir;
				if (maze[pos].getWall(facing) == Maze::Cell::Wall_state::NoWall)
				{
					if (!maze[move_dir_follow(pos, facing)].getVisited())
					{
						toVisit.push_back(move_dir_follow(pos, facing));
					}
				}
			}
			while (!toVisit.empty()) // Get next tile to visit
			{
				next_pos = toVisit.back();
				if ((maze[next_pos].getVisited()) || (maze[next_pos].getBlack())) // Continue if already visited or if is a black tile
				{
					toVisit.pop_back();
					continue;
				}
				break;
			}
			if (toVisit.empty())
				break; // Go home
		}
		if (!isAdj(pos, next_pos))
		{
			// pathfind to point
			if (!pathfind_to(next_pos))
			{
				// Inaccessible

				toVisit.pop_back();
			}
		}
		else
		{
			HwImpl::moveToAdjacent(next_pos);
		}
	}
	{
		while (pos != make_pair(make_pair<int8_t, int8_t>(0, 0), static_cast<int8_t>(0)))
		{
			pathfind_to(make_pair(make_pair(0, 0), 0));
		}
	}
}

void Algorithm::setCheckOrder(std::array<Direction, 4> order)
{
	check_order = order;
	std::copy(check_order.rbegin(), check_order.rend(), check_order_rev.begin());
}

void Algorithm::init_solve(bool isReal)
{
	if (isReal)
		HwImpl::preinitInitSolve();
	pos = make_pair(make_pair(0, 0), 0);
	orientation = Direction::N;
	setCheckOrder(check_orders[0]);
	toVisit.clear();
	toVisit.push_back(pos);
	HwImpl::initUpdateWalls();
	solve();
}

void Algorithm::init_solve(void)
{
	init_solve(true);
}

void Algorithm::resume_solve(void)
{
	HwImpl::preinitResumeSolve();
	const auto data = HwImpl::read_data();
	if (data.empty())
	{
		init_solve(false);
		return;
	}
	auto it = data.begin();
	{
		pos.first.first  = *it++;
		pos.first.second = *it++;
		pos.second       = *it++;
	}
	it = maze.deserialize(it);
	{
		uint16_t stack_size = *it++;
		stack_size |= *it++ << 8;
		for (uint16_t i = 0; i < stack_size; ++i)
		{
			coord_t tmp;
			tmp.first.first  = *it++;
			tmp.first.second = *it++;
			tmp.second       = *it++;
			toVisit.push_back(tmp);
		}
	}
	setCheckOrder(check_orders[HwImpl::getSeed() % check_orders.size()]);
	solve();
}

void Algorithm::save_state()
{
	const coord_t& tile = pos;
	vector<uint8_t> maze_data;
	{
		maze_data.push_back(tile.first.first);
		maze_data.push_back(tile.first.second);
		maze_data.push_back(tile.second);
	}
	{
		auto maze_dump = maze.serialize();
		maze_data.insert(maze_data.end(), maze_dump.begin(), maze_dump.end());
	}
	{
		vector<uint8_t> stack_dump(2); // Reserve space for size
		uint16_t size = 0;
		for (coord_t curr : toVisit)
		{
			stack_dump.push_back(curr.first.first);
			stack_dump.push_back(curr.first.second);
			stack_dump.push_back(curr.second);
			++size;
		}
		stack_dump[0] = (uint8_t)(size & 0xff); // Low bit
		stack_dump[1] = (uint8_t)((size >> 8) & 0xff); // High bit
		maze_data.insert(maze_data.end(), stack_dump.begin(), stack_dump.end());
	}
	HwImpl::save_data(maze_data);
}
