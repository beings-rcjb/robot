#include "flash.h"

FlashBank::FlashBank(uint32_t sector_, void* begin_, uint16_t size, bool clear)
{
	assert_param(IS_FLASH_SECTOR(sector_));
	assert_param(IS_FLASH_ADDRESS((size_t)begin_));
	assert_param(IS_FLASH_ADDRESS((size_t)begin_ + size));
	sector = sector_;
	begin = begin_;

	size = (size + 1) / 2 * 2;

	if (clear)
	{
		mem.resize(size, 0xff);
	}
	else
	{
		mem.reserve(size);
		for (uint8_t* ptr = (uint8_t*)begin; ptr < (void*)((size_t)(begin) + size); ++ptr)
		{
			mem.push_back(*ptr);
		}
	}
}

template <>
size_t FlashBank::write<uint8_t, uint8_t>(const uint8_t* addr, const uint8_t& data)
{
	uint16_t ind = (size_t)addr - (size_t)begin;
	mem[ind] = data;
	return 1;
}

void FlashBank::commit()
{
	HAL_FLASH_Unlock(); // Unlock Flash Memory

	/* Clear All pending flags */
	__HAL_FLASH_CLEAR_FLAG(FLASH_FLAG_EOP | FLASH_FLAG_OPERR | FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);

	// Erase Sector before Writing
	FLASH_Erase_Sector(sector, VOLTAGE_RANGE_3);

	for (uint16_t* ptr = (uint16_t*)begin; ptr < (void*)((size_t)(begin) + mem.size()); ++ptr)
	{
		uint16_t ind = (size_t)ptr - (size_t)begin;
		HAL_FLASH_Program(TYPEPROGRAM_HALFWORD, (size_t)ptr, mem[ind] | (mem[ind + 1] << 8));
	}

	HAL_FLASH_Lock(); // Lock flash memory
}
