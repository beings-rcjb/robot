#include <stdint.h>
#include "includes.h"
#include "move.h"
#include "PIDCtrl.h"
#include <cmath>
#include <memory>
#include "hardwaredefs.h"
#include "calibration.h"

const float ENCODER_SCALE_FACTOR = WHEEL_DIAMETER * 2 * 3.1415926535 / 100 / 12;

const PIDCtrl<float> MOVE_SPEED_PID(0.5, 0, 0.5);
const float MOVE_SPEED_SCALAR = 0.3;
const PIDCtrl<float> MOVE_SYNC_PID(2, 0.1, 0);
const PIDCtrl<float> MOVE_HEADING_PID(10, 0, 5);

const PIDCtrl<float> TURN_SPEED_PID(0.5, 0, 0.5);
const float TURN_SPEED_SCALAR = 0.2;
const PIDCtrl<float> TURN_SYNC_PID(2, 0.1, 2);

const float SYNC_ERROR_MAX = 100;

using namespace std;

MoveDriver::MoveDriver(Robot& robot_) : robot(robot_)
{}

bool MoveDriver::move_straight(bool reverse, float power, unique_ptr<MoverMoveInfo>&& info)
{
	if (state != ActState::Idle)
	{
		return false;
	}
	info->bind(this);
	float y, p, r;
	robot.mpu.getQuat().toYPR(y, p, r);
	ActData.MoveData.power = power / 2; // Allow PID controller to ramp up
	ActData.MoveData.scale = reverse ? -1 : 1;
	ActData.MoveData.speed_pid = MOVE_SPEED_PID;
	ActData.MoveData.speed_target = power * MOVE_SPEED_SCALAR;
	ActData.MoveData.sync_pid = MOVE_SYNC_PID;
	ActData.MoveData.heading_pid = info->getHeadingPID();
	ActData.MoveData.heading = y * ActData.MoveData.scale;
	ActData.MoveData.curr_distance = 0;
	ActData.MoveData.info = move(info);
	for (uint8_t i = 0; i < 2; ++i)
	{
		robot.motors[i].update(power, reverse);
		robot.motors[i].setEncoder(0);
	}
	state = ActState::Move;
	return true;
}

bool MoveDriver::move_straight(float distance, float power)
{
	return move_straight(distance < 0, power, unique_ptr<MoverMoveInfo>(new MoverMoveDistanceInfo(distance)));
}

bool MoveDriver::turn(bool direction, float power, unique_ptr<MoverTurnInfo>&& info)
{
	if (state != ActState::Idle)
	{
		return false;
	}
	ActData.TurnData.speed_pid = TURN_SPEED_PID;
	ActData.TurnData.speed_target = power * TURN_SPEED_SCALAR;
	ActData.TurnData.sync_pid = TURN_SYNC_PID;
	{
		float y, p, r;
		robot.mpu.getQuat().toYPR(y, p, r);
		ActData.TurnData.y_start = y;
	}
	info->bind(this);
	ActData.TurnData.power = power / 2; // Allow PID controller to ramp up
	ActData.TurnData.info = move(info);
	if (direction)
	{
		ActData.TurnData.scales[0] = -1;
		ActData.TurnData.scales[1] = 1;
		robot.motors[0].update(true);
		robot.motors[1].update(false);
	}
	else
	{
		ActData.TurnData.scales[0] = 1;
		ActData.TurnData.scales[1] = -1;
		robot.motors[0].update(false);
		robot.motors[1].update(true);
	}
	{
		robot.motors[0].setEncoder(0);
		robot.motors[1].setEncoder(0);
	}
	state = ActState::Turn;
	return true;
}

bool MoveDriver::turn(float degrees, float power)
{
	degrees = clamp_wrap(degrees, -180, 180);
	return turn(degrees > 0, power, unique_ptr<MoverTurnInfo>(new MoverTurnDegreesInfo(degrees)));
}

float MoveDriver::getCurrDistance()
{
	if (state == ActState::Move)
	{
		return ActData.MoveData.curr_distance;
	}
	else if (state == ActState::Idle)
	{
		return prev_distance;
	}
	else
	{
		return 0;
	}
}

float MoveDriver::getCurrTurn()
{
	if (state == ActState::Turn)
	{
		float y, p, r;
		robot.mpu.getQuat().toYPR(y, p, r);
		return clamp_wrap(y - ActData.TurnData.y_start, -180, 180);
	}
	else if (state == ActState::Idle)
	{
		return prev_turn;
	}
	else
	{
		return 0;
	}
}

bool MoveDriver::isFinished()
{
	return state == ActState::Idle;
}

void MoveDriver::stop()
{
	for (uint8_t i = 0; i < 2; ++i)
	{
		robot.motors[i].update(0, false);
	}
	prev_distance = prev_turn = 0;
	switch (state)
	{
	case ActState::Move:
		ActData.MoveData.info.reset();
		prev_distance = ActData.MoveData.curr_distance;
		break;
	case ActState::Turn:
		ActData.TurnData.info.reset();
		{
			float y, p, r;
			robot.mpu.getQuat().toYPR(y, p, r);
			prev_turn = clamp_wrap((y) - ActData.TurnData.y_start, -180, 180);
		}
		break;
	default:
		;
	}
	state = ActState::Idle;
}

void MoveDriver::onSysTick()
{
	static bool isFirst = true;
	static uint32_t last_time;
	if (isFirst)
	{
		// Don't do anything but log current time so there is
		// no bad time interval caused by last_time == 0
		isFirst = false;
		last_time = HAL_GetTick();
	}
	if ((HAL_GetTick() - last_time) > 10)
	{
		last_time = HAL_GetTick();
		switch (state)
		{
		case ActState::Move:
		{
			if (ActData.MoveData.info->isFinished())
			{
				stop();
				break;
			}
			else
			{
				int16_t encoder[2];
				for (uint8_t i = 0; i < 2; ++i)
				{
					encoder[i] = robot.motors[i].getEncoder() * ActData.MoveData.scale;
					robot.motors[i].setEncoder(0); // Both encoders will not be cleared at exactly the same time, but they will get read after the same interval
				}
				ActData.MoveData.curr_distance += (encoder[0] + encoder[1]) * ENCODER_SCALE_FACTOR / 2.0;
				float y, p, r;
				robot.mpu.getQuat().toYPR(y, p, r);
				y *= ActData.MoveData.scale;
				float heading_error = clamp_wrap(y - ActData.MoveData.heading, -180, 180);
				heading_error += ActData.MoveData.info->getHeadingCorrection(heading_error);
				heading_error = clamp_wrap(heading_error, -180, 180);
				float heading_correction = ActData.MoveData.heading_pid.update(heading_error);
				float sync_error;
				if ((encoder[0] == 0) && (encoder[1] == 0))
					sync_error = 0;
				else if ((encoder[0] * encoder[1]) < 0)
					if (encoder[0] < 0)
						sync_error = -SYNC_ERROR_MAX;
					else
						sync_error = SYNC_ERROR_MAX;
				else
					sync_error = clamp(10 * log((float)encoder[0] / encoder[1]), -SYNC_ERROR_MAX, SYNC_ERROR_MAX);
				float speed_error = ActData.MoveData.speed_target - ((encoder[0] + encoder[1]) / 2);
				speed_error += ActData.MoveData.info->getSpeedCorrection();
				float sync_correction =
				    ActData.MoveData.sync_pid.update(sync_error - heading_correction);
				float speed_correction =
				    ActData.MoveData.speed_pid.update(speed_error);
				ActData.MoveData.power += speed_correction;
				robot.motors[0].update(ActData.MoveData.power - sync_correction);
				robot.motors[1].update(ActData.MoveData.power + sync_correction);
			}
			break;
		}
		case ActState::Turn:
		{
			float y, p, r;
			robot.mpu.getQuat().toYPR(y, p, r);
			if (ActData.TurnData.info->isFinished())
			{
				stop();
				break;
			}
			else
			{
				int16_t encoder[2];
				for (uint8_t i = 0; i < 2; ++i)
				{
					encoder[i] = robot.motors[i].getEncoder() * ActData.TurnData.scales[i];
					robot.motors[i].setEncoder(0); // Both encoders will not be cleared at exactly the same time, but they will get read after the same interval
				}
				float speed_error = ActData.TurnData.speed_target - ((encoder[0] + encoder[1]) / 2);
				speed_error += ActData.TurnData.info->getSpeedCorrection();
				float sync_correction =
				    ActData.TurnData.sync_pid.update(encoder[0] - encoder[1]);
				float speed_correction =
				    ActData.TurnData.speed_pid.update(speed_error);
				ActData.TurnData.power += speed_correction;
				robot.motors[0].update(ActData.TurnData.power - sync_correction);
				robot.motors[1].update(ActData.TurnData.power + sync_correction);
			}
			break;
		}
		case ActState::Idle:
			break;
		}
	}
}

void MoveDriver::clearPreviousRecords()
{
	prev_distance = 0;
	prev_turn = 0;
}

PIDCtrl<float> MoveDriver::MoverMoveInfo::getHeadingPID()
{
	return MOVE_HEADING_PID;
}

MoveDriver::MoverTurnDegreesInfo::MoverTurnDegreesInfo(float degrees) : target_degrees(abs(degrees))
{
	if (target_degrees > 0)
	{
		target_degrees *= TURN_OFFSETS[0];
	}
	else
	{
		target_degrees *= TURN_OFFSETS[1];
	}
}
