#include "disp_menu.h"

#include "GFX.h"

namespace Graph
{

Menu::Menu(GFX& display_, InputCB cb_, unsigned char color_) : display(display_), inpCb(cb_), color(color_)
{
	display.setTextWrap(false);
	display.setTextSize(1);
	display.setTextColor(color);
	display.setFont();
	char_height = 7;
	display.invertDisplay(0);
}

void Menu::addItem(std::string name, MenuCB cb, void* data)
{
	Item item;
	item.name = name;
	item.cb = cb;
	item.data = data;
	items.push_back(item);
}

void Menu::addExitItem(std::string name)
{
	addItem(name, [] (void* a) {return true; });
}

void Menu::draw(unsigned char selected)
{
	static unsigned char dispCnt = display.height() / (char_height + line_space);
	static unsigned char topMargin = (display.height() - (dispCnt * (char_height + line_space) - line_space)) / 2;

	display.fillScreen(Black);
	// LABELS
	static unsigned char pad_top = (((float)dispCnt / 2) - 0.5); // Pad from selected item
	short top_ind = selected - pad_top;
	top_ind -= max(top_ind + dispCnt - (signed char)items.size(), 0);
	if (top_ind < 0)
	{
		top_ind = 0;
	}
	unsigned char currY = topMargin;
	for (int toDisp = top_ind; toDisp < (top_ind + dispCnt); ++toDisp)
	{
		if (toDisp >= items.size()) // At the end, nothing more to display
		{
			break;
		}
		unsigned char nextY = currY + (char_height + line_space);
		display.setCursor(2, currY);
		if (toDisp == selected)
		{
			display.drawRect(0, currY - (line_space / 2), /*display.width() - */ 1, (char_height + line_space), color);
		}
		display.print(items[toDisp].name);
		currY = nextY;
	}

	// SCROLLBAR
	display.fillRect(display.width() - 2, 0, display.width() - 1, display.height() - 1, Black);
	// Scrollbar track
	//display.drawLine(display.width() - 1, 0, display.width() - 1, display.height() - 1, color);
	// Scrollbar thumb
	unsigned char thumb_height = display.height(), travel_height;
	float thumb_sz_pct = (float)dispCnt / items.size();
	thumb_height *= thumb_sz_pct;
	travel_height = max(display.height() - thumb_height, 0);
	display.fillRect(display.width() - 2, travel_height * (selected / (items.size() - 1.0)),
	                 display.width() - 1, thumb_height, color);

	display.display();
}

void Menu::show()
{
	unsigned char current = 0;
	size_t size = items.size();
	bool exit = false;
	while (!exit)
	{
		draw(current);
		unsigned char opcode;
		while ((opcode = (*inpCb)()) == 0)  // Wait until an action is received
		{}
		switch (opcode)
		{
		case 1:
			if (current == 0)
			{
				current = size;
			}
			current -= 1;
			break;
		case 2:
			current += 1;
			current %= size;
			break;
		case 3:
			exit = (*items[current].cb)(items[current].data);
			break;
		case 4:
			exit = true;
			break;
		}
	}
}

}
