#include "includes.h"

__attribute__((noreturn)) void reset_hardware(void)
{
	NVIC_SystemReset();
	__builtin_unreachable();
}
