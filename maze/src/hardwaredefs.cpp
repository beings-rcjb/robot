#include "hardwaredefs.h"

const float SERVO_PULSE_CENTER = 1.5;
const float SERVO_PULSE_RANGE = 0.5;

ld_section(".flash-config") volatile const int16_t MPU_OFFS_AX = 0;
ld_section(".flash-config") volatile const int16_t MPU_OFFS_AY = 0;
ld_section(".flash-config") volatile const int16_t MPU_OFFS_AZ = 0;
ld_section(".flash-config") volatile const int16_t MPU_OFFS_GX = 0;
ld_section(".flash-config") volatile const int16_t MPU_OFFS_GY = 0;
ld_section(".flash-config") volatile const int16_t MPU_OFFS_GZ = 0;

const float TOF_OFFSET_LF = -3.0f;
const float TOF_OFFSET_LB = -3.0f;
const float TOF_OFFSET_RF = -3.0f;
const float TOF_OFFSET_RB = -3.0f;
const float TOF_OFFSET_FL = -1.5f;
const float TOF_OFFSET_FR = -1.5f;

const float IR_TEMP_BASE = 15;
const float IR_TEMP_RANGE = 20;

const float PING_OFFSET = -1;

const float WHEEL_DIAMETER = 1.83;

const float IR_FB_DISTANCE = 12.2;

ld_section(".flash-config") volatile const float TURN_OFFSETS[2] = { 1, 1 }; // Correction factors for if offsets are wrong;
