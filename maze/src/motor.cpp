#include "motor.h"
#include "gpio.h"
#include "includes.h"
#include <stdexcept>

Motor::Motor(uint16_t ctrl1_pin_, GPIO_TypeDef* ctrl1_bus_, uint16_t ctrl2_pin_, GPIO_TypeDef* ctrl2_bus_,
             TIM_HandleTypeDef* pwm_tim_, uint8_t pwm_channel_, TIM_HandleTypeDef* encoder_tim_)
{
	if (!IS_GPIO_PIN(ctrl1_pin_) || !IS_GPIO_PIN(ctrl2_pin_))
	{
		throw std::invalid_argument("Control pins are not a pin");
	}
	// TODO: check that gpio bus, timer, and channel are valid
	ctrl1_pin = ctrl1_pin_;
	ctrl2_pin = ctrl2_pin_;
	ctrl1_bus = ctrl1_bus_;
	ctrl2_bus = ctrl2_bus_;
	pwm_tim = pwm_tim_;
	pwm_channel = pwm_channel_;
	encoder_tim = encoder_tim_;
}

bool Motor::init()
{
	uint8_t status = 0;
	status |= HAL_TIM_PWM_Start(pwm_tim, TIM_CHANNEL(pwm_channel));
	status |= HAL_TIM_Encoder_Start(encoder_tim, TIM_CHANNEL_ALL);
	return !status;
}

void Motor::update(float power, bool direction)
{
	update(direction);
	update(power);
}

void Motor::update(float power)
{
	power = clamp(power, 0, 100);
	uint16_t dc = (pwm_tim->Instance->ARR - 1) * power / 100;
	__IO uint32_t* ccr = &(pwm_tim->Instance->CCR1);
	ccr += (pwm_channel - 1);
	*ccr = dc;
}

void Motor::update(bool direction)
{
	GPIO_WritePin(ctrl1_bus, ctrl1_pin,  direction);
	GPIO_WritePin(ctrl2_bus, ctrl2_pin, !direction);
}

int16_t Motor::getEncoder()
{
	return encoder_tim->Instance->CNT;
}

void Motor::setEncoder(int16_t val)
{
	encoder_tim->Instance->CNT = val;
}
