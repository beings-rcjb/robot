#include "piInterface.h"

#include <functional>

bool PiInterface::init()
{
	using namespace std::placeholders;
	bool iStatus = false;
	iStatus |= !hdlc.init();
	iStatus |= !hdlc.setRecvHandler(std::bind(&PiInterface::handleData,
	                                          this, _1, _2));
	return !iStatus;
}

void PiInterface::handleData(std::vector<uint8_t> data, bool valid)
{
	if (!valid || data.size() < 1) // No header/ID byte - fail out
		return;
	callbacks[data[0]].call(std::vector<uint8_t>(++data.begin(), data.end()));
}

bool PiInterface::send(uint8_t id, std::vector<uint8_t> data)
{
	data.insert(data.begin(), id);
	return sendRaw(data);
}

bool PiInterface::sendRaw(const std::vector<uint8_t>& data)
{
	return hdlc.send(data);
}
