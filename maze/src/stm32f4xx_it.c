/* Includes ------------------------------------------------------------------*/
#include "includes.h"


/* External variables --------------------------------------------------------*/
extern DMA_HandleTypeDef hdma_adc1;
extern DMA_HandleTypeDef hdma_adc2;
extern DMA_HandleTypeDef hdma_adc3;
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern ADC_HandleTypeDef hadc3;
extern DMA_HandleTypeDef hdma_i2c2_tx;
extern DMA_HandleTypeDef hdma_i2c2_rx;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim5;
extern TIM_HandleTypeDef htim12;
extern DMA_HandleTypeDef hdma_uart4_tx;

/******************************************************************************/
/*            Cortex-M4 Processor Interruption and Exception Handlers         */
/******************************************************************************/

/**
 * @brief This function handles System tick timer.
 */
void SysTick_Handler(void)
{

	HAL_IncTick();
	HAL_SYSTICK_IRQHandler();

	SystemTick();

}

/******************************************************************************/
/* STM32F4xx Peripheral Interrupt Handlers                                    */
/* Add here the Interrupt Handlers for the used peripherals.                  */
/* For the available peripheral interrupt handler names,                      */
/* please refer to the startup file (startup_stm32f4xx.s).                    */
/******************************************************************************/

/**
 * @brief This function handles UART4 global interrupt
 */
void UART4_IRQHandler(void)
{

	HAL_UART_IRQHandler(&huart4);

}

/**
 * @brief This function handles DMA1 Stream2 global interrupt.
 */
void DMA1_Stream2_IRQHandler(void)
{

	HAL_DMA_IRQHandler(&hdma_i2c2_rx);

}

/**
 * @brief This function handles DMA1 Stream4 global interrupt.
 */
void DMA1_Stream4_IRQHandler(void)
{

	HAL_DMA_IRQHandler(&hdma_uart4_tx);

}

/**
 * @brief This function handles ADC1, ADC2 and ADC3 global interrupts.
 */
void ADC_IRQHandler(void)
{

	HAL_ADC_IRQHandler(&hadc1);
	HAL_ADC_IRQHandler(&hadc2);
	HAL_ADC_IRQHandler(&hadc3);

}

/**
 * @brief This function handles TIM2 global interrupt.
 */
void TIM2_IRQHandler(void)
{

	HAL_TIM_IRQHandler(&htim2);

}

/**
 * @brief This function handles TIM8 Break interrupt and TIM12 global interrupt.
 */
void TIM8_BRK_TIM12_IRQHandler(void)
{

	HAL_TIM_IRQHandler(&htim12);

}

/**
 * @brief This function handles DMA1 Stream7 global interrupt.
 */
void DMA1_Stream7_IRQHandler(void)
{

	HAL_DMA_IRQHandler(&hdma_i2c2_tx);

}

/**
 * @brief This function handles TIM5 global interrupt.
 */
void TIM5_IRQHandler(void)
{

	HAL_TIM_IRQHandler(&htim5);

}

/**
 * @brief This function handles DMA2 Stream0 global interrupt.
 */
void DMA2_Stream0_IRQHandler(void)
{

	HAL_DMA_IRQHandler(&hdma_adc1);

}

/**
 * @brief This function handles DMA2 Stream1 global interrupt.
 */
void DMA2_Stream1_IRQHandler(void)
{

	HAL_DMA_IRQHandler(&hdma_adc3);

}

/**
 * @brief This function handles DMA2 Stream2 global interrupt.
 */
void DMA2_Stream2_IRQHandler(void)
{

	HAL_DMA_IRQHandler(&hdma_adc2);

}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
