#include "timeout.h"
#include <stm32f4xx_hal.h>
#include <list>
#include "includes.h"

namespace
{

struct QueueData
{
	uint32_t timeToExecute;
	uint8_t priority;
	bool recurs;
	uint32_t ticks;
	std::function<void(void)> callback;
};

std::list<QueueData>& getQueue()
{
	static std::list<QueueData> queue;
	return queue;
}

void addToQueue(QueueData& qd)
{
	qd.timeToExecute = HAL_GetTick() + qd.ticks;
	auto& queue = getQueue();
	auto it = queue.begin();
	while (it != queue.end() && it->timeToExecute <= qd.timeToExecute)
	{
		++it;
	}
	queue.insert(it, qd);
}

void addToQueue(uint32_t ticks, std::function<void(void)> callback, bool recurs)
{
	QueueData qd;
	qd.ticks = ticks;
	qd.callback = callback;
	qd.recurs = recurs;
	addToQueue(qd);
}

}

namespace Timeout
{

void setTimeout(uint32_t ticks, std::function<void(void)> callback)
{
	addToQueue(ticks, callback, false);
}

void setInterval(uint32_t ticks, std::function<void(void)> callback, bool triggersNow)
{
	if (triggersNow)
	{
		callback();
	}
	addToQueue(ticks, callback, true);
}

void onSysTick()
{
	auto& queue = getQueue();
	auto it = queue.begin();
	uint32_t time = HAL_GetTick();
	while (it != queue.end() && static_cast<int32_t>(it->timeToExecute - time) <= 0)
	{
		it->callback();
		if (it->recurs)
		{
			addToQueue(*it);
		}
		++it;
	}
	queue.erase(queue.begin(), it);
}

}
