cmake_minimum_required(VERSION 3.0)

include_directories(system/include/)
include_directories(system/include/cmsis)
include_directories(system/include/stm32f4-hal)

file(GLOB_RECURSE SYSTEM_SOURCE_FILES_UNFILTERED "system/src/*.c")

foreach(ITR ${SYSTEM_SOURCE_FILES_UNFILTERED})
	if(NOT ITR MATCHES "(.*)template\\.(.*)")
		list(APPEND SYSTEM_SOURCE_FILES ${ITR})
	endif()
endforeach()

set(SOURCE_FILES ${SOURCE_FILES} ${SYSTEM_SOURCE_FILES})
