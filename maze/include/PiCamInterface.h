#pragma once

#include <cstdint>
#include <vector>
#include <algorithm>
#include <piInterface.h>
#include <CallbackManager.h>

class PiCamInterface
{
protected:

	PiInterface& device;

private:

	static constexpr uint8_t CHANNEL_ID = 0x02;

	void dataHandler(std::vector<uint8_t> data);

public:

	PiCamInterface(PiInterface& device);

	enum class VisVicType { H = 1, S = 2, U = 3, None = 0 };

	class ValueAcquiredCallbackManager : public CallbackManagerAddableShim<std::pair<VisVicType, bool> >
	{ friend PiCamInterface; };

	ValueAcquiredCallbackManager valueAcquiredCallbackManager;

};
