#pragma once

#include <cstdint>

class AT24CXXX
{
private:

	uint8_t device_address;

public:

	AT24CXXX(uint8_t address = 0xA0);

	/**
	 * Write (up to) a page to EEPROM
	 *
	 * Performs a batch write operation - size limit of 64 bytes
	 * per transaction
	 *
	 * @param address EEPROM start address to write to
	 * @param data Pointer to data buffer to write
	 * @param size Buffer length to write - must be less than 64
	 *
	 * @return Status - 0 is ok
	 */
	uint8_t write(uint16_t address, void* data, uint8_t size);

	/**
	 * Write a byte to EEPROM
	 *
	 * @param address EEPROM address to write to
	 * @param data byte to write
	 *
	 * @return Status - 0 is ok
	 */
	uint8_t write(uint16_t address, uint8_t data);

	/**
	 * Read (up to) a page from EEPROM
	 *
	 * Performs a batch read operation
	 *
	 * @param address EEPROM start address to read from
	 * @param data Pointer to buffer to place data in
	 * @param size Number of bytes to read
	 *
	 * @return Status - 0 is ok
	 */
	uint8_t read(uint16_t address, void* data, uint8_t size);

	/**
	 * Read a byte from EEPROM
	 *
	 * @param address EEPROM address to read from
	 * @param data Reference to put received data
	 *
	 * @return Status - 0 is ok
	 */
	uint8_t read(uint16_t address, uint8_t& data);
};
