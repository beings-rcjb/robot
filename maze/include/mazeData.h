#pragma once

#include <cstdint>
#include <utility>
#include <vector>
#include <map>

typedef std::pair<std::pair<int8_t, int8_t>, int8_t> coord_t;

class Direction
{
public:

	enum dirs {N, E, S, W};

private:

	dirs _dir;

public:

	Direction() : _dir(N) {}
	Direction(dirs dir) : _dir(dir) {}

	Direction& operator++();
	Direction& operator--();
	Direction operator++(int);
	Direction operator--(int);
	Direction operator+(const Direction& other) const;
	Direction operator-(const Direction& other) const;
	Direction operator+(int8_t count) const;
	Direction operator-(int8_t count) const;
	bool operator==(const Direction& other) const;
	bool operator!=(const Direction& other) const;

	dirs toEnum() const { return _dir; }
};

class Maze
{
public:

	class Cell
	{
	public:

		enum class Wall_state : uint8_t { NoWall = 0, Wall = 1, Unknown = 2 };

		Cell() : data(0) {}

		Wall_state getWall(Direction direction) const;
		void setWall(Direction direction, Wall_state state = Wall_state::Wall);
		inline void setWall(Direction direction, bool state) { setWall(direction, state ? Wall_state::Wall : Wall_state::NoWall); }
		bool getVisited() const;
		void setVisited(bool state);
		bool getBlack() const;
		void setBlack(bool state);
		bool getRamp() const;
		void setRamp(bool state);
		bool getRampDirection() const;
		void setRampDirection(bool direction);

		std::vector<uint8_t> serialize() const;
		std::vector<uint8_t>::const_iterator deserialize(std::vector<uint8_t>::const_iterator inp_iterator);

	private:

		/*
		 * 0-3: Wall / No wall
		 * 4-7: Wall unknown / known
		 * 8:   Is visited
		 * 9:   Is black
		 * 10:  Is ramp
		 * 11:  Ramp direction
		 */
		uint16_t data;
	};

private:

	std::map<coord_t, Cell> data;

public:

	inline Cell& operator[](const coord_t& loc) { return data[loc]; }
	inline auto begin()->decltype(data.begin()) { return data.begin(); }
	inline auto end()->decltype(data.end()) { return data.end(); }
	inline bool cellExists(const coord_t& loc) { return static_cast<bool>(data.count(loc)); }

	std::vector<uint8_t> serialize() const;
	std::vector<uint8_t>::const_iterator deserialize(std::vector<uint8_t>::const_iterator inp_iterator);

	static Maze& getInstance();
};

namespace MazeState
{

extern Direction orientation;
extern coord_t pos;

}
