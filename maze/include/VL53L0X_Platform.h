#pragma once

#include "VL53L0X_Platform.hpp"
#include <stm32f4xx_hal.h>
#include <stm32f4xx_hal_i2c.h>

class VL53L0X_RobotPlatform : public VL53L0X::VL53L0X_Platform
{
public:

	VL53L0X_RobotPlatform(I2C_HandleTypeDef* hi2c, uint8_t address = 0x52) : hi2c(hi2c), address(address) {}

	VL53L0X::VL53L0X_Error WriteMulti(uint8_t index, uint8_t* pdata, uint32_t count) override;

	VL53L0X::VL53L0X_Error ReadMulti(uint8_t index, uint8_t* pdata, uint32_t count) override;

	VL53L0X::VL53L0X_Error WrByte(uint8_t index, uint8_t data) override;

	VL53L0X::VL53L0X_Error WrWord(uint8_t index, uint16_t data) override;

	VL53L0X::VL53L0X_Error WrDWord(uint8_t index, uint32_t data) override;

	VL53L0X::VL53L0X_Error RdByte(uint8_t index, uint8_t* data) override;

	VL53L0X::VL53L0X_Error RdWord(uint8_t index, uint16_t* data) override;

	VL53L0X::VL53L0X_Error RdDWord(uint8_t index, uint32_t* data) override;

	// No polling delay because we don't ever want to block

	void UpdateAddress(uint8_t DeviceAddress) override;

private:

	I2C_HandleTypeDef* hi2c;
	uint8_t address;
};
