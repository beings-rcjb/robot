#ifndef __TIMER_H
#define __TIMER_H

#include "includes.h"

extern uint16_t tim9_period;
extern uint16_t tim9_pulse1;
extern uint16_t tim9_pulse2;

void init_timerdefs(void);

#endif // __TIMER_H
