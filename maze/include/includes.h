#ifndef __INCLUDES_H
#define __INCLUDES_H

#include "stm32f4xx.h"
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_cortex.h"
#include "stm32f4xx_hal_adc.h"
#include "stm32f4xx_hal_gpio.h"
#include "stm32f4xx_hal_tim.h"
#include "stm32f4xx_hal_uart.h"
#include "stm32f4xx_hal_i2c.h"

#include "stm32f4xx_hal_flash.h"

#include "timer_defs.h"
#include "maths.h" // not a typo

//#define min(x, y) ((x) < (y) ? (x) : (y))
//#define max(x, y) ((x) > (y) ? (x) : (y))
//#define clamp(x, a, b) (max((a), min((b), (x))))

#define TIM_CHANNEL(x) (((x) - 1) << 2)

extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;
extern ADC_HandleTypeDef hadc3;
extern DMA_HandleTypeDef hdma_adc1;
extern DMA_HandleTypeDef hdma_adc2;
extern DMA_HandleTypeDef hdma_adc3;

extern DAC_HandleTypeDef hdac;

extern I2C_HandleTypeDef hi2c2;

extern SPI_HandleTypeDef hspi1;

extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim5;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim9;
extern TIM_HandleTypeDef htim10;
extern TIM_HandleTypeDef htim12;

extern UART_HandleTypeDef huart4;

#ifdef __cplusplus
extern "C" {
#endif

void SystemTick(void);
__attribute__((noreturn)) void reset_hardware(void);

#ifdef __cplusplus
}
#endif

#endif // __INCLUDES_H
