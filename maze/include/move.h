#pragma once
#include "robot.h"
#include "PIDCtrl.h"

#include <memory>

#include <cmath>

class MoveDriver
{
public:

	MoveDriver(Robot& robot);

	class MoverInfo
	{
	public:

		/**
		 * Bind to a MoveDriver
		 *
		 * Called once at the beginning of a move
		 *
		 * @param md MoveDriver to bind to
		 */
		virtual void bind(MoveDriver* md) = 0;

		/**
		 * Determine if mover should finish moving
		 *
		 * Called in each control update cycle
		 *
		 * @return true to stop, false to continue
		 */
		virtual bool isFinished() = 0;

		/**
		 * Get additional speed correction to insert into control loop
		 *
		 * Called in each control update cycle
		 *
		 * @return speed correction
		 */
		virtual float getSpeedCorrection() { return 0; }
	};

	class MoverMoveInfo : public MoverInfo
	{
	public:

		/**
		 * Get additional heading correction to insert into control loop
		 *
		 * @param currError current heading error without correction offset -
		 *                  positive means currently correcting left (assuming
		 *                  heading correction of 0)
		 *
		 * @return heading correction - positive turns left
		 */
		virtual float getHeadingCorrection(float currError) { return 0; }

		/**
		 * Get PID controller for maintaining heading
		 *
		 * Can be overridden to change tune / use different controller
		 */
		virtual PIDCtrl<float> getHeadingPID();
	};

	class MoverTurnInfo : public MoverInfo
	{};

	class MoverMoveDistanceInfo : public MoverMoveInfo
	{
	protected:

		float target_distance;
		MoveDriver* md;

	public:

		MoverMoveDistanceInfo(float distance) : target_distance(abs(distance)) {}

		inline virtual void bind(MoveDriver* driver) override { md = driver; }

		inline virtual bool isFinished() override { return abs(md->getCurrDistance()) >= target_distance; }
	};

	class MoverTurnDegreesInfo : public MoverTurnInfo
	{
	protected:

		float target_degrees;
		MoveDriver* md;

	public:

		MoverTurnDegreesInfo(float degrees);

		inline virtual void bind(MoveDriver* driver) override { md = driver; }

		inline virtual bool isFinished() override { return abs(md->getCurrTurn()) >= target_degrees; }
	};

	/**
	 * Move straight, using MoverInfo
	 *
	 * @param reverse whether or not to reverse the motor direction
	 * @param power motor power (positive)
	 * @param info std::unique_ptr to) MoveDriver::MoverMoveInfo object (must be rvalue as is unique_ptr)
	 * @return true on success, false if mover is busy
	 */
	bool move_straight(bool reverse, float power, unique_ptr<MoverMoveInfo>&& info);

	/**
	 * Turn, using MoverInfo
	 *
	 * @param direction turn direction: true = right, false = left
	 * @param power motor power (positive)
	 * @param info (std::unique_ptr to) MoveDriver::MoverTurnInfo object (must be rvalue as is unique_ptr)
	 * @return true on success, false if mover is busy
	 */
	bool turn(bool direction, float power, unique_ptr<MoverTurnInfo>&& info);

	/**
	 * Move straight a certain distance
	 * @param distance distance (in cm) to travel - sign indicates direction
	 * @param power motor power (positive)
	 * @return true on success, false if mover is busy
	 */
	bool move_straight(float distance, float power);

	/**
	 * Turn a number of degrees
	 * @param degrees degrees to turn (compass heading sign - positive is right, negative is left)
	 * @param power motor power (positive)
	 * @return true on success, false if mover is busy
	 */
	bool turn(float degrees, float power);

	/**
	 * Get currently traveled distance
	 * @return currently traveled distance, in cm - sign reflects direction
	 */
	float getCurrDistance();

	/**
	 * Get currently turned degrees
	 * @return currently turned degrees - sign reflects direction
	 */
	float getCurrTurn();
	bool isFinished();
	void stop();

	void clearPreviousRecords();

	// CALLBACKS

	// WARNING: Be sure to call after updating MPU readings
	void onSysTick();

private:

	Robot& robot;

	struct ____InternalData_MD
	{
		float curr_distance;
		float power;
		float speed_target;
		int8_t scale;
		PIDCtrl<float> speed_pid;
		PIDCtrl<float> sync_pid;
		PIDCtrl<float> heading_pid;
		float heading;

		unique_ptr<MoverMoveInfo> info;

		____InternalData_MD() : speed_pid(PIDCtrl<float>(0, 0, 0)),
			sync_pid(PIDCtrl<float>(0, 0, 0)),
			heading_pid(PIDCtrl<float>(0, 0, 0)) {}
	};

	struct ____InternalData_TD
	{
		float power;
		float speed_target;
		int8_t scales[2];
		float y_start;
		PIDCtrl<float> speed_pid;
		PIDCtrl<float> sync_pid;

		unique_ptr<MoverTurnInfo> info;

		____InternalData_TD() : speed_pid(PIDCtrl<float>(0, 0, 0)),
			sync_pid(PIDCtrl<float>(0, 0, 0)) {}
	};

	struct ____InternalData_AD
	{
		____InternalData_MD MoveData;
		____InternalData_TD TurnData;
	};

	____InternalData_AD ActData;

	enum class ActState { Idle, Move, Turn };

	ActState state = ActState::Idle;

	float prev_distance;
	float prev_turn;
};
