#pragma once

#include "flash.h"

// CONSTANTS
extern ld_section(".flash-config") volatile const float MIN_TEMPERATURE;

extern ld_section(".flash-config") volatile const float BLACK_THRESHOLD;

extern ld_section(".flash-config") volatile const float SILVER_THRESHOLD;
extern ld_section(".flash-config") volatile const float SILVER_SAMPLING_SAMPLES_NEEDED;

extern ld_section(".flash-config") volatile const float CELL_INTER_SIZE; ///< Cell size, used for reference between cells
extern ld_section(".flash-config") volatile const float CELL_INTRA_SIZE; ///< Cell size, used for reference within cells


void run_calibration();
