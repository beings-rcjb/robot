#pragma once

#if defined(__cplusplus)

	#ifdef DEBUG
		#include <stdexcept>
	#endif // DEBUG

template <typename A, typename B>
inline auto min(A a, B b)->decltype(a < b ? a : b)
{
	return a < b ? a : b;
}

template <typename A, typename B>
inline auto max(A a, B b)->decltype(a > b ? a : b)
{
	return a > b ? a : b;
}

template <typename X, typename L, typename H>
inline auto clamp(X x, L lo, H hi)->decltype(min(max(x, lo), hi))
{
	return min(max(x, lo), hi);
}

template <typename X, typename L, typename H>
auto clamp_wrap(X x, const L &lo, const H &hi)
     ->decltype((clamp(x, lo, hi) == x) ? x : x - (hi - lo))
{
	auto range = hi - lo;
	#ifdef DEBUG
	if (range < 0)
	{
		throw std::invalid_argument("lo is greater than hi");
	}
	#endif // DEBUG
	bool isSub = x > hi;
	while (clamp(x, lo, hi) != x)
	{
		if (isSub)
		{
			x -= range;
		}
		else
		{
			x += range;
		}
	}
	return x;
}

#else

	#define min(x, y) ((x) < (y) ? (x) : (y))
	#define max(x, y) ((x) > (y) ? (x) : (y))
	#define clamp(x, a, b) (max((a), min((b), (x))))

#endif // defined(__cplusplus)
