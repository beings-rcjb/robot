#pragma once

template <typename T>
class PIDCtrl
{
public:

	/**
	 * Constructor
	 *
	 * Sets PID constants
	 *
	 * @param kP P constant
	 * @param kI I constant
	 * @param kD D constant
	 * @param IClamp I clamp (MUST BE POSITIVE)
	 */
	PIDCtrl(T kP, T kI, T kD, T IClamp = 0);

	/**
	 * Update controller
	 *
	 * @param error Input error
	 *
	 * @return correction
	 */
	T update(T error);

	/**
	 * Reset controller
	 *
	 * Clears I and D
	 */
	void reset();

protected:

	T kP, kI, kD;
	T pI, pD; // persist for I and D
	T IClamp;
};

template <typename T>
PIDCtrl<T>::PIDCtrl(T kP_, T kI_, T kD_, T IClamp_)
{
	kP = kP_;
	kI = kI_;
	kD = kD_;
	pI = 0;
	pD = 0;
	IClamp = IClamp_;
}

template <typename T>
T PIDCtrl<T>::update(T error)
{
	T correct = 0;
	if (kP)
	{
		correct += (error * kP);
	}
	if (kI)
	{
		correct += (pI * kI);
		pI += error;
		if (IClamp != 0)
		{
			pI = max(-IClamp, min(pI, IClamp));
		}
	}
	if (kD)
	{
		correct += ((pD - error) * kD);
		pD = error;
	}
	return correct;
}

template <typename T>
void PIDCtrl<T>::reset()
{
	pI = pD = 0;
}
