/*
 * Quaternion.h
 *
 *  Created on: Aug 26, 2015
 *      Author: ethan
 */

#ifndef QUATERNION_H_
#define QUATERNION_H_

class Quaternion
{
public:

	Quaternion() : q0(1), q1(0), q2(0), q3(0) {}
	Quaternion(float q0_, float q1_, float q2_, float q3_) : q0(q0_), q1(q1_), q2(q2_), q3(q3_) {}

	void toYPR(float& y, float& p, float& r);

	float q0, q1, q2, q3;
};

#endif /* QUATERNION_H_ */
