#pragma once

#include "mazeData.h"

#include <cstdint>
#include <utility>

namespace MazeUtils
{

// Utility - cells
coord_t move_dir(coord_t start, Direction dir);
// move_dir and follow ramps
coord_t move_dir_follow(coord_t start, Direction dir);
bool isAdj(coord_t a, coord_t b);
bool isThruRamp(coord_t a, coord_t b);
Direction getDir(coord_t a, coord_t b);
Direction getDirThruRamp(coord_t a, coord_t b);

}
