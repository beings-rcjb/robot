#include "move.h"
#include "algo.h"
#include <array>
#include <vector>

using namespace std;

namespace HwImpl
{

const float MOVE_POWER_LOW = 30;
const float MOVE_POWER_NORM = 50;
const float MOVE_POWER_RAMP_UP = 60;
const float MOVE_POWER_RAMP_DOWN = 40;

const float TURN_POWER_NORM = 50;
const float TURN_POWER_LOW = 30;

/**
 * Begins moving to adjacent tile
 *
 * If is not directly ahead, turns to face the tile
 * Tile can be adjacent through ramp
 *
 * @param tile tile to go to - must adjacent
 */
bool moveToAdjacent(coord_t tile);

void preinitInitSolve();

void preinitResumeSolve();

void initUpdateWalls();

bool shouldReturn();

extern MoveDriver mover;

void save_data(vector<uint8_t> data);

void clear_data();

const vector<uint8_t> read_data();

uint8_t getSeed();


uint8_t buttonHandler();

}
