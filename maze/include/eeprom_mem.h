#pragma once

#include <cstdint>

namespace EEPROM
{

constexpr uint16_t SilverResetCount = 0x00;
constexpr uint16_t KitDropCount     = 0x01;

}
