#pragma once

#include <cstdint>
#include <functional>

namespace Timeout
{

void setTimeout(uint32_t ticks, std::function<void(void)> callback);

void setInterval(uint32_t ticks, std::function<void(void)> callback, bool triggersNow = true);

void onSysTick();

}
