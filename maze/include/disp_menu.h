#pragma once

#include <string>
#include <vector>
#include "GFX.h"

namespace Graph
{

class Menu
{
public:

	/**
	 * Callback for determining menu InputCB
	 *
	 * Blocking to wait for input optional
	 *
	 * @return 0 -> no input
	 *         1 -> up
	 *         2 -> down
	 *         3 -> select
	 *         4 -> exit (OPTIONAL)
	 */
	typedef unsigned char (*InputCB)();

	explicit Menu(GFX& display, InputCB cb, unsigned char color = 1);

	/**
	 * Type for item callback function
	 *
	 * Will be called whenever the registered menu item is selected.
	 * Can send additional data through void ptr.
	 * Return value determines whether to return to menu (false) or exit menu (true)
	 */
	typedef bool (*MenuCB)(void*);

	/**
	 * Add a menu item to the menu
	 *
	 * @param name Display name of the item
	 * @param cb   Callback for when item is selected
	 *             @see MenuCB
	 * @param data Additional data to be sent to the Callback
	 * @returns Nothing
	 */
	void addItem(std::string name, MenuCB cb, void* data = nullptr);

	/**
	 * Add an item that when selected, exits the menu
	 *
	 * Convenience wrapper for @ref addItem
	 *
	 * @see addItem
	 */
	void addExitItem(std::string name);

	/**
	 * Show the current menu
	 *
	 * Will block until the menu is dismissed (by a menu item callback)
	 */
	void show();

protected:

	/**
	 * Draw the menu
	 */
	void draw(unsigned char selected);

private:

	GFX& display;
	InputCB inpCb;

	unsigned char char_height, line_space = 2; // Line space should be divisible by 2
	unsigned char color, Black = 0;

	struct Item
	{
		std::string name;
		MenuCB cb;
		void* data;
	};

	std::vector<Item> items;

};

};
