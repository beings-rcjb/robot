#pragma once
#include <array>
#include "motor.h"
#include "gpio.h"
#include "mpu.h"
#include "SSD1306.h"
#include "piInterface.h"
#include "PiCamInterface.h"
#include "roundrobin.hpp"
#include "VL53L0X.hpp"
#include "VL53L0X_Platform.h"
#include "AT24CXXX.h"

class Robot
{
protected:

	class TOF_Sensor
	{
	public:

		TOF_Sensor(GPIO_TypeDef* gpioX, uint16_t gpioPin, float offset);

		void preInit();

		VL53L0X::VL53L0X_Error initSensor(uint8_t address = 0x52);

		VL53L0X::VL53L0X_Error postInit();

		void onSysTick();

		float getDistance() const;

		const uint8_t timingBudgetMs = 50;

	private:

		VL53L0X_RobotPlatform platform;
		VL53L0X::VL53L0X sensor;

		GPIO_TypeDef* gpioX;
		uint16_t gpioPin;
		const float offset;

		VL53L0X::VL53L0X_RangingMeasurementData_t lastRangingData;
	};

public:

	// INITIALIZATION
	Robot(TIM_TypeDef* PWM_TIM1, TIM_TypeDef* PWM_TIM2, TIM_TypeDef* Servo_TIM,
	      float Servo_RangeMin, float Servo_RangeMax);
	bool init();

	// ENUMS
	class Distance_FrontSensor
	{
	public:

		Distance_FrontSensor() = delete;

		static TOF_Sensor Left;
		static TOF_Sensor Right;
	};

	class Distance_SideSensor
	{
	public:

		Distance_SideSensor() = delete;

		static TOF_Sensor LeftFront;
		static TOF_Sensor LeftBack;
		static TOF_Sensor RightFront;
		static TOF_Sensor RightBack;
	};

	enum class Temp_Sensor { Left = 0, Right };

	enum class Switch { Slide = 0, LeftPush, RightPush };

	enum class LEDIndicator { Right = 14, Left };

	// DATA - units are cm and degrees C
	float getBatteryVoltage();
	float getDistance(const TOF_Sensor& sensor);
	float getPingDistance();
	float getFloorLight();
	float getTemp(Temp_Sensor sensor);
	bool getSwitchState(Switch sw);
	void setLEDState(LEDIndicator led, bool state);
	void setServoPos(float pos);
	uint16_t getRawFloorLight();

	// DEVICES
	Motor motors[2] = {
		Motor(GPIO_PIN(9),  GPIOA, GPIO_PIN(8),  GPIOA, &htim9, 1, &htim4),
		Motor(GPIO_PIN(11), GPIOA, GPIO_PIN(10), GPIOA, &htim9, 2, &htim3)
	};

	Mpu mpu;
	SSD1306 display = SSD1306(&hspi1, GPIOB, GPIO_PIN(13), GPIOB, GPIO_PIN(12), GPIOD, GPIO_PIN(2));

	HDLC hdlc = HDLC(&huart4);

	PiInterface piInterface = PiInterface(hdlc);
	PiCamInterface piCamInterface = PiCamInterface(piInterface);

	AT24CXXX eeprom;

	// CALLBACKS
	void onSysTick(); // to be called every 1ms

protected:

	bool isInitialized = false;

private:

	// ADC converted values: 2 ADC's, maximum of 2 conversions per ADC
	uint32_t ADC_values[2][2];

	TIM_TypeDef* PWM_TIM1, * PWM_TIM2, * Servo_TIM;
	const float Servo_RangeMin, Servo_RangeMax;

};

extern Robot robot;
