#pragma once

#define setBit(val, bit) ((val) | (1 << (bit)))
#define resetBit(val, bit) ((val) & ~(1 << (bit)))
