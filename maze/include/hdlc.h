#pragma once

#include <stm32f4xx.h>
#include <stm32f4xx_hal_uart.h>
#include <vector>
#include <functional>

class HDLC
{
private:

	static constexpr size_t BUF_SIZE = 256;

	const uint8_t FLAG, ESCAPE_CHAR, ESCAPE_MASK;

	UART_HandleTypeDef* huart;
	uint8_t intRxBuf[1];
	uint8_t frameRxBuf[BUF_SIZE], frameTxBuf[BUF_SIZE];

	/**
	 * Arguments: data received, is data complete frame?
	 */
	typedef std::function<void(std::vector<uint8_t>, bool)> handler_t;
	handler_t handlerFunc;

	/**
	 * Load (escaped) data into frameTxBuf
	 *
	 * frameTxBuf may be modified on failure
	 *
	 * @param data
	 * @return length of escaped sequence - 0 if buffer overflow
	 */
	size_t load_and_escape(std::vector<uint8_t> data);

	size_t rxFrameIndex = 0;
	bool rxIsInFrame = false;
	bool rxIsNextByteEscaped = false;

	inline HAL_StatusTypeDef startRx() { return HAL_UART_Receive_IT(huart, intRxBuf, 1); }

public:

	explicit HDLC(UART_HandleTypeDef* huart, uint8_t flag = 0x7E,
	     uint8_t escape_char = 0x7D, uint8_t escape_mask = 0x20)
			: FLAG(flag), ESCAPE_CHAR(escape_char),
			  ESCAPE_MASK(escape_mask), huart(huart)
	{}; // TODO: validate parameters

	bool init();

	bool send(const std::vector<uint8_t>& data);

	bool setRecvHandler(const handler_t& func);
	inline bool hasRecvHandler() { return static_cast<bool>(handlerFunc); }
	bool clearRecvHandler();

	// CALLBACKS

	void RxCpltCallback(UART_HandleTypeDef* huart_);
	void ErrorCallback(UART_HandleTypeDef* huart_);

};
