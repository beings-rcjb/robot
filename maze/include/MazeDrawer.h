#pragma once

#include <array>
#include "GFX.h"

namespace MazeDrawer
{

std::array<std::array<bool, 56>, 56> draw();

void drawToDisplay(GFX& display);

}
