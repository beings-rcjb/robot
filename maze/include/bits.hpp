#include "bits.h"

#include <cstddef>

template <typename T>
T writeBit(T val, std::size_t bit, bool state)
{
	if (state)
	{
		return setBit(val, bit);
	}
	else
	{
		return resetBit(val, bit);
	}
}
