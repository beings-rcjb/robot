#pragma once

#include "mazeData.h"

#include <algorithm>
#include <array>
#include <map>
#include "flash.h"

class Algorithm
{
private:

	// Searching and pathing

	bool pathfind_to(const coord_t& next_pos);

	void solve();

	void init_solve(bool isReal);

	std::array<Direction, 4> check_order;
	std::array<Direction, 4> check_order_rev;
	void setCheckOrder(std::array<Direction, 4> order);

public:

	Algorithm() = default;

	// Control
	void init_solve(void);
	void resume_solve(void);

	void save_state();

};

extern Algorithm algo;
