#pragma once

#include <cstdlib>
#include <vector>

template <typename T, class A = std::allocator<T> >
class roundrobin
{
public:

	typedef typename A::value_type value_type;
	typedef typename A::difference_type difference_type;
	typedef typename A::reference reference;
	typedef typename A::const_reference const_reference;

	roundrobin(size_t size);

	size_t size() const { return real_size; }

	const_reference operator[](size_t index) const;
	void insert(const_reference value);
	void clear();
	void fill(const T& value);

	class const_iterator
	{
		friend class roundrobin;

	public:

		typedef typename A::value_type value_type;
		typedef typename A::difference_type difference_type;
		typedef typename A::pointer pointer;
		typedef typename A::reference reference;
		typedef typename A::const_reference const_reference;
		typedef std::forward_iterator_tag iterator_category;

		const_iterator& operator++();

		const_reference operator*();

		bool operator==(const const_iterator& other) const { return curr_index == other.curr_index; }
		bool operator!=(const const_iterator& other) const { return curr_index != other.curr_index; }

	private:

		const_iterator(const roundrobin& parent, size_t curr_index);

		const roundrobin& parent;

		size_t curr_index;
	};

	const_iterator begin() const;
	const_iterator end() const;

private:

	std::vector<T, A> data;
	size_t curr_index = 0;
	size_t real_size = 0;
	const size_t cont_size;
};

template <typename T, class A>
typename roundrobin<T, A>::const_reference roundrobin<T, A>::operator[](size_t index) const
{
	if (curr_index - index >= cont_size) // curr_index - index is negative
	{
		index = curr_index + cont_size - index;
	}
	else
	{
		index = curr_index - index;
	}
	return data[index];
}

template <typename T, class A>
roundrobin<T, A>::roundrobin(size_t size) : data(size), cont_size(size)
{}

template <typename T, class A>
void roundrobin<T, A>::insert(const_reference value)
{
	if (++curr_index >= cont_size)
	{
		curr_index -= cont_size;
	}
	if (real_size < cont_size)
	{
		++real_size;
	}
	data[curr_index] = value;
}

template <typename T, class A>
void roundrobin<T, A>::clear()
{
	real_size = 0;
}

template <typename T, class A>
void roundrobin<T, A>::fill(const T& value)
{
	std::fill(data.begin(), data.end(), value);
	real_size = cont_size;
}

template <typename T, class A>
typename roundrobin<T, A>::const_iterator roundrobin<T, A>::begin() const
{
	return const_iterator(*this, 0);
}

template <typename T, class A>
typename roundrobin<T, A>::const_iterator roundrobin<T, A>::end() const
{
	return const_iterator(*this, real_size);
}

template <typename T, class A>
roundrobin<T, A>::const_iterator::const_iterator(const roundrobin& parent_, size_t curr_index_) : parent(parent_), curr_index(curr_index_)
{}

template <typename T, class A>
typename roundrobin<T, A>::const_iterator& roundrobin<T, A>::const_iterator::operator++()
{
	++curr_index;
	return *this;
}

template <typename T, class A>
typename roundrobin<T, A>::const_reference roundrobin<T, A>::const_iterator::operator*()
{
	return parent[curr_index];
}
