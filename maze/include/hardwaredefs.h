#pragma once

#include <stdint.h>
#include "flash.h"

extern const float SERVO_PULSE_CENTER;
extern const float SERVO_PULSE_RANGE;

extern ld_section(".flash-config") volatile const int16_t MPU_OFFS_AX;
extern ld_section(".flash-config") volatile const int16_t MPU_OFFS_AY;
extern ld_section(".flash-config") volatile const int16_t MPU_OFFS_AZ;
extern ld_section(".flash-config") volatile const int16_t MPU_OFFS_GX;
extern ld_section(".flash-config") volatile const int16_t MPU_OFFS_GY;
extern ld_section(".flash-config") volatile const int16_t MPU_OFFS_GZ;

extern const float TOF_OFFSET_LF;
extern const float TOF_OFFSET_LB;
extern const float TOF_OFFSET_RF;
extern const float TOF_OFFSET_RB;
extern const float TOF_OFFSET_FL;
extern const float TOF_OFFSET_FR;

extern const float IR_TEMP_BASE;
extern const float IR_TEMP_RANGE;

extern const float PING_OFFSET;

extern const float WHEEL_DIAMETER;

extern const float IR_FB_DISTANCE;

extern ld_section(".flash-config") volatile const float TURN_OFFSETS[2]; // Correction factors for if offsets are wrong;
