#pragma once

#include "hdlc.h"
#include "CallbackManager.h"
#include <vector>
#include <array>

class PiInterface
{
private:

	HDLC& hdlc;
	std::array<CallbackManager<std::vector<uint8_t> >, 256> callbacks;

	void handleData(std::vector<uint8_t> data, bool valid);

public:

	/**
	 * Construct an interface object
	 *
	 * @param hdlc uninitialized HDLC device to use
	 */
	PiInterface(HDLC& hdlc) : hdlc(hdlc)
	{}

	/**
	 * Initialize interface
	 *
	 * @return status - true for success
	 */
	bool init();

	/**
	 * Add a handler on a given channel
	 *
	 * @param id Channel ID to listen on
	 * @param cb Callback function
	 */
	inline void registerHandler(uint8_t id, std::function<void(std::vector<uint8_t>)> cb)
	{ callbacks[id].addCallback(std::move(cb)); }

	/**
	 * Send to a given channel
	 *
	 * @param id Channel ID to send on
	 * @param data Data to send
	 * @return status - true for success
	 */
	bool send(uint8_t id, std::vector<uint8_t> data);

	/**
	 * Send raw data down the line
	 *
	 * Wraps HDLC::send()
	 *
	 * @param data Data to send
	 * @return status - true for success
	 */
	bool sendRaw(const std::vector<uint8_t>& data);

};
