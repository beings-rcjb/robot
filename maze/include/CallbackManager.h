#pragma once

#include <functional>
#include <vector>

template <typename... Args>
class CallbackManagerAddable
{
public:

	virtual void addCallback(std::function<void(Args...)> cb) = 0;
};

template <typename... Args>
class CallbackManagerCallable
{
public:

	virtual void call(Args... args) = 0;
};

template <typename... Args>
class CallbackManager : public CallbackManagerAddable<Args...>, public CallbackManagerCallable<Args...>
{
public:

	void addCallback(std::function<void(Args...)> cb) override;

	void call(Args... args) override;

private:

	std::vector<std::function<void(Args...)> > callbacks;

};

template <typename... Args>
void CallbackManager<Args...>::addCallback(std::function<void(Args...)> cb)
{
	callbacks.push_back(std::move(cb));
}

template <typename... Args>
void CallbackManager<Args...>::call(Args... args)
{
	for (const auto& cb : callbacks)
	{
		cb(args...);
	}
}

template <typename... Args>
class CallbackManagerAddableShim : CallbackManagerAddable<Args...>
{
private:

	CallbackManager<Args...> cm;

protected:

	virtual void call(Args... args) { cm.call(args...); }

public:

	virtual void addCallback(std::function<void(Args...)> cb) override
	{ cm.addCallback(cb); }
};

template <typename... Args>
class CallbackManagerCallableShim : CallbackManagerCallable<Args...>
{
private:

	CallbackManager<Args...> cm;

protected:

	virtual void addCallback(std::function<void(Args...)> cb)
	{ cm.addCallback(cb); }

public:

	virtual void call(Args... args) override { cm.call(args...); }
};
