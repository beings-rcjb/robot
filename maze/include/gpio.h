#pragma once

#include "includes.h"

#define GPIO_PIN(x) (1 << x)

void GPIO_WritePin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin, bool state);

bool GPIO_ReadPin(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
