#pragma once
#include "MadgwickAHRS.h"
#include "Quaternion.h"
#include "includes.h"

class Mpu
{
public:

	// INITIALIZATION
	Mpu(I2C_HandleTypeDef* hi2c, uint8_t addr = 0x68);
	bool init(uint16_t offs_ax, uint16_t offs_ay, uint16_t offs_az,
	          uint16_t offs_gx, uint16_t offs_gy, uint16_t offs_gz);

	// DATA
	Quaternion getQuat();

	// CALLBACKS
	bool updateMPU();

private:

	I2C_HandleTypeDef* hi2c;
	uint8_t addr;
	MadgwickAHRS AHRS;

	bool isStarted;

	uint16_t last_time;
};
