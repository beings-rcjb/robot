#pragma once

#include "includes.h"
#include <vector>

using namespace std;

#define ld_section(x) __attribute__((section(x)))

class FlashBank
{
public:

	FlashBank(uint32_t sector, void* begin, uint16_t size, bool clear = true);

	template <typename T, typename U>
	size_t write(const U* addr, const T& data);
	template <typename Iter, typename U>
	size_t write(const U* addr, const Iter& begin, const Iter& end);

	inline size_t writeByte(const void* addr, const uint8_t& data) { return write(addr, data); }

	void commit();

private:

	uint32_t sector;
	void* begin;

	vector<uint8_t> mem;

};

template <typename T, typename U>
size_t FlashBank::write(const U* _addr, const T& data)
{
	const uint8_t* addr = (const uint8_t*)_addr; // Cast away potential volatile
	const uint8_t* pdata = (const uint8_t*)&data; // Get address
	for (size_t i = 0; i < sizeof(T); ++i)
	{
		write<uint8_t, uint8_t>(addr + i, *(pdata + i));
	}
	return sizeof(T);
}

template <typename Iter, typename U>
size_t FlashBank::write(const U* addr_in, const Iter& begin, const Iter& end)
{
	const uint8_t* addr = (const uint8_t*)(addr_in); // Cast away potential volatile
	for (Iter it = begin; it != end; ++it)
	{
		addr += write(addr, *it);
	}
	return addr - addr_in;
}
