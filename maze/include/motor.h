#pragma once
#include "includes.h"

// TODO: Currently only supports 16bit timers preconfigured for PWM output (with period)
class Motor
{
public:

	Motor(uint16_t ctrl1_pin, GPIO_TypeDef* ctrl1_bus, uint16_t ctrl2_pin, GPIO_TypeDef* ctrl2_bus,
	      TIM_HandleTypeDef* pwm_tim, uint8_t pwm_channel, TIM_HandleTypeDef* encoder_tim);
	bool init();

	void update(float power, bool direction);
	void update(float power);
	void update(bool direction);
	int16_t getEncoder();
	void setEncoder(int16_t val);

private:

	uint16_t ctrl1_pin, ctrl2_pin;
	GPIO_TypeDef* ctrl1_bus, * ctrl2_bus;
	TIM_HandleTypeDef* pwm_tim, * encoder_tim;
	uint8_t pwm_channel;
};
