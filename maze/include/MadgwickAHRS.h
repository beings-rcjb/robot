#pragma once

#include "Quaternion.h"

class MadgwickAHRS
{
public:

	/**
	 * @brief Constructor
	 *
	 * @param sampleFreq_: sample frequency in Hz
	 */
	MadgwickAHRS(float sampleFreq_) : sampleFreq(sampleFreq_) {}

	void setFreq(float sampleFreq_) { sampleFreq = sampleFreq_; }

	void update(float gx, float gy, float gz, float ax, float ay, float az, float mx, float my, float mz);
	void update(float gx, float gy, float gz, float ax, float ay, float az);

	Quaternion getQuat() { return q; }

private:

	float sampleFreq;
	float beta = 0.1f;
	Quaternion q;

	float invSqrt(float x);
};
