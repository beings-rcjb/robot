#!/bin/sh

# Script to flash robot from build directory
# Copy to (or execute from) build-directory that is a subdirectory
# of the maze source root (like maze/build)

# Set your preferred build tool (defaults to cmake --build)
BUILDCMD=''

if [ -z ${BUILDCMD} ]; then
  BUILDCMD='cmake --build .'
fi
echo "Building using '${BUILDCMD}'"
${BUILDCMD}
if [ $? -ne 0 ]; then
  echo "Build failed! Terminating..."
  exit 1
fi

# Exit on errors
set -e

echo "Writing 'load.gdb'"
cat > load.gdb <<EOF
target remote localhost:3333
load
monitor reset
monitor shutdown
detach
quit
EOF

# Don't exit on errors - handle errors manually (to print some text)
set +e

echo "Starting OpenOCD"
openocd -f ../STM32F405-ST-LINK.cfg &
OPENOCD_PID=$!

echo "Loading with GDB"
arm-none-eabi-gdb maze.elf -batch -x load.gdb

echo "Stopping OpenOCD"
kill -INT ${OPENOCD_PID} 2>/dev/null
[ $? -ne 0 ]
exit $?
